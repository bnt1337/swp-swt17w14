package de.gardenmanager.warning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class WarningSystemTest {

  @Autowired
  private WarningSystem warn;
  @Autowired
  private PersonRepository personRepository;
  @Autowired
  private UserAccountManager userAccountManager;
  private LocalDateTime date;
  private String description;
  private Person tenant, registrar;

  @Before
  public void setUp() {
    this.date = LocalDateTime.now();
    this.description = "Kinder bewerfen die Oma im Nachbargarten mit Tomaten und allerlei Unrat";
    this.tenant = new Person("Vorname", "Nachname", LocalDate.now().minusYears(35),
        new Address("Bambuswald", "Strauchhausen",
            "08153", "3"), new ContactInfo("spam@mail.com",
        "01234567899"), userAccountManager.create("u1", "p",
        Role.of("EMPTY")));
    this.registrar = new Person("Vorname", "Nachname", LocalDate.now().minusYears(35),
        new Address("Schnitzelweg", "Bratkartoffeldorf",
            "08153", "42"), new ContactInfo("spam@meail.com",
        "01234567899"), userAccountManager.create("u2", "p", Role.of("EMPTY")));
    personRepository.save(this.tenant);
    personRepository.save(this.registrar);
  }

  @Test
  public void createWarning() {
    assertNotNull(
        warn.createWarning(this.date, this.description, this.tenant.getId(), this.registrar));
  }

  @Test
  public void showWarning1() {
    warn.createWarning(this.date, this.description, this.tenant.getId(), this.registrar);
    warn.createWarning(this.date, this.description, this.tenant.getId(), this.registrar);
    warn.createWarning(this.date, this.description, this.tenant.getId(), this.registrar);
    Iterable<Warning> warner = warn.showWarnings();

    Iterator it = warner.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }

    assertEquals(count, 3);
  }

  @Test
  public void showWarning2() {
    Iterable<Warning> warner = warn.showWarnings();

    Iterator it = warner.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }

    assertEquals(count, 0);
  }

  @Test
  public void findOne() {
    Warning warner = warn
        .createWarning(this.date, this.description, this.tenant.getId(), this.registrar);
    assertNotNull(warn.findOne(warner.getId()));
  }

  @Test
  public void deleteWarning() {
    Warning warner = warn
        .createWarning(this.date, this.description, this.tenant.getId(), this.registrar);
    warn.deleteWarning(warner.getId());
  }

  @Test
  public void deleteAllWarning() {
    warn.deleteAllWarnings();
    Iterable<Warning> warner1 = warn.showWarnings();

    Iterator it = warner1.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 0);
  }
}
