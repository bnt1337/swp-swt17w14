package de.gardenmanager.warning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Person;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.salespointframework.useraccount.UserAccount;

public class WarningTest {

  private Warning warning;
  private LocalDateTime date;
  private String description;
  private Person tenant;
  private Person registrar;

  @Before
  public void setUp() {
    this.date = LocalDateTime.now();
    this.description = "Kinder bewerfen die Oma im Nachbargarten mit Tomaten und allerlei Unrat";
    this.tenant = new Person("Vorname", "Nachname", LocalDate.now().minusYears(35),
        new Address("Bambuswald", "Strauchhausen",
            "08153", "3"), new ContactInfo("spam@mail.com", "01234567899"), new UserAccount());
    this.registrar = new Person("Vorname", "Nachname", LocalDate.now().minusYears(35),
        new Address("Schnitzelweg", "Bratkartoffeldorf",
            "08153", "42"), new ContactInfo("spam@meail.com", "01234567899"), new UserAccount());
    this.warning = new Warning(date, description, tenant, registrar);
  }

  @Test
  public void createWarning() {
    assertNotNull(this.warning);
  }

  @Test
  public void getter() {
    assertEquals(this.warning.getDate(), this.date);
    assertEquals(this.warning.getDescription(), this.description);
    assertEquals(this.warning.getTenant(), this.tenant);
    assertEquals(this.warning.getRegistrar(), this.registrar);
  }
}
