package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AssignmentValidationFormTest {

  private AssignmentValidationForm form;

  @Before
  public void setUp() {
    this.form = new AssignmentValidationForm();
  }

  @Test
  public void TestForm() {
    form.setHoursDone((short) 45);

    assertEquals(45, (short) form.getHoursDone());
  }
}
