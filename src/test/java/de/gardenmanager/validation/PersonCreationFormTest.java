package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

public class PersonCreationFormTest {

  private PersonCreationForm form;

  @Before
  public void setUp() {
    this.form = new PersonCreationForm();
  }

  @Test
  public void TestForm() {
    form.setBirthday(LocalDate.now().minusYears(35));
    form.setFirstname("Firstname");
    form.setLastname("Lastname");
    form.setStreet("Street");
    form.setStreetNumber("22b");
    form.setCity("City");
    form.setZipCode("01234");
    form.setPhoneNumber("01234567890");
    form.setEmail("e@mail.com");
    form.setUsername("userA");
    form.setPassword("password");
    form.setParcel((long) 20184453);

    assertEquals("Firstname", form.getFirstname());
    assertEquals("Lastname", form.getLastname());
    assertEquals("Street", form.getStreet());
    assertEquals("22b", form.getStreetNumber());
    assertEquals("City", form.getCity());
    assertEquals("01234", form.getZipCode());
    assertEquals("01234567890", form.getPhoneNumber());
    assertEquals("e@mail.com", form.getEmail());
    assertEquals("userA", form.getUsername());
    assertEquals("password", form.getPassword());
    assertTrue((long) 20184453 == form.getParcel());
  }
}
