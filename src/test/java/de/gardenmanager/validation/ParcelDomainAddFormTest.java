package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;

public class ParcelDomainAddFormTest {

  private ParcelDomainAddForm parcelDomainAddForm;

  @Before
  public void setUp() throws Exception {
    parcelDomainAddForm = new ParcelDomainAddForm();
    parcelDomainAddForm.setChairman(5L);
    parcelDomainAddForm.setParcels(Arrays.asList(1L, 2L, 3L));
  }

  @Test
  public void getChairman() {
    assertEquals(5L, (long)parcelDomainAddForm.getChairman());
  }

  @Test
  public void setChairman() {
    assertEquals(5L, (long)parcelDomainAddForm.getChairman());
    parcelDomainAddForm.setChairman(6L);
    assertEquals(6L, (long)parcelDomainAddForm.getChairman());
  }

  @Test
  public void getParcels() {
    assertNotNull(parcelDomainAddForm.getParcels());
    assertEquals(3, parcelDomainAddForm.getParcels().size());
  }

  @Test
  public void setParcels() {
    assertNotNull(parcelDomainAddForm.getParcels());
    parcelDomainAddForm.setParcels(null);
    assertNull(parcelDomainAddForm.getParcels());
  }
}