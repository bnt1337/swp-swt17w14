package de.gardenmanager.validation;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;

public class PreorderCreationFormTest {

  private PreorderCreationForm preorderCreationForm;

  @Before
  public void setUp() throws Exception {
    preorderCreationForm = new PreorderCreationForm();
  }

  @Test
  public void getFirstname() {
    assertNull(preorderCreationForm.getFirstname());
  }

  @Test
  public void setFirstname() {
    assertNull(preorderCreationForm.getFirstname());
    preorderCreationForm.setFirstname("dsdsasav");
    assertNotNull(preorderCreationForm.getFirstname());
  }

  @Test
  public void getLastname() {
    assertNull(preorderCreationForm.getLastname());
  }

  @Test
  public void setLastname() {
    assertNull(preorderCreationForm.getLastname());
    preorderCreationForm.setLastname("dsdsasav");
    assertNotNull(preorderCreationForm.getLastname());
  }

  @Test
  public void getDate() {
    assertNull(preorderCreationForm.getDate());
  }

  @Test
  public void setDate() {
    assertNull(preorderCreationForm.getDate());
    preorderCreationForm.setDate(LocalDateTime.now());
    assertNotNull(preorderCreationForm.getDate());

  }
}