package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;

public class PersonRoleAddFormTest {

  private PersonRoleAddForm personRoleAddForm;

  @Before
  public void setUp() throws Exception {
    personRoleAddForm = new PersonRoleAddForm();
  }

  @Test
  public void getPerson() {
    assertNull(personRoleAddForm.getPerson());
  }

  @Test
  public void setPerson() {
    assertNull(personRoleAddForm.getPerson());
    personRoleAddForm.setPerson(1L);
    assertNotNull(personRoleAddForm.getPerson());
  }

  @Test
  public void getRoles() {
    assertNull(personRoleAddForm.getRoles());
  }

  @Test
  public void setRoles() {
    assertNull(personRoleAddForm.getRoles());
    personRoleAddForm.setRoles(Arrays.asList("A", "B", "C"));
    assertNotNull(personRoleAddForm.getRoles());
    assertEquals(3, personRoleAddForm.getRoles().size());
  }
}