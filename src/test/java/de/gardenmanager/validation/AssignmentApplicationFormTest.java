package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AssignmentApplicationFormTest {

  private AssignmentApplicationForm form;

  @Before
  public void setUp() {
    this.form = new AssignmentApplicationForm();
  }

  @Test
  public void TestForm() {
    form.setHours((short) 45);

    assertEquals(45, (short) form.getHours());
  }
}
