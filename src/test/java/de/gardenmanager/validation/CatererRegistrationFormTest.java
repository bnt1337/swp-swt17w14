package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CatererRegistrationFormTest {

  private CatererRegistrationForm form;

  @Before
  public void setUp() {
    this.form = new CatererRegistrationForm();
  }

  @Test
  public void TestForm() {
    form.setName("Name");
    form.setPassword("password");

    assertEquals("Name", form.getName());
    assertEquals("password", form.getPassword());
  }
}
