package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DishCreationFormTest {

  private DishCreationForm form;

  @Before
  public void setUp() {
    this.form = new DishCreationForm();
  }

  @Test
  public void TestForm() {
    form.setDishType("Starter");
    form.setAllergen("Allergen");
    form.setName("Name");
    form.setPrice(3.3);

    assertEquals("Starter", form.getDishType());
    assertEquals("Allergen", form.getAllergen());
    assertEquals("Name", form.getName());
    assertEquals(3.3, form.getPrice());
  }
}
