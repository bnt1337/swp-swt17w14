package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class WarningCreationFormTest {

  private WarningCreationForm form;

  @Before
  public void setUp() {
    this.form = new WarningCreationForm();
  }

  @Test
  public void TestForm() {
    form.setDescription("Es war einmal...");
    form.setTenantId((long) 2018010);

    assertEquals("Es war einmal...", form.getDescription());
    assertEquals(2018010, (long) form.getTenantId());
  }
}
