package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import org.junit.Before;
import org.junit.Test;

public class ReservationCreationFormTest {

  private ReservationCreationForm form;

  @Before
  public void setUp() {
    this.form = new ReservationCreationForm();
  }

  @Test
  public void TestForm() {
    form.setDescription("ich will aber...");
    form.setOwner("Klaus Jürgen");
    form.setStart(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 18, 50, 0).atZone(ZoneId.systemDefault()));
    form.setEnd(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 20, 50, 0).atZone(ZoneId
        .systemDefault()));

    assertEquals("ich will aber...", form.getDescription());
    assertEquals("Klaus Jürgen", form.getOwner());
    assertEquals(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 18, 50, 0).atZone(ZoneId.systemDefault()), form.getStart());
    assertEquals(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 20, 50, 0).atZone(ZoneId.systemDefault()), form.getEnd());
  }
}
