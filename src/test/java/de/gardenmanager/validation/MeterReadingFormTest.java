package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;

public class MeterReadingFormTest {

  private MeterReadingForm form;

  @Before
  public void setUp() {
    this.form = new MeterReadingForm();
  }

  @Test
  public void TestForm() {
    form.setWater(BigDecimal.TEN);
    form.setElectricity(BigDecimal.ONE);

    assertEquals(BigDecimal.TEN, form.getWater());
    assertEquals(BigDecimal.ONE, form.getElectricity());
  }
}
