package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ParcelCreationFormTest {

  private ParcelCreationForm form;

  @Before
  public void setUp() {
    this.form = new ParcelCreationForm();
  }

  @Test
  public void TestForm() {
    form.setLocation("ganz hinten links in der Ecke");
    form.setSize((float) 255.42);

    assertEquals("ganz hinten links in der Ecke", form.getLocation());
    assertTrue((float) 255.42 == form.getSize());
  }
}
