package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import org.junit.Before;
import org.junit.Test;

public class EventCreationFormTest {

  private EventCreationForm form;

  @Before
  public void setUp() {
    this.form = new EventCreationForm();
  }

  @Test
  public void TestForm() {
    form.setDescription("Toller Tag");
    form.setName("Toller Name");
    form.setStart(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 18, 50, 0).atZone(ZoneId.systemDefault()));
    form.setEnd(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 20, 50, 0).atZone(ZoneId.systemDefault()));
    form.setPublicvisible(true);
    form.setAssignment(false);

    assertEquals("Toller Tag", form.getDescription());
    assertEquals("Toller Name", form.getName());
    assertEquals(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 18, 50, 0).atZone(ZoneId.systemDefault()), form
        .getStart());
    assertEquals(LocalDateTime.of(2014, Month.SEPTEMBER, 14, 20, 50, 0).atZone(ZoneId.systemDefault()), form
        .getEnd());
    assertEquals(true, form.getPublicvisible());
    assertEquals(false, form.getAssignment());
  }
}
