package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AssignPersonParcelFormTest {

  private AssignPersonParcelForm form;

  @Before
  public void setUp() {
    this.form = new AssignPersonParcelForm();
  }

  @Test
  public void TestForm() {
    form.setPersonId("0815");

    assertEquals("0815", form.getPersonId());
  }
}
