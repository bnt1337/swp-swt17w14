package de.gardenmanager.validation;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class MenuCreationFormTest {

  private MenuCreationForm form;

  @Before
  public void setUp() {
    this.form = new MenuCreationForm();
  }

  @Test
  public void TestForm() {
    List<String> starters = new LinkedList<>();
    List<String> mainclourses = new LinkedList<>();
    List<String> desserts = new LinkedList<>();

    starters.add("Soljanka");
    starters.add("Klare Brühe");
    mainclourses.add("Schnitzel");
    mainclourses.add("Kartoffelsalat");
    desserts.add("Eis");
    desserts.add("Schokocreme");

    form.setName("Toller Name");
    form.setStarters(starters);
    form.setMaincourses(mainclourses);
    form.setDesserts(desserts);

    assertEquals("Toller Name", form.getName());
    assertEquals(starters, form.getStarters());
    assertEquals(mainclourses, form.getMaincourses());
    assertEquals(desserts, form.getDesserts());
  }
}
