package de.gardenmanager.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import de.gardenmanager.accounting.AccountingSystem;
import de.gardenmanager.calendar.Assignment;
import de.gardenmanager.calendar.AssignmentManagement;
import de.gardenmanager.calendar.Calendar;
import de.gardenmanager.calendar.Event;
import de.gardenmanager.calendar.WorkingHoursRepository;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class MainControllerTest {

  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private ParcelRepository parcelRepository;
  @Autowired
  private WorkingHoursRepository workingHoursRepository;
  @Autowired
  private Calendar calendar;
  @Autowired
  private AssignmentManagement assignmentManagement;
  @Autowired
  private AccountingSystem accountingSystem;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  public void getIndex() throws Exception {
    mockMvc.perform(get("/"))
        .andExpect(status().isOk())
        .andExpect(view().name("fe_welcome"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboard() throws Exception {
    Parcel parcel = parcelRepository.findOne(1L);
    Person person = personManagement
        .createPerson("", "Lastname", LocalDate.now().minusYears(30), new Address("", "",
                "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry",
                "p", Roles.ROLE_TENANTRY));

    Person person1 = personManagement
        .createPerson("", "Lastname", LocalDate.now().minusYears(80), new Address("", "",
                "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));

    Event event = calendar.createEvent("Leck mich!", "", LocalDateTime.now(),
        LocalDateTime.now().plusHours(10), false);
    Assignment assignment = assignmentManagement.createAssignment(event);
    assignmentManagement.applyForAssignment(assignment, person, (short) 4);
    assignmentManagement.validateAssignmentHours(assignment, person, person1, (short) 4);

    mockMvc.perform(get("/dashboard").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(model().attribute("hoursdone", (Object) null));

    parcel.setCurrentOwner(person1);
    parcelRepository.save(parcel);

    mockMvc.perform(get("/dashboard").with(user("tenantry1").roles("TENANTRY")))
        .andExpect(status().isOk())
        .andExpect(model().attribute("hoursdone", (Object) null));

    parcel.lease(person);
    parcelRepository.save(parcel);
    accountingSystem.createInvoices();


    mockMvc.perform(get("/dashboard").with(user("tenantry").roles("TENANTRY")))
        .andExpect(status().isOk())
        .andExpect(model().attribute("hoursdone", 4.0))
        .andExpect(model().attribute("invoiceavailable", true))
        .andExpect(model().attributeExists("invoice", "invoicepercentage"));
  }
}
