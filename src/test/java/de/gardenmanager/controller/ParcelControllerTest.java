package de.gardenmanager.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import de.gardenmanager.person.ParcelRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ParcelControllerTest {

  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private ParcelRepository parcelRepository;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  public void getFreeparcels() throws Exception {
    mockMvc.perform(get("/freeParcels"))
        .andExpect(status().isOk())
        .andExpect(view().name("fe_freeParcels"))
        .andExpect(model().attributeExists("parcels"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardParcelsParcelidAssign() throws Exception {
    mockMvc.perform(
        post("/dashboard/parcels/1/assign")
            .with(user("admin").roles("ADMIN"))
            .with(csrf())
            .param("personId", ""))
        .andExpect(status().isOk())
    .andExpect(model().hasErrors())
    .andExpect(model().attributeExists("parcel", "persons"));

    mockMvc.perform(
        post("/dashboard/parcels/1/assign")
            .with(user("admin").roles("ADMIN"))
            .with(csrf())
            .param("personId", "1"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/parcels"));

    assert (parcelRepository.findOne(1L).getCurrentOwner().getId() == 1);
  }

  @Test
  public void getDashboardParcelsParcelidAssign() throws Exception {
    mockMvc.perform(get("/dashboard/parcels/1/assign").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_parcelOverride"))
        .andExpect(model().attributeExists("assignPersonParcelForm", "parcel", "persons"));
  }

  @Test
  public void getDashboardParcels() throws Exception {
    mockMvc.perform(get("/dashboard/parcels").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_parcel"))
        .andExpect(model().attributeDoesNotExist("parcels"));
  }

  @Test
  public void getDashboardParcelsJson() throws Exception {
    mockMvc.perform(get("/dashboard/parcels/json").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
  }

  //  @Test
  //  public void getDashboardParcelsParcel() throws Exception {
  //    mockMvc.perform(get("/dashboard/parcels/1").with(user("admin").roles("ADMIN")))
  //        .andExpect(status().isOk())
  //        .andExpect(view().name("db_parcelInfo"))
  //        .andExpect(model().attributeExists("parcel"))
  //        .andExpect(model().attribute("parcel", Matchers.typeCompatibleWith(Parcel.class)));
  //  }
}
