package de.gardenmanager.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CatererControllerTest {

  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private UserAccountManager userAccountManager;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardCatererRegister() throws Exception {
    long c = userAccountManager.findAll().stream().count();

    mockMvc.perform(
        post("/dashboard/caterer/register")
            .with(csrf())
            .with(user("admin").roles("ADMIN"))
            .param("name", "user")
            .param("password", "short"))
        .andExpect(status().isOk())
        .andExpect(model().hasErrors());

    assertEquals(c, userAccountManager.findAll().stream().count());

    mockMvc.perform(
        post("/dashboard/caterer/register")
            .with(csrf())
            .with(user("admin").roles("ADMIN"))
            .param("name", "user")
            .param("password", "password123"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard"));

    assertEquals(c + 1, userAccountManager.findAll().stream().count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardCatererAdd() throws Exception {
    mockMvc.perform(
        get("/dashboard/caterer/add")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_catererRegistration"))
        .andExpect(model().attributeExists("catererRegistrationForm"));
  }
}
