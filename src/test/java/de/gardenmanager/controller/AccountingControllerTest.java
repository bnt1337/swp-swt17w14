package de.gardenmanager.controller;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.google.common.collect.Iterables;
import de.gardenmanager.accounting.AccountingSystem;
import de.gardenmanager.accounting.invoice.Invoice;
import de.gardenmanager.accounting.invoice.InvoiceManagement;
import de.gardenmanager.accounting.invoice.InvoiceRepository;
import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItemRepository;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringRunner.class)
@SpringBootTest
//@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AccountingControllerTest {

  //@Autowired
  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext context;

  //region Autowired
  @Autowired
  private InvoiceRepository invoiceRepository;
  @Autowired
  private InvoiceManagement invoiceManagement;
  @Autowired
  private AccountingSystem accountingSystem;
  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private ParcelRepository parcelRepository;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private InvoiceItemRepository invoiceItemRepository;
  //endregion

  @Before
  public void setup() {
    /*this.mockMvc = MockMvcBuilders.standaloneSetup(new AccountingController(invoiceRepository,
        invoiceManagement, accountingSystem, personManagement, parcelRepository,
        meteredPricingRepository))
        .build();*/
    mockMvc = MockMvcBuilders
        .webAppContextSetup(context)
        .apply(springSecurity())
        .build();

    invoiceItemRepository.deleteAll();
    invoiceRepository.deleteAll();
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccounting_asAdmin() throws Exception {
    if (!userAccountManager.findByUsername("admin").isPresent()) {
      userAccountManager.create("admin",
          "p", Role.of("ROLE_ADMIN"));
    }
    mockMvc
        .perform(get("/dashboard/accounting").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_accounting"))
        .andExpect(model().attributeExists("totals"))
        .andExpect(model().attributeExists("AllInvoices"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccounting_asCeo() throws Exception {
    if (!userAccountManager.findByUsername("ceo").isPresent()) {
      userAccountManager.create("ceo",
          "p", Roles.ROLE_CEO);
    }
    mockMvc
        .perform(get("/dashboard/accounting").with(user("ceo").roles("CEO")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_accounting"))
        .andExpect(model().attributeExists("totals"))
        .andExpect(model().attributeExists("AllInvoices"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccounting_asTreasurer() throws Exception {
    if (!userAccountManager.findByUsername("treasurer").isPresent()) {
      userAccountManager.create("treasurer",
          "p", Roles.ROLE_TREASURER);
    }
    mockMvc
        .perform(get("/dashboard/accounting").with(user("treasurer").roles("TREASURER")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_accounting"))
        .andExpect(model().attributeExists("totals"))
        .andExpect(model().attributeExists("AllInvoices"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccounting_asUser() throws Exception {
    if (!userAccountManager.findByUsername("tenantry").isPresent()) {
      personManagement
          .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
              new ContactInfo("", ""),
              userAccountManager.create("tenantry",
                  "p", Roles.ROLE_TENANTRY));
    }
    mockMvc
        .perform(get("/dashboard/accounting").with(user("tenantry").roles("TENANTRY")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_accounting"))
        .andExpect(model().attributeDoesNotExist("totals"))
        .andExpect(model().attributeExists("AllInvoices"));
  }

//  @Test
//  @Transactional
//  @Rollback
//  public void getDashboardAccountingInvoice() throws Exception {
//    Person p1 = personManagement
//        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
//            new ContactInfo("", ""),
//            userAccountManager.create("tenantry1",
//                "p", Roles.ROLE_TENANTRY));
//    Person p2 = personManagement
//        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
//            new ContactInfo("", ""),
//            userAccountManager.create("tenantry2",
//                "p", Roles.ROLE_TENANTRY));
//
//    UserAccount u = userAccountManager.create("treasurer",
//        "p", Roles.ROLE_TREASURER);
//
//    Parcel pa1 = Iterables.getFirst(parcelRepository.findAll(), new Parcel("TEST1", 2)).lease(p1);
//    Parcel pa2 = Iterables.getLast(parcelRepository.findAll(), new Parcel("TEST2", 2)).lease(p2);
//    parcelRepository.save(pa1);
//    parcelRepository.save(pa2);
//
//    accountingSystem.createInvoices();
//    Invoice i1 = invoiceManagement.findAllByParcel(pa1).stream().findFirst().get();
//    Invoice i2 = invoiceManagement.findAllByParcel(pa2).stream().findFirst().get();
//    assertNotNull(i1);
//    assertNotNull(i2);
//
//    mockMvc.perform(
//        get("/dashboard/accounting/" + i1.getId())
//            .with(user(p1.getUserAccount().getUsername()).roles("TENANTRY")))
//        .andExpect(status().isOk())
//        .andExpect(view().name("db_invoice"))
//        .andExpect(model().attributeExists("Invoice"));
//
//    mockMvc.perform(
//        get("/dashboard/accounting/" + i2.getId())
//            .with(user(p1.getUserAccount().getUsername()).roles("TENANTRY")))
//        .andExpect(status().is4xxClientError());
//
//    mockMvc.perform(
//        get("/dashboard/accounting/" + i1.getId())
//            .with(user(u.getUsername()).roles("TREASURER")))
//        .andExpect(status().isOk())
//        .andExpect(view().name("db_invoice"))
//        .andExpect(model().attributeExists("Invoice"));
//
//  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountingInvoiceSign() throws Exception {
    Person p1 = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));

    UserAccount u = userAccountManager.create("treasurer",
        "p", Roles.ROLE_TREASURER);

    Parcel pa1 = Iterables.getFirst(parcelRepository.findAll(), new Parcel("TEST1", 2)).lease(p1);
    parcelRepository.save(pa1);

    accountingSystem.createInvoices();
    Invoice i1 = invoiceManagement.findAllByParcel(pa1).stream().findFirst().get();
    assertNotNull(i1);

    mockMvc.perform(
        get("/dashboard/accounting/" + i1.getId() + "/sign")
            .with(user(u.getUsername()).roles("TREASURER")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounting"));

    assertNotNull(i1.getInvoiceDate());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountingInvoicePaid() throws Exception {
    Person p1 = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));

    UserAccount u = userAccountManager.create("treasurer",
        "p", Roles.ROLE_TREASURER);

    Parcel pa1 = Iterables.getFirst(parcelRepository.findAll(), new Parcel("TEST1", 2)).lease(p1);
    parcelRepository.save(pa1);

    accountingSystem.createInvoices();
    Invoice i1 = invoiceManagement.findAllByParcel(pa1).stream().findFirst().get();
    assertNotNull(i1);

    mockMvc.perform(
        get("/dashboard/accounting/" + i1.getId() + "/paid")
            .with(user(u.getUsername()).roles("TREASURER")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounting"));

    assertNotNull(i1.getPaidBy());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountingCreateinvoices() throws Exception {
    Person p1 = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));

    UserAccount u = userAccountManager.create("treasurer",
        "p", Roles.ROLE_TREASURER);

    Parcel pa1 = Iterables.getFirst(parcelRepository.findAll(), new Parcel("TEST1", 2)).lease(p1);
    parcelRepository.save(pa1);

    mockMvc.perform(
        get("/dashboard/accounting/createinvoices")
            .with(user(u.getUsername()).roles("TREASURER")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounting"));

    assertNotEquals(0, invoiceManagement.count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountingMeterreading() throws Exception {
    Person p1 = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));
    mockMvc.perform(
        get("/dashboard/accounting/meterreading")
            .with(user(p1.getUserAccount().getUsername()).roles("TENANTRY")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounting"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardAccountingMeterreadingAdd() throws Exception {
    Person p1 = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));

    Parcel pa1 = Iterables.getFirst(parcelRepository.findAll(), new Parcel("TEST1", 2)).lease(p1);
    parcelRepository.save(pa1);

    accountingSystem.createInvoices();

    mockMvc.perform(
        post("/dashboard/accounting/meterreading/add")
            .with(user(p1.getUserAccount().getUsername()).roles("TENANTRY"))
            .with(csrf())
            .param("water", "500.0")
            .param("electricity", "456.5"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounting"));

    assertTrue(invoiceManagement.findAllByParcel(pa1).stream().findFirst().map(i ->
        i.getInvoiceItems().stream().anyMatch(ii -> ii.getTag().equals("water") && ii
            .getUnits() == 500.0)).orElse(false));
    assertTrue(invoiceManagement.findAllByParcel(pa1).stream().findFirst().map(i ->
        i.getInvoiceItems().stream().anyMatch(ii -> ii.getTag().equals("electricity") && ii
            .getUnits() == 456.5)).orElse(false));
  }

}
