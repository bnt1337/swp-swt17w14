package de.gardenmanager.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import de.gardenmanager.application.Application;
import de.gardenmanager.application.ApplicationManagement;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import de.gardenmanager.validation.ApplicationCreationForm;
import java.time.LocalDate;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PersonControllerTest {

  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private ApplicationManagement applicationManagement;
  @Autowired
  private ParcelRepository parcelRepository;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccounts() throws Exception {
    mockMvc.perform(get("/dashboard/accounts").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_person"))
        .andExpect(model().attributeExists("persons"));

    personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("chair@man.de", ""),
            userAccountManager.create("chairman",
                "p", Roles.ROLE_CHAIRMAN));

    mockMvc.perform(get("/dashboard/accounts").with(user("chairman").roles("CHAIRMAN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_person"))
        .andExpect(model().attribute("persons", Matchers.emptyCollectionOf(Person.class)));

  }

  @Test
  public void getDashboardAccountsChairman() throws Exception {
    mockMvc.perform(get("/dashboard/accounts/chairman").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_person_chairman"))
        .andExpect(model().attributeExists("chairmen", "parcels"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardAccountsChairman() throws Exception {
    Person chairman = personManagement.createPerson("", "Lastname", LocalDate.MIN,
        new Address("", "", "", ""),
        new ContactInfo("chair@man.de", ""),
        userAccountManager.create("chairman",
            "p", Roles.ROLE_CHAIRMAN));
    mockMvc.perform(
        post("/dashboard/accounts/chairman")
            .with(user("admin").roles("ADMIN"))
            .with(csrf())
            .param("chairman", Long.toString(chairman.getId()))
            .param("parcels", "1", "2", "3"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts/chairman"));

    assertEquals(3, chairman.getDomain().size());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountsAdd() throws Exception {
    mockMvc.perform(get("/dashboard/accounts/add").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addTenant"))
        .andExpect(model().attributeExists("parcels", "personCreationForm"));

    ApplicationCreationForm applicationCreationForm = new ApplicationCreationForm();
    applicationCreationForm.setParcel("A1");
    applicationCreationForm.setEmail("hein@z.de");
    applicationCreationForm.setFirstname("H");
    applicationCreationForm.setName("1");
    applicationCreationForm.setTelephone("973287323");
    Application appl = applicationManagement.createApplication(applicationCreationForm);

    mockMvc.perform(
        get("/dashboard/accounts/add?application=" + appl.getId())
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addTenant"))
        .andExpect(model().attributeExists("parcels", "personCreationForm"))
        .andExpect(model().attribute("personCreationForm", Matchers.hasProperty("email", Matchers
            .is(appl.getEmail()))))
        .andExpect(model().attribute("personCreationForm", Matchers.hasProperty("parcel", Matchers
            .is(appl.getParcel().getId()))));

    Person chairman = personManagement.createPerson("", "Lastname", LocalDate.MIN,
        new Address("", "", "", ""),
        new ContactInfo("chair@man.de", ""),
        userAccountManager.create("chairman",
            "p", Roles.ROLE_CHAIRMAN));

    mockMvc.perform(
        get("/dashboard/accounts/add").with(user("chairman").roles("CHAIRMAN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addTenant"))
        .andExpect(model().attributeExists("parcels", "personCreationForm"))
        .andExpect(model().attribute("parcels", Matchers.emptyCollectionOf(Parcel.class)));
  }

  @Test
  public void postDashboardAccountsAdd() {
    //TODO
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountsAccountDisableAndEnable() throws Exception {
    Person chairman = personManagement.createPerson("", "Lastname", LocalDate.MIN,
        new Address("", "", "", ""),
        new ContactInfo("chair@man.de", ""),
        userAccountManager.create("chairman",
            "p", Roles.ROLE_CHAIRMAN));

    mockMvc.perform(get("/dashboard/accounts/" + chairman.getId() + "/disable").with(user("admin")
        .roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts"));

    assertFalse(chairman.getUserAccount().isEnabled());

    mockMvc.perform(get("/dashboard/accounts/" + chairman.getId() + "/enable").with(user("admin")
        .roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts"));

    assertTrue(chairman.getUserAccount().isEnabled());
  }

  @Test
  public void getDashboardAccountsAccountSellparcel() throws Exception {
    Parcel parcel = parcelRepository.findOne(1L);
    parcel.lease(personManagement.findOne(1L));
    parcelRepository.save(parcel);

    mockMvc.perform(get("/dashboard/accounts/1/sellparcel").with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts"));

    assertNotEquals(personManagement.findOne(1L), parcel.getCurrentOwner());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountsAccount() throws Exception {
    Person chairman = personManagement.createPerson("", "Lastname", LocalDate.MIN,
        new Address("", "", "", ""),
        new ContactInfo("chair@man.de", ""),
        userAccountManager.create("user",
            "p", Roles.ROLE_TENANTRY));

    mockMvc.perform(get("/dashboard/accounts/1").with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("person", "warningRepository"))
        .andExpect(view().name("db_userAccount"));

    mockMvc.perform(get("/dashboard/accounts/1").with(user("user").roles("TENANTRY")))
        .andExpect(status().is4xxClientError());
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardAccountsManage() throws Exception {
    Person user = personManagement.findOne(1L);
    user.addParcel(parcelRepository.findOne(1L));
    personManagement.save(user);

    mockMvc.perform(
        post("/dashboard/accounts/manage")
            .with(user("admin").roles("ADMIN"))
            .with(csrf())
            .param("person", "1")
            .param("roles", "ROLE_CEO"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts/1"));

    assertEquals(0, user.getDomain().size());
    assertTrue(user.getUserAccount().hasRole(Roles.ROLE_CEO));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountsGenerateperson() throws Exception {
    mockMvc.perform(get("/dashboard/accounts/generateperson").with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts"))
        .andExpect(flash().attributeExists("message"));

    assertEquals(2, personManagement.findAll().count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountsGeneratechairman() throws Exception {
    mockMvc.perform(get("/dashboard/accounts/generatechairman").with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts"))
        .andExpect(flash().attributeExists("message"));

    assertEquals(2, personManagement.findAll().count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountsGeneratetreasurer() throws Exception {
    mockMvc.perform(get("/dashboard/accounts/generatetreasurer").with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts"))
        .andExpect(flash().attributeExists("message"));

    assertEquals(2, personManagement.findAll().count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAccountsGenerateceo() throws Exception {
    mockMvc.perform(get("/dashboard/accounts/generateceo").with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/accounts"))
        .andExpect(flash().attributeExists("message"));

    assertEquals(2, personManagement.findAll().count());
  }
}
