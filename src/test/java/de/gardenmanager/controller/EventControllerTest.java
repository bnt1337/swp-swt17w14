package de.gardenmanager.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import de.gardenmanager.calendar.Assignment;
import de.gardenmanager.calendar.AssignmentManagement;
import de.gardenmanager.calendar.AssignmentRepository;
import de.gardenmanager.calendar.Calendar;
import de.gardenmanager.calendar.Event;
import de.gardenmanager.calendar.EventRepository;
import de.gardenmanager.calendar.WorkingHours;
import de.gardenmanager.calendar.WorkingHoursRepository;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EventControllerTest {

  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private EventRepository eventRepository;
  @Autowired
  private Calendar calendar;
  @Autowired
  private AssignmentRepository assignmentRepository;
  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private AssignmentManagement assignmentManagement;
  @Autowired
  private WorkingHoursRepository workingHoursRepository;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardCalendar() throws Exception {
    mockMvc.perform(
        get("/dashboard/calendar")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_calendar"))
        .andExpect(model().attributeExists("events", "events_a"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardEventsAdd() throws Exception {
    mockMvc.perform(
        get("/dashboard/events/add")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addEvent"))
        .andExpect(model().attributeExists("eventCreationForm"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardEventsCreate() throws Exception {
    assertEquals(0, eventRepository.count());
    assertEquals(0, assignmentRepository.count());
    mockMvc.perform(
        post("/dashboard/events/add")
            .with(csrf())
            .with(user("admin").roles("ADMIN"))
            .param("name", "32-Bit - The signed Overflow Party")
            .param("description",
                "This is the party of the millennium. With overflowing joy and beer in "
                    + "overflowing abundance, we're celebrating the overflow of 32-bit Unix time "
                    + "and let ourselves be swamped.")
            .param("start", "2038-01-19T03:14:08Z")
            .param("end", "1901-12-13T20:45:52Z")
            .param("assignment", "false")
            .param("publicvisible", "false"))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addEvent"))
        .andExpect(model().hasErrors());
    assertEquals(0, eventRepository.count());
    assertEquals(0, assignmentRepository.count());
    mockMvc.perform(
        post("/dashboard/events/add")
            .with(csrf())
            .with(user("admin").roles("ADMIN"))
            .param("name", "64-Bit Only Event")
            .param("description",
                "The party for people with more Bits.")
            .param("start", "2038-01-19T03:14:09Z")
            .param("end", "9999-12-31T23:59:59Z")
            .param("assignment", "false")
            .param("publicvisible", "false"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/calendar"));
    assertEquals(1, eventRepository.count());
    assertEquals(0, assignmentRepository.count());
    mockMvc.perform(
        post("/dashboard/events/add")
            .with(csrf())
            .with(user("admin").roles("ADMIN"))
            .param("name", "Work is Fun!")
            .param("description",
                "Work!!")
            .param("start", "1970-01-01T00:00:00Z")
            .param("end", "9999-12-31T23:59:59Z")
            .param("assignment", "true")
            .param("publicvisible", "false"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/calendar"));
    assertEquals(2, eventRepository.count());
    assertEquals(1, assignmentRepository.count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardEventsEditId() throws Exception {
    mockMvc.perform(
        get("/dashboard/events/52345/edit")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/events/add"));

    Event e1 = calendar.createEvent("Demo Event to Edit", "Fancy description",
        LocalDateTime.now().plusDays(10),
        LocalDateTime.now().plusDays(10).plusHours(10), false);

    Event e2 = calendar.createEvent("Demo Event to Edit", "Fancy description",
        LocalDateTime.now().minusDays(10),
        LocalDateTime.now().minusDays(10).plusHours(10), false);

    mockMvc.perform(
        get("/dashboard/events/" + e1.getId() + "/edit")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_editEvent"))
        .andExpect(model().attribute("eventEditForm",
            hasProperty("name", is(e1.getName()))))
        .andExpect(model().attribute("eventEditForm",
            hasProperty("description", is(e1.getDescription()))))
        .andExpect(model().attribute("eventEditForm",
            hasProperty("start", is(e1.getStart().atZone(ZoneOffset.UTC)))))
        .andExpect(model().attribute("eventEditForm",
            hasProperty("end", is(e1.getEnd().atZone(ZoneOffset.UTC)))))
        .andExpect(model().attribute("eventEditForm",
            hasProperty("assignment", is(e1.hasAssignment()))))
        .andExpect(model().attribute("eventEditForm",
            hasProperty("publicvisible", is(e1.isPublished()))));

    mockMvc.perform(
        get("/dashboard/events/" + e2.getId() + "/edit")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/calendar"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardEventsEditId() throws Exception {
    Event e1 = calendar.createEvent("Demo Event to Edit", "Fancy description",
        LocalDateTime.now().plusDays(10),
        LocalDateTime.now().plusDays(10).plusHours(10), false);

    LocalDateTime newstart = LocalDateTime.now().plusDays(12);
    LocalDateTime newend = newstart.plusHours(10);

    mockMvc.perform(
        post("/dashboard/events/" + e1.getId() + "/edit")
            .with(user("admin").roles("ADMIN"))
            .with(csrf())
            .param("name", "Edited")
            .param("description", "Edited")
            .param("start", newstart.atZone(ZoneOffset.UTC).toString())
            .param("end", newend.atZone(ZoneOffset.UTC).toString())
            .param("assignment", "false")
            .param("publicvisible", "true"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/calendar"));

    e1 = eventRepository.findById(e1.getId());

    assertTrue(e1.getStart().equals(newstart));
    assertTrue(e1.getEnd().equals(newend));
    assertTrue(e1.isPublished());
    assertEquals("Edited", e1.getName());
    assertEquals("Edited", e1.getDescription());

    mockMvc.perform(
        post("/dashboard/events/" + e1.getId() + "/edit")
            .with(user("admin").roles("ADMIN"))
            .with(csrf())
            .param("name", "")
            .param("description", "")
            .param("start", "2004-06-14T23:34:30Z")
            .param("end", "2004-06-14T23:34:30Z")
            .param("assignment", "")
            .param("publicvisible", ""))
        .andExpect(status().isOk())
        .andExpect(model().hasErrors());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardEventsRemoveName() throws Exception {
    Event e = calendar.createEvent("Demo Event to Edit", "Fancy description",
        LocalDateTime.now().plusDays(10),
        LocalDateTime.now().plusDays(10).plusHours(10), false);
    assertEquals(1, eventRepository.count());

    mockMvc.perform(
        get("/dashboard/events/remove/" + e.getName())
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/calendar"));
    assertEquals(0, eventRepository.count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAssignments() throws Exception {
    mockMvc.perform(
        get("/dashboard/assignments")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_assignments"))
        .andExpect(model().attributeExists("assignments"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAssignmentsAssignmentidApply() throws Exception {
    Event e = calendar.createEvent("Demo Event to Edit", "Fancy description",
        LocalDateTime.now().plusDays(10),
        LocalDateTime.now().plusDays(10).plusHours(10), true);
    Assignment a = assignmentManagement.createAssignment(e);
    mockMvc.perform(
        get("/dashboard/assignments/" + a.getId() + "/apply")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_applyAssignment"))
        .andExpect(model().attributeExists("assignmentApplicationForm", "assignment"));

  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardAssignmentsAssignmentidApplyApplied() throws Exception {
    Person p = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));
    Event e = calendar.createEvent("Demo Event to Edit", "Fancy description",
        LocalDateTime.now().plusDays(10),
        LocalDateTime.now().plusDays(10).plusHours(10), true);
    Assignment a = assignmentManagement.createAssignment(e);

    assertEquals(0, workingHoursRepository.count());
    mockMvc.perform(
        post("/dashboard/assignments/" + a.getId() + "/apply")
            .with(csrf())
            .with(user(p.getUserAccount().getUsername()).roles("TENANTRY"))
            .param("hours", "-8"))
        .andExpect(status().isOk())
        .andExpect(model().hasErrors());
    assertEquals(0, workingHoursRepository.count());
    mockMvc.perform(
        post("/dashboard/assignments/" + a.getId() + "/apply")
            .with(csrf())
            .with(user(p.getUserAccount().getUsername()).roles("TENANTRY"))
            .param("hours", "5"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/assignments"));
    assertEquals(1, workingHoursRepository.count());
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardAssigmentsAssigmentidValidatePersonid() throws Exception {
    mockMvc.perform(
        get("/dashboard/assignments/213/validate/23")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_validateAssignments"))
        .andExpect(model().attributeExists("assignmentValidationForm", "personid",
            "assignmentid"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardAssigmentsAssigmentidValidatePersonidValid() throws Exception {
    Person p1 = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_TENANTRY));
    Person p2 = personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            userAccountManager.create("chairman",
                "p", Roles.ROLE_CHAIRMAN));

    Event e = calendar.createEvent("Demo Event to Edit", "Fancy description",
        LocalDateTime.now().plusDays(10),
        LocalDateTime.now().plusDays(10).plusHours(10), true);
    Assignment a = assignmentManagement.createAssignment(e);
    assignmentManagement.applyForAssignment(a, p1, (short) 3);
    WorkingHours w = workingHoursRepository.findByAssignmentAndAndPerson(a, p1).get();

    mockMvc.perform(
        post("/dashboard/assignments/" + a.getId() + "/validate/" + p1.getId())
            .with(csrf())
            .with(user(p2.getUserAccount().getUsername()).roles("CHAIRMAN"))
            .param("hoursDone", "-3"))
        .andExpect(status().isOk())
        .andExpect(model().hasErrors())
        .andExpect(model().attributeExists("assignmentValidationForm", "personid",
            "assignmentid"));

    assertEquals(0, w.getHoursDone());

    mockMvc.perform(
        post("/dashboard/assignments/" + a.getId() + "/validate/" + p1.getId())
            .with(csrf())
            .with(user(p2.getUserAccount().getUsername()).roles("CHAIRMAN"))
            .param("hoursDone", "3"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/assignments/" + a.getId()));
    w = workingHoursRepository.findByAssignmentAndAndPerson(a, p1).get();
    assertNotNull(w);
    assertEquals(3, w.getHoursDone());
    assertEquals(p2, w.getValidator());
  }

  @Test
  @Transactional
  @Rollback
  public void getEvents() throws Exception {
    mockMvc.perform(get("/events"))
        .andExpect(status().isOk())
        .andExpect(view().name("fe_events"))
        .andExpect(model().attributeExists("events"));
  }
}
