package de.gardenmanager.controller;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ApplicationControllerTest {

  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private ParcelRepository parcelRepository;
  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private UserAccountManager userAccountManager;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
  }

  @Test
  @Transactional
  @Rollback
  public void getApplication() throws Exception {
    Parcel p = parcelRepository.findOne(1L);
    mockMvc.perform(get("/application").param("id", "1"))
        .andExpect(status().isOk())
        .andExpect(view().name("fe_application"))
        .andExpect(model().attributeExists("applicationCreationForm"))
        .andExpect(model()
            .attribute("applicationCreationForm", hasProperty("parcel", is(p.getLocation()))));
  }

  @Test
  @Transactional
  @Rollback
  public void postApplicationCreate() throws Exception {
    personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("chair@man.de", ""),
            userAccountManager.create("tenantry1",
                "p", Roles.ROLE_CHAIRMAN));

    Parcel p = parcelRepository.findOne(1L);
    mockMvc.perform(
        post("/application")
            .with(csrf())
            .param("parcel", p.getLocation())
            .param("firstname", "Hans")
            .param("name", "Wurst")
            .param("telephone", "384562394657923846")
            .param("email", "hans@example.com"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/"));

    mockMvc.perform(
        post("/application")
            .with(csrf())
            .param("parcel", "hcliasjdgc")
            .param("firstname", "Hans")
            .param("name", "Wurst")
            .param("telephone", "384562394657923846")
            .param("email", "hans@example.com"))
        .andExpect(status().isOk())
        .andExpect(model().hasErrors());

  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardApplicationView() throws Exception {
    mockMvc.perform(
        get("/dashboard/application/view")
            .with(user("admin").roles("ADMIN")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_application"))
        .andExpect(model().attributeExists("applications"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardApplicationDeleteId() {
    //TODO
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardApplicationAcceptId() {
    //TODO
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardApplicationDeclineId() {
    //TODO
  }
}
