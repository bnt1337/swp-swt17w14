package de.gardenmanager.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.google.common.collect.Iterables;
import de.gardenmanager.restaurant.DishCatalog;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class RestaurantControllerTest {

  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private DishCatalog dishCatalog;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    userAccountManager.create("caterer", "p", Role.of("ROLE_CATERER"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardResturantDish() throws Exception {
    mockMvc.perform(get("/dashboard/restaurant/dish")
        .with(user("caterer").roles("CATERER")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_dish"))
        .andExpect(model().attributeExists("dishList"));

  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardRestaurantMenu() throws Exception {
    mockMvc.perform(get("/dashboard/restaurant/menu")
        .with(user("caterer").roles("CATERER")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_menu"))
        .andExpect(model().attributeExists("menuList"));
  }

  @Test
  @Transactional
  @Rollback
  public void getOpening() throws Exception {
    mockMvc.perform(get("/opening"))
        .andExpect(status().isOk())
        .andExpect(view().name("fe_r_opening"));
  }

  @Test
  @Transactional
  @Rollback
  public void getMenu() throws Exception {
    mockMvc.perform(get("/menu"))
        .andExpect(status().isOk())
        .andExpect(view().name("fe_r_menu"))
        .andExpect(model().attributeExists("starters", "maincourses", "desserts"));
  }

  @Test
  @Transactional
  @Rollback
  public void getPreorder() throws Exception {
    mockMvc.perform(get("/preorder"))
        .andExpect(status().isOk())
        .andExpect(view().name("fe_r_preorder_add"));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardRestaurantDishAdd() throws Exception {
    mockMvc.perform(get("/dashboard/restaurant/dish/add")
        .with(user("caterer").roles("CATERER")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addDish"))
        .andExpect(model().attributeExists("dishForm"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardRestaurantDishNew() throws Exception {
    assertEquals(0, Iterables.size(dishCatalog.findAll()));

    mockMvc.perform(
        post("/dashboard/restaurant/dish/new")
            .with(user("caterer").roles("CATERER"))
            .with(csrf())
            .param("name", "")
            .param("allergen", "")
            .param("price", "")
            .param("dishType", ""))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/restaurant/dish/add"));

    assertEquals(0, Iterables.size(dishCatalog.findAll()));

    mockMvc.perform(
        post("/dashboard/restaurant/dish/new")
            .with(user("caterer").roles("CATERER"))
            .with(csrf())
            .param("name", "test")
            .param("allergen", "test allergen")
            .param("price", "10.0")
            .param("dishType", "something"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/dashboard/restaurant/dish"));

    assertEquals(1, Iterables.size(dishCatalog.findAll()));
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardRestaurantDishRemoveName() {
    //TODO
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardRestaurantMenuAdd() throws Exception {
    mockMvc.perform(get("/dashboard/restaurant/menu/add")
        .with(user("caterer").roles("CATERER")))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addMenu"))
        .andExpect(model().attributeExists("menuForm", "starters", "maincourses", "desserts"));
  }

  @Test
  @Transactional
  @Rollback
  public void postDashboardRestaurantMenuNew() throws Exception {
    mockMvc.perform(
        post("/dashboard/restaurant/menu/new")
            .with(user("caterer").roles("CATERER"))
            .with(csrf())
            .param("name", "")
            .param("starters", "")
            .param("maincourses", "")
            .param("desserts", ""))
        .andExpect(status().isOk())
        .andExpect(view().name("db_addMenu"))
        .andExpect(model().attributeExists("menuForm", "starters", "maincourses", "desserts"))
        .andExpect(model().hasErrors());
    //TODO submit real form
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardRestaurantMenuRemoveName() {
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardRestaurentMenuPublishName() {
    //TODO
  }

  @Test
  @Transactional
  @Rollback
  public void getDashboardRestaurantMenuUnpublishName() {
    //TODO
  }
}
