package de.gardenmanager.controller;

import static org.junit.Assert.assertEquals;

import de.gardenmanager.controller.Message.Severity;
import org.junit.Before;
import org.junit.Test;

public class MessageTest {

  private Message message;

  @Before
  public void setUp() throws Exception {
    message = new Message("demo", Severity.INFO);
  }

  @Test
  public void getText() {
    assertEquals("demo", message.getText());
  }

  @Test
  public void getSeverity() {
    assertEquals(Severity.INFO, message.getSeverity());
  }

  @Test
  public void getSeverityCss() {
    assertEquals("info", new Message("", Severity.INFO).getSeverityCss());
    assertEquals("success", new Message("", Severity.SUCCESS).getSeverityCss());
    assertEquals("warning", new Message("", Severity.WARNING).getSeverityCss());
    assertEquals("danger", new Message("", Severity.DANGER).getSeverityCss());
  }
}