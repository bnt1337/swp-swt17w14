package de.gardenmanager.restaurant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class CatererManagementTest {

  @Autowired
  private CatererManagement catererManagement;
  @Autowired
  private UserAccountManager userAccounts;

  private UserAccount user;

  @Before
  public void setUp() {
    user = catererManagement.createCustomer("HansWurst", "G3h31m!");
  }

  @Test
  public void createCustomer() {
    assertNotNull(user);
  }

  @Test
  public void save() {
    UserAccount userAccount = userAccounts.create("BerndJonas", "5up3rG3h31m!",
        Role.of("ROLE_CATERER"));
    assertEquals("BerndJonas", catererManagement.save(userAccount).getUsername());
  }

  @Test
  public void findAll() {
    assertNotNull(catererManagement.findAll());
  }
}
