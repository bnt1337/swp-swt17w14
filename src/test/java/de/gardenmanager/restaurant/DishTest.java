package de.gardenmanager.restaurant;

import static org.junit.Assert.assertEquals;
import static org.salespointframework.core.Currencies.EURO;

import de.gardenmanager.restaurant.Dish.DishType;
import org.javamoney.moneta.Money;
import org.junit.Test;

public class DishTest {

  @Test
  public void dish() {
    Dish dish = new Dish("Dishname", "allergens", Money.of(10.00, EURO), DishType.DESSERT);
    assertEquals("Dishname", dish.getName());
    assertEquals("allergens", dish.getAllergens());
    assertEquals(DishType.DESSERT, dish.getDishType());
    dish.setDishType(DishType.MAINCOURSE);
    assertEquals(DishType.MAINCOURSE, dish.getDishType());
  }
}
