package de.gardenmanager.restaurant;

import static org.junit.Assert.assertEquals;
import static org.salespointframework.core.Currencies.EURO;

import de.gardenmanager.restaurant.Dish.DishType;
import java.util.ArrayList;
import org.javamoney.moneta.Money;
import org.junit.Before;
import org.junit.Test;

public class MenuTest {

  private ArrayList<Dish> starterList = new ArrayList<>();
  private ArrayList<Dish> maincourseList = new ArrayList<>();
  private ArrayList<Dish> dessertList = new ArrayList<>();

  @Before
  public void setUp() {
    Dish dish1 = new Dish("starter1", "allergens", Money.of(10.00, EURO), DishType.STARTER);
    Dish dish2 = new Dish("starter2", "allergens", Money.of(10.00, EURO), DishType.STARTER);
    Dish dish3 = new Dish("starter3", "allergens", Money.of(10.00, EURO), DishType.STARTER);
    Dish dish4 = new Dish("starter4", "allergens", Money.of(10.00, EURO), DishType.STARTER);
    Dish dish5 = new Dish("main1", "allergens", Money.of(10.00, EURO), DishType.MAINCOURSE);
    Dish dish6 = new Dish("main2", "allergens", Money.of(10.00, EURO), DishType.MAINCOURSE);
    Dish dish7 = new Dish("main3", "allergens", Money.of(10.00, EURO), DishType.MAINCOURSE);
    Dish dish8 = new Dish("main4", "allergens", Money.of(10.00, EURO), DishType.MAINCOURSE);
    Dish dish9 = new Dish("dessert1", "allergens", Money.of(10.00, EURO), DishType.DESSERT);
    Dish dish10 = new Dish("dessert2", "allergens", Money.of(10.00, EURO), DishType.DESSERT);
    Dish dish11 = new Dish("dessert3", "allergens", Money.of(10.00, EURO), DishType.DESSERT);
    Dish dish12 = new Dish("dessert4", "allergens", Money.of(10.00, EURO), DishType.DESSERT);

    starterList.add(dish1);
    starterList.add(dish2);
    starterList.add(dish3);
    starterList.add(dish4);
    maincourseList.add(dish5);
    maincourseList.add(dish6);
    maincourseList.add(dish7);
    maincourseList.add(dish8);
    dessertList.add(dish9);
    dessertList.add(dish10);
    dessertList.add(dish11);
    dessertList.add(dish12);
  }

  @Test
  public void menuCtor() {
    Menu menu = new Menu("Menuname", starterList, maincourseList, dessertList);
    assertEquals("Menuname", menu.getName());
    assertEquals(menu.getStarters().size(), starterList.size());
    assertEquals(menu.getStarters().get(0), starterList.get(0));
    assertEquals(menu.getStarters().get(1), starterList.get(1));
    assertEquals(menu.getStarters().get(2), starterList.get(2));
    assertEquals(menu.getStarters().get(3), starterList.get(3));

    assertEquals(menu.getMainCourses().size(), maincourseList.size());
    assertEquals(menu.getMainCourses().get(0), maincourseList.get(0));
    assertEquals(menu.getMainCourses().get(1), maincourseList.get(1));
    assertEquals(menu.getMainCourses().get(2), maincourseList.get(2));
    assertEquals(menu.getMainCourses().get(3), maincourseList.get(3));

    assertEquals(menu.getDesserts().size(), dessertList.size());
    assertEquals(menu.getDesserts().get(0), dessertList.get(0));
    assertEquals(menu.getDesserts().get(1), dessertList.get(1));
    assertEquals(menu.getDesserts().get(2), dessertList.get(2));
    assertEquals(menu.getDesserts().get(3), dessertList.get(3));

    assertEquals(menu.getStatePublic(), false);
    menu.setStatePublic(true);
    assertEquals(menu.getStatePublic(), true);
  }
}
