package de.gardenmanager.restaurant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.salespointframework.core.Currencies.EURO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.javamoney.moneta.Money;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class RestaurantTest {

  @Autowired
  public Restaurant restaurant;

  @Before
  public void setUp() {

  }

  @Test
  public void createDish() {
    restaurant.createDish("Eingelegte Kellerstufen", "kann Spuren von Spuren enthalten",
        Money.of(5.30, EURO), "Hauptgericht");
    assertNotNull(restaurant.showDishs());
  }

  @Test
  public void deleteDish() {
    restaurant.createDish("Eingelegte Kellerstufen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    restaurant.createDish("Kandierte Straußeneier", "Schwein",
        Money.of(12.50, EURO), "Dessert");

    Iterable<Dish> dishs = restaurant.showDishs();
    Iterator it = dishs.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 2);

    restaurant.deleteDish("Eingelegte Kellerstufen");
    dishs = restaurant.showDishs();
    it = dishs.iterator();
    count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 1);
  }

  @Test
  public void getStarters() {
    restaurant.createDish("Eingelegte Kellerstufen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    restaurant.createDish("Pasteurisierte Schuhsolen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Vorspeise");
    restaurant.createDish("Blanchierte Krehenfüße", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Vorspeise");
    restaurant.createDish("gedünstete Blutwurst", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    assertNotNull(restaurant.showDishs());

    Iterable<Dish> dishs = restaurant.getStarters();
    restaurant.getStarters();

    Iterator it = dishs.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 2);
  }

  @Test
  public void getMainMenus() {
    restaurant.createDish("Eingelegte Kellerstufen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    restaurant.createDish("Pasteurisierte Schuhsolen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    restaurant.createDish("Blanchierte Krehenfüße", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Vorspeise");
    restaurant.createDish("gedünstete Blutwurst", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    assertNotNull(restaurant.showDishs());

    Iterable<Dish> dishs = restaurant.getMainCourses();
    restaurant.getStarters();

    Iterator it = dishs.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 2);
  }

  @Test
  public void getDesserts() {
    restaurant.createDish("Eingelegte Kellerstufen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    restaurant.createDish("Pasteurisierte Schuhsolen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    restaurant.createDish("Blanchierte Krehenfüße", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    restaurant.createDish("gedünstete Blutwurst", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    assertNotNull(restaurant.showDishs());

    Iterable<Dish> dishs = restaurant.getDesserts();
    restaurant.getStarters();

    Iterator it = dishs.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 4);
  }

  @Test
  public void add2Menu() {
    Dish d1 = restaurant.createDish("Tomatensuppe", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d2 = restaurant.createDish("Antipasti", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d3 = restaurant.createDish("Knallerbsen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d4 = restaurant.createDish("Safrantorte", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d5 = restaurant.createDish("Pizza", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d6 = restaurant.createDish("Pasta", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d7 = restaurant.createDish("Lasagne", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d8 = restaurant.createDish("Spiegelei", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d9 = restaurant.createDish("Erdbeerjogurt", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d10 = restaurant.createDish("Quarkspeise", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d11 = restaurant.createDish("Apfelmus", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d12 = restaurant.createDish("Birnen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");

    List<Dish> starters = new ArrayList<>();
    List<Dish> maincourses = new ArrayList<>();
    List<Dish> desserts = new ArrayList<>();

    starters.add(d1);
    starters.add(d2);
    starters.add(d3);
    starters.add(d4);
    maincourses.add(d5);
    maincourses.add(d6);
    maincourses.add(d7);
    maincourses.add(d8);
    desserts.add(d9);
    desserts.add(d10);
    desserts.add(d11);
    desserts.add(d12);

    restaurant.createMenuByObjects("Italienische Woche", starters, maincourses, desserts);
    assertNotNull(restaurant.showMenus());

    Iterable<Menu> menu = restaurant.showMenus();

    Iterator it = menu.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 1);

    //Test MenuCreation by String List
    List<String> stringStarters = new ArrayList<>();
    List<String> stringMaincourses = new ArrayList<>();
    List<String> stringDesserts = new ArrayList<>();

    stringStarters.add(d1.getName());
    stringStarters.add(d2.getName());
    stringStarters.add(d3.getName());
    stringMaincourses.add(d5.getName());
    stringMaincourses.add(d6.getName());
    stringMaincourses.add(d7.getName());
    stringDesserts.add(d9.getName());
    stringDesserts.add(d10.getName());
    stringDesserts.add(d11.getName());

    try {
      restaurant.createMenu("Italienische Spezialitäten",
          stringStarters, stringMaincourses, stringDesserts);
    } catch (Exception e) {
    }
  }

  @Test
  public void deleteMenu() {
    Dish d1 = restaurant.createDish("Tomatensuppe", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d2 = restaurant.createDish("Antipasti", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d3 = restaurant.createDish("Knallerbsen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d4 = restaurant.createDish("Safrantorte", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d5 = restaurant.createDish("Pizza", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d6 = restaurant.createDish("Pasta", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d7 = restaurant.createDish("Lasagne", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d8 = restaurant.createDish("Spiegelei", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d9 = restaurant.createDish("Erdbeerjogurt", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d10 = restaurant.createDish("Quarkspeise", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d11 = restaurant.createDish("Apfelmus", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d12 = restaurant.createDish("Birnen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");

    List<Dish> starters = new ArrayList<>();
    List<Dish> maincourses = new ArrayList<>();
    List<Dish> desserts = new ArrayList<>();

    starters.add(d1);
    starters.add(d2);
    starters.add(d3);
    starters.add(d4);
    maincourses.add(d5);
    maincourses.add(d6);
    maincourses.add(d7);
    maincourses.add(d8);
    desserts.add(d9);
    desserts.add(d10);
    desserts.add(d11);
    desserts.add(d12);

    restaurant.createMenuByObjects("Italienische Woche", starters, maincourses, desserts);

    assertNotNull(restaurant.showMenus());

    restaurant.deleteMenu("Italienische Woche");
    Iterable<Menu> menu = restaurant.showMenus();

    Iterator it = menu.iterator();
    int count = 0;
    while (it.hasNext()) {
      it.next();
      count++;
    }
    assertEquals(count, 0);
  }

  @Test
  public void testState() {
    Dish d1 = restaurant.createDish("Tomatensuppe", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d2 = restaurant.createDish("Antipasti", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d3 = restaurant.createDish("Knallerbsen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d4 = restaurant.createDish("Safrantorte", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d5 = restaurant.createDish("Pizza", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d6 = restaurant.createDish("Pasta", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d7 = restaurant.createDish("Lasagne", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d8 = restaurant.createDish("Spiegelei", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d9 = restaurant.createDish("Erdbeerjogurt", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d10 = restaurant.createDish("Quarkspeise", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d11 = restaurant.createDish("Apfelmus", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d12 = restaurant.createDish("Birnen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");

    List<Dish> starters = new ArrayList<>();
    List<Dish> maincourses = new ArrayList<>();
    List<Dish> desserts = new ArrayList<>();

    starters.add(d1);
    starters.add(d2);
    starters.add(d3);
    starters.add(d4);
    maincourses.add(d5);
    maincourses.add(d6);
    maincourses.add(d7);
    maincourses.add(d8);
    desserts.add(d9);
    desserts.add(d10);
    desserts.add(d11);
    desserts.add(d12);

    restaurant.createMenuByObjects("Italienische Woche", starters, maincourses, desserts);
    assertEquals(restaurant.getMenuState("Italienische Woche"), false);
    restaurant.setMenuState("Italienische Woche", true);
    assertEquals(restaurant.getMenuState("Italienische Woche"), true);
  }

  @Test
  public void testPublics() {

    Dish d1 = restaurant.createDish("Tomatensuppe", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d2 = restaurant.createDish("Antipasti", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d3 = restaurant.createDish("Knallerbsen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d4 = restaurant.createDish("Safrantorte", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Starter");
    Dish d5 = restaurant.createDish("Pizza", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d6 = restaurant.createDish("Pasta", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d7 = restaurant.createDish("Lasagne", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d8 = restaurant.createDish("Spiegelei", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Hauptgericht");
    Dish d9 = restaurant.createDish("Erdbeerjogurt", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d10 = restaurant.createDish("Quarkspeise", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d11 = restaurant.createDish("Apfelmus", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");
    Dish d12 = restaurant.createDish("Birnen", "kann Spuren von Spuren enthalten",
        Money.of(12.50, EURO), "Dessert");

    List<Dish> starters = new ArrayList<>();
    List<Dish> maincourses = new ArrayList<>();
    List<Dish> desserts = new ArrayList<>();

    starters.add(d1);
    starters.add(d2);
    starters.add(d3);
    starters.add(d4);
    maincourses.add(d5);
    maincourses.add(d6);
    maincourses.add(d7);
    maincourses.add(d8);
    desserts.add(d9);
    desserts.add(d10);
    desserts.add(d11);
    desserts.add(d12);

    restaurant.createMenuByObjects("Italienische Woche", starters, maincourses, desserts);
    restaurant.setMenuState("Italienische Woche", true);
    ArrayList<Dish> starterList = restaurant.showPublicStarters();
    assertEquals(starters, starterList);
    ArrayList<Dish> maincourseList = restaurant.showPublicMaincourses();
    assertEquals(maincourses, maincourseList);
    ArrayList<Dish> dessertList = restaurant.showPublicDesserts();
    assertEquals(desserts, dessertList);
  }
}
