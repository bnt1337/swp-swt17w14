package de.gardenmanager.application;

import static org.junit.Assert.fail;

import de.gardenmanager.person.Parcel;
import org.junit.Before;
import org.junit.Test;

public class ApplicationStateTest {

  Application application;

  @Before
  public void setUp() {
    application = new Application("Mustermann", "Max", "0351 0123456", "mustermann@meinemail.de",
        new Parcel("", 1));
  }

  @Test
  public void processNew() {
    try {
      application.process();
    } catch (Exception ignore) {
      fail("A new application should be processable");
    }
  }

  @Test
  public void processPending() {
    try {
      application.process();
      application.process();
      fail("An already processed application should not be processable");
    } catch (Exception ignore) {

    }
  }

  @Test
  public void processAccepted() {
    try {
      application.accept();
      application.process();
      fail("An accepted application should not be processable");
    } catch (Exception ignore) {

    }
  }

  @Test
  public void processDeclined() {
    try {
      application.decline();
      application.process();
      fail("A declined application should not be processable");
    } catch (Exception ignore) {

    }
  }

  @Test
  public void acceptNew() {
    try {
      application.accept();
    } catch (Exception ignore) {
      fail("A new application should be acceptable");
    }
  }

  @Test
  public void acceptPending() {
    try {
      application.process();
      application.accept();
    } catch (Exception ignore) {
      fail("A pending application should be acceptable");
    }
  }

  @Test
  public void acceptAccepted() {
    try {
      application.accept();
      application.accept();
      fail("An already accepted application should not be acceptable");
    } catch (Exception ignore) {

    }
  }

  @Test
  public void acceptDeclined() {
    try {
      application.decline();
      application.accept();
      fail("An declined application should not be acceptable");
    } catch (Exception ignore) {

    }
  }

  @Test
  public void declineNew() {
    try {
      application.decline();
    } catch (Exception ignore) {
      fail("A new application should be declinable");
    }
  }

  @Test
  public void declinePending() {
    try {
      application.process();
      application.decline();
    } catch (Exception ignore) {
      fail("A new application should be declinable");
    }
  }

  @Test
  public void declineAccepted() {
    try {
      application.accept();
      application.decline();
      fail("An accepted application should not be declinable");
    } catch (Exception ignore) {

    }
  }

  @Test
  public void declineDeclined() {
    try {
      application.decline();
      application.decline();
      fail("An already declined application should not be declinable");
    } catch (Exception ignore) {

    }
  }

  /*
  TODO: Test deleting applications when it's implemented.
   */
}
