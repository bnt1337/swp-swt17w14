package de.gardenmanager.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.google.common.collect.Iterables;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import de.gardenmanager.validation.ApplicationCreationForm;
import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bennet on 02.01.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ApplicationManagementTest {

  @Autowired
  private ApplicationManagement applicationManagement;
  @Autowired
  private ApplicationRepository applicationRepository;

  @Autowired
  private PersonManagement personManagement;

  @Autowired
  private UserAccountManager userAccountManager;

  private ApplicationCreationForm form1;

  private ApplicationCreationForm form2;

  @Before
  @Transactional
  @Commit
  public void setUp() {
    form1 = new ApplicationCreationForm();
    form1.setParcel("A1");
    form1.setEmail("test@example.com");
    form1.setFirstname("hans");
    form1.setName("wurst");
    form1.setTelephone("1231212312");

    form2 = new ApplicationCreationForm();
    form2.setParcel("AKLSHFDP");
    form2.setEmail("test@example.com");
    form2.setFirstname("hans");
    form2.setName("wurst");
    form2.setTelephone("1231212312");

    personManagement.createPerson("", "", LocalDate.now().minusYears(35),
        new Address("AeCh6ong8Kil1chai5aiX8OhYe2Tee", "", "", ""),
        new ContactInfo("e@mail.com", ""),
        userAccountManager.create("u1", "p", Roles.ROLE_CHAIRMAN));
  }

  @Test
  @Transactional
  @Rollback
  public void createApplication() {
    assertEquals(0, Iterables.size(applicationRepository.findAll()));
    assertNotNull(applicationManagement.createApplication(form1));
    assertEquals(1, Iterables.size(applicationRepository.findAll()));
    assertNull(applicationManagement.createApplication(form2));
    assertEquals(1, Iterables.size(applicationRepository.findAll()));
  }

  @Test
  @Transactional
  @Rollback
  public void deleteApplicationSuccess() {
    assertEquals(0, Iterables.size(applicationRepository.findAll()));
    Application a = applicationManagement.createApplication(form1);
    assertNotNull(a);
    assertEquals(1, Iterables.size(applicationRepository.findAll()));
    applicationManagement.deleteApplication(a.getId());
    assertEquals(0, Iterables.size(applicationRepository.findAll()));
  }

  @Test
  @Transactional
  @Rollback
  public void deleteApplicationFail() {
    applicationManagement.createApplication(form1);
    try {
      applicationManagement.deleteApplication(0);
    } catch (Exception e) {
    }
  }

  @Test
  @Transactional
  @Rollback
  public void acceptApplication() {
    assertEquals(0, Iterables.size(applicationRepository.findAll()));
    Application a = applicationManagement.createApplication(form1);
    assertNotNull(a);
    assert (a.getState() instanceof ApplicationStateNew);
    applicationManagement.acceptApplication(a.getId());
    assert (a.getState() instanceof ApplicationStateAccepted);
  }

  @Test
  @Transactional
  @Rollback
  public void declineApplication() {
    assertEquals(0, Iterables.size(applicationRepository.findAll()));
    Application a = applicationManagement.createApplication(form1);
    assertNotNull(a);
    assert (a.getState() instanceof ApplicationStateNew);
    applicationManagement.declineApplication(a.getId());
    assert (a.getState() instanceof ApplicationStateDeclined);
  }

  @Test
  @Transactional
  @Rollback
  public void save() {
    assertNotNull(applicationManagement.save(applicationManagement.createApplication(form1)));
  }

  @Test
  @Transactional
  @Rollback
  public void findAll() {
    applicationManagement.createApplication(form1);
    assertNotNull(applicationManagement.findAll());
  }

  @Test
  @Transactional
  @Rollback
  public void findOne() {
    Application a = applicationManagement.createApplication(form1);
    assertNotNull(applicationManagement.fineOne(a.getId()));
  }
}
