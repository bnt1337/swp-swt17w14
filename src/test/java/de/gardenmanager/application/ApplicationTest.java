package de.gardenmanager.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.gardenmanager.person.Parcel;
import org.junit.Before;
import org.junit.Test;

public class ApplicationTest {

  private Application testApplication;

  @Before
  public void setUp() {
    this.testApplication = new Application("Mustermann", "Max", "0351 0123456",
        "mustermann@meinemail.de", new Parcel("", 1));
  }

  @Test
  public void getName() {
    assertEquals(this.testApplication.getName(), "Mustermann");
  }

  @Test
  public void getFirstname() {
    assertEquals(this.testApplication.getFirstname(), "Max");
  }

  @Test
  public void getTelephone() {
    assertEquals(this.testApplication.getTelephone(), "0351 0123456");
  }

  @Test
  public void getEmail() {
    assertEquals(this.testApplication.getEmail(), "mustermann@meinemail.de");
  }

  @Test
  public void createApplication() {
    assertNotNull(this.testApplication);
  }
}
