package de.gardenmanager.request;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class RequestIdentifierTest {

  @Test
  public void creation() {
    RequestIdentifier ri = new RequestIdentifier();
    assertNotNull(ri);

    ri = new RequestIdentifier("3");
    assertNotNull(ri);
  }
}
