package de.gardenmanager.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonRepository;
import java.time.LocalDate;
import javax.transaction.Transactional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class RequestTest {

  @Autowired
  private PersonRepository personRepository;
  @Autowired
  private UserAccountManager userAccountManager;

  private Person p1;

  @Before
  public void setUp() {
    this.p1 = new Person("Firstname", "Lastname", LocalDate.now().minusYears(35),
        new Address("AeCh6ong8Kil1chai5aiX8OhYe2Tee", "", "", ""),
        new ContactInfo("", ""),
        userAccountManager.create("u1", "p", Role.of("EMPTY")));

    personRepository.save(this.p1);
  }

  @Test
  public void creation() {
    Request r = new Request(this.p1, "Bauantrag Laube",
        "Ich will meine Laube um 4 Etagen aufstocken.");
    assertNotNull(r);
  }

  @Test
  public void GetterSetter() {
    Request r = new Request(this.p1, "Bauantrag Laube",
        "Ich will meine Laube um 4 Etagen aufstocken.");
    assertNotNull(r.getId());
    assertEquals(this.p1, r.getRequestor());
    assertEquals("Firstname", r.getFirstname());
    assertEquals("Lastname", r.getLastname());
    assertEquals("Bauantrag Laube", r.getTitle());
    assertEquals("Ich will meine Laube um 4 Etagen aufstocken.", r.getText());
    assertEquals(RequestState.NEW, r.getState());
    r.setState(RequestState.READ);
    assertEquals(RequestState.READ, r.getState());
    r.setState(RequestState.NEW);
    assertEquals(RequestState.NEW, r.getState());
  }
}
