package de.gardenmanager.request;

import static org.junit.Assert.assertNotNull;

import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonRepository;
import de.gardenmanager.validation.RequestForm;
import java.time.LocalDate;
import javax.transaction.Transactional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class RequestManagementTest {

  @Autowired
  private PersonRepository personRepository;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private RequestRepository requestRepository;
  @Autowired
  private RequestManagement requestManagement;

  private Person p1;

  @Before
  public void setUp() {
    this.p1 = new Person("Firstname", "Lastname", LocalDate.now().minusYears(35),
        new Address("AeCh6ong8Kil1chai5aiX8OhYe2Tee", "", "", ""),
        new ContactInfo("", ""),
        userAccountManager.create("u1", "p", Role.of("EMPTY")));

    this.requestManagement = new RequestManagement(requestRepository);

    personRepository.save(this.p1);
  }

  @Test
  public void creation() {
    assertNotNull(requestManagement);
  }

  @Test
  public void createRequest() {
    Request r = this.requestManagement.createRequest("Bauantrag Laube",
        "Ich will meine Laube um 4 Etagen aufstocken.", this.p1);
    assertNotNull(r);
  }

  @Test
  public void testForm() {
    RequestForm form1 = new RequestForm();
    form1.setText("Bauantrag Laube");
    form1.setTitle("Ich will meine Laube um 4 Etagen aufstocken.");
    Request r = this.requestManagement.createRequest(form1, this.p1);
    assertNotNull(r);
  }

  @Test
  public void findAll() {
    this.requestManagement.createRequest("Bauantrag Laube",
        "Ich will meine Laube um 4 Etagen aufstocken.", this.p1);
    this.requestManagement.createRequest("Bauantrag Pool",
        "Ich benötige einen Pool zur Bewässerung meinerselbst.", this.p1);

    assertNotNull(this.requestManagement.findAll());
  }
}
