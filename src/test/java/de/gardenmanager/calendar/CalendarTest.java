package de.gardenmanager.calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;

import java.time.LocalDateTime;
import java.time.Month;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class CalendarTest {

  @Autowired
  private Calendar calendar;
  @Autowired
  private EventRepository eventRepository;
  @Autowired
  private EventRepository repository;

  @Before
  public void setUp() {
  }

  @Test
  public void createEventSuccess() {
    LocalDateTime start = LocalDateTime.of(2018, Month.MARCH, 14, 15, 0, 0);
    LocalDateTime end = LocalDateTime.of(2018, Month.MARCH, 14, 20, 0, 0);
    Event event1 = new Event("2.Vorstandssitzung",
        "Thema: Fleischfressende Pflanzen zulassen?", start, end);
    Event event2 = this.calendar.createEvent("2.Vorstandssitzung",
        "Thema: Fleischfressende Pflanzen zulassen?", start, end);

    assertEquals(event1.getName(), event2.getName());
    assertEquals(event1.getStart(), event2.getStart());
    assertEquals(event1.getEnd(), event2.getEnd());
    assertEquals(event1.getDescription(), event2.getDescription());
  }

  @Test
  @Transactional
  @Rollback
  public void createEventFailed() {
    LocalDateTime start = LocalDateTime.of(2018, Month.MARCH, 14, 15, 0, 0);
    LocalDateTime end = LocalDateTime.of(2018, Month.MARCH, 14, 20, 0, 0);
    try {
      this.calendar.createEvent("", "Thema: Fleischfressende Pflanzen zulassen?",
          start, end);
      fail("createEvent() should throw an IllegalArgumentException if the name is a void string!");
    } catch (IllegalArgumentException e) {
    }
  }

  @Test
  @Transactional
  @Rollback
  public void getEvents() {
    LocalDateTime start = LocalDateTime.of(2018, Month.MARCH, 14, 15, 0, 0);
    LocalDateTime end = LocalDateTime.of(2018, Month.MARCH, 14, 20, 0, 0);

    Event event1 = new Event("1.Vorstandssitzung",
        "Thema: Anbau von Bierbäumen zulässig?", start, end);
    this.calendar.createEvent("1.Vorstandssitzung",
        "Thema: Anbau von Bierbäumen zulässig?", start, end);

    start = LocalDateTime.of(2018, Month.MARCH, 15, 14, 0, 0);
    end = LocalDateTime.of(2018, Month.MARCH, 15, 21, 0, 0);
    Event event2 = new Event("2.Vorstandssitzung",
        "Thema: Fleischfressende Pflanzen zulassen?", start, end);
    this.calendar.createEvent("2.Vorstandssitzung",
        "Thema: Fleischfressende Pflanzen zulassen?", start, end);

    EventRepository testRepository = this.repository;
    testRepository.save(event1);
    testRepository.save(event2);

    assertEquals(testRepository, this.eventRepository);
  }

  @Test
  @Transactional
  @Rollback
  public void changeEvent() {
    LocalDateTime start = LocalDateTime.of(2018, Month.MARCH, 14, 15, 0, 0);
    LocalDateTime end = LocalDateTime.of(2018, Month.MARCH, 14, 20, 0, 0);
    Event event1 = new Event("1.Vorstandssitzung",
        "Thema: Fleischfressende Pflanzen zulassen?", start, end);

    this.calendar.createEvent("2.Vorstandssitzung",
        "Thema: Fleischfressende Pflanzen zulassen?", start, end);
    Long id = this.eventRepository.findByNameOrderByNameAsc("2.Vorstandssitzung").getId();

    Event event2 = this.calendar.changeEvent(id, "1.Vorstandssitzung", "",
        null, null, false, false);

    assertEquals(event1.getName(), event2.getName());
    assertEquals(event1.getStart(), event2.getStart());
    assertEquals(event1.getEnd(), event2.getEnd());
    assertEquals(event1.getDescription(), event2.getDescription());

    start = LocalDateTime.of(2018, Month.MARCH, 15, 14, 0, 0);
    end = LocalDateTime.of(2018, Month.MARCH, 15, 19, 0, 0);
    event1 = new Event("1.Vorstandssitzung",
        "Anbau von Bierbäumen zulässig??", start, end);
    event2 = this.calendar.changeEvent(id, "1.Vorstandssitzung",
        "Anbau von Bierbäumen zulässig?", start, end, false, false);

    assertEquals(event1.getName(), event2.getName());
    assertEquals(event1.getStart(), event2.getStart());
    assertEquals(event1.getEnd(), event2.getEnd());
    assertNotSame(event1.getDescription(), event2.getDescription());
  }

  @Test
  @Transactional
  @Rollback
  public void deleteEvent() {
    LocalDateTime start = LocalDateTime.of(2018, Month.MARCH, 14, 15, 0, 0);
    LocalDateTime end = LocalDateTime.of(2018, Month.MARCH, 14, 20, 0, 0);

    this.calendar.createEvent("2.Vorstandssitzung",
        "Thema: Fleischfressende Pflanzen zulassen?", start, end);

    this.calendar.deleteEvent("2.Vorstandssitzung");

    assertEquals(this.repository, this.eventRepository);
  }
}
