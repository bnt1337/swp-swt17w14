package de.gardenmanager.calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.Before;
import org.junit.Test;

public class EventTest {

  private Event testEvent;
  private LocalDateTime start;
  private LocalDateTime end;

  @Before
  public void setUp() {
    this.start = LocalDateTime.of(2018, Month.MARCH, 14, 15, 0, 0);
    this.end = LocalDateTime.of(2018, Month.MARCH, 14, 20, 0, 0);
    this.testEvent = new Event("Gartenparty", "Wir wollen chilln und grilln",
        start, end);
  }

  @Test
  public void createEvent() {
    assertNotNull(this.testEvent);
  }

  @Test
  public void getName() {
    assertEquals("Gartenparty", this.testEvent.getName());
  }

  @Test
  public void getDate() { // start time
    assertEquals(this.start, this.testEvent.getStart());
  }

  @Test
  public void getTime() { // end time
    assertEquals(this.end, this.testEvent.getEnd());
  }

  @Test
  public void getAssignment() { // end time
    assertEquals(null, this.testEvent.getAssignment());
  }

  @Test
  public void isPublished() { // end time
    assertEquals(false, this.testEvent.isPublished());
  }

  @Test
  public void getDescription() {
    assertEquals("Wir wollen chilln und grilln", this.testEvent.getDescription());
  }

  @Test
  public void startString() {
    assertEquals("14.03.2018 15:00", this.testEvent.startString());
  }

  @Test
  public void testToString() {
    assertEquals("Gartenparty (14.03 15:00 - 14.03 20:00)", this.testEvent.toString());
  }

  @Test
  public void testCompareTo() {
    Event e = new Event("Gartenparty", "Wir wollen chilln und grilln", start, end);
    assertEquals(0, this.testEvent.compareTo(e));
  }

  @Test
  public void setName() {
    String name = "Schnitzelfest";

    assertEquals(this.testEvent.getName(), "Gartenparty");
    this.testEvent.setName(name);
    assertEquals(this.testEvent.getName(), name);
  }

  @Test
  public void setDate() { // start time
    LocalDateTime start = LocalDateTime.of(2014, Month.SEPTEMBER, 14, 18, 50, 0);

    assertEquals(this.testEvent.getStart(), this.start);
    this.testEvent.setStart(start);
    assertEquals(this.testEvent.getStart(), start);
  }

  @Test
  public void setTime() { // end time
    LocalDateTime end = LocalDateTime.of(2020, Month.SEPTEMBER, 14, 18, 50, 0);

    assertEquals(this.testEvent.getEnd(), this.end);
    this.testEvent.setEnd(end);
    assertEquals(this.testEvent.getEnd(), end);
  }

  @Test
  public void setDescription() {
    String desription = "versuchs mal mit Gemütlichkeit";

    assertEquals(this.testEvent.getDescription(), "Wir wollen chilln und grilln");
    this.testEvent.setDescription(desription);
    assertEquals(this.testEvent.getDescription(), desription);
  }

  @Test
  public void end_before_start1() {
    LocalDateTime end = LocalDateTime.of(2014, Month.SEPTEMBER, 14, 18, 50, 0);

    try {
      this.testEvent.setEnd(end);
      fail("setEnd() should throw an InvalidParameterException if the end is before the start!");
    } catch (InvalidParameterException e) {
    }
  }

  @Test
  public void end_before_start2() {
    LocalDateTime start = LocalDateTime.of(2019, Month.SEPTEMBER, 14, 18, 50, 0);

    try {
      this.testEvent.setStart(start);
      fail("setEnd() should throw an InvalidParameterException if the end is before the start!");
    } catch (InvalidParameterException e) {
    }
  }

  @Test
  public void start_equal_end() {
    LocalDateTime end = this.start;

    try {
      this.testEvent.setEnd(end);
      fail("setEnd() should throw an InvalidParameterException if the end is equal to the start!");
    } catch (InvalidParameterException e) {
    }
  }

  @Test
  public void end_before_start_ctor() {
    LocalDateTime end = LocalDateTime.of(2014, Month.SEPTEMBER, 14, 18, 50, 0);

    try {
      new Event("Gartenparty", "Wir wollen chilln und grilln",
          start, end);
      fail("Event() should throw an InvalidParameterException if the end is before the start!");
    } catch (InvalidParameterException e) {
    }
  }

  @Test
  public void event_with_assignment() {
    Event e = new Event("Payrtey", "hier gehts ab", this.start, this.end,
        new Assignment(this.testEvent));
    assertNotNull(e);
  }
}
