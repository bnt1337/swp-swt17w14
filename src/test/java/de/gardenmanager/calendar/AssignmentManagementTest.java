package de.gardenmanager.calendar;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import com.google.common.collect.Iterables;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AssignmentManagementTest {

  @Autowired
  private AssignmentManagement assignmentManagement;
  @Autowired
  private Calendar calendar;
  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private WorkingHoursRepository workingHoursRepository;

  private Event event;

  @Before
  public void setUp() {
    event = calendar.createEvent("test", "demo", LocalDateTime.MIN, LocalDateTime.MAX);
  }

  @Test
  @Transactional
  @Rollback
  public void createAssignment() {
    assertEquals(0, assignmentManagement.findAllAssignments().stream().count());

    assignmentManagement.createAssignment(event);
    assertEquals(1, assignmentManagement.findAllAssignments().stream().count());

    Assignment a = assignmentManagement.findAllAssignments().stream().findAny().get();
    assertNotNull(a);

    assertSame(event, a.getEvent());
  }

  @Test
  @Transactional
  @Rollback
  public void applyForAssignment() {
    Assignment a = assignmentManagement.createAssignment(event);
    Person p = personManagement.createPerson("", "", LocalDate.MIN, new Address("", "", "", ""),
        new ContactInfo(null, ""), userAccountManager.create("user", "password", Role.of("TEST")));

    assignmentManagement.applyForAssignment(a, p, (short) 3);
    assertEquals(1, Iterables.size(workingHoursRepository.findAll()));
  }

  @Test
  @Transactional
  @Rollback
  public void validateAssignmentHours() {
    Assignment a = assignmentManagement.createAssignment(event);
    Person p1 = personManagement.createPerson("", "", LocalDate.MIN, new Address("", "", "", ""),
        new ContactInfo(null, ""), userAccountManager.create("user1", "password", Role.of("TEST")));
    Person p2 = personManagement.createPerson("", "", LocalDate.MIN, new Address("", "", "", ""),
        new ContactInfo(null, ""), userAccountManager.create("user2", "password", Role.of("TEST")));

    assignmentManagement.applyForAssignment(a, p1, (short) 3);
    try {
      assignmentManagement.validateAssignmentHours(a, p1, p1, (short) 3);
      fail("you should not be able to validate your own hours");
    } catch (IllegalArgumentException ignored) {
    }
    try {
      assignmentManagement.validateAssignmentHours(a, p1, p2, (short) 3);
    } catch (IllegalArgumentException ignored) {
      fail("hours should be validateable");
    }
  }
}
