package de.gardenmanager.calendar;

import static org.junit.Assert.fail;

import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Person;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.salespointframework.useraccount.UserAccount;

public class WorkinghoursTest {

  private WorkingHours workingHours;

  @Before
  public void setUp() {
    workingHours = new WorkingHours(
        new Assignment(
            new Event("", "", LocalDateTime.MIN, LocalDateTime.MAX)),
        new Person("", "", LocalDate.MIN,
            new Address("", "", "", ""),
            new ContactInfo("", ""),
            new UserAccount()
        ), (short) 3);
  }

  @Test
  public void validate() {
    try {
      workingHours.validate(new Person("", "", LocalDate.MIN,
          new Address("", "", "", ""),
          new ContactInfo("", ""),
          new UserAccount()
      ), (short) -1);
      fail("negative hours should fail");
    } catch (IllegalArgumentException ignored) {
    }
  }
}
