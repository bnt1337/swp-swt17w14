package de.gardenmanager.person;

import static org.junit.Assert.assertEquals;

import de.gardenmanager.validation.PersonCreationForm;
import org.junit.Before;
import org.junit.Test;

public class PersonRegistrationFormTest {

  @Before
  public void setUp() {

  }

  @Test
  public void testPasswordHashed() {
    PersonCreationForm form1 = new PersonCreationForm();
    form1.setFirstname("Max");
    form1.setLastname("Mustermann");
    form1.setStreet("Musterstraße");
    form1.setCity("Musterstadt");
    form1.setZipCode("01234");
    form1.setStreetNumber("1a");
    form1.setEmail("mm@example.com");
    form1.setUsername("mm");
    form1.setPassword("password");

    assertEquals("password", form1.getPassword());
    /*assertNotEquals(form1.getPassword(), form1.getPassword());

    String hash = form1.getPassword();
    assertEquals(Crypt.crypt("password", hash), hash);*/
  }

}