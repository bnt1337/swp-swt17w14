package de.gardenmanager.person;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.core.Streamable;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class PersonTest {

  @Autowired
  private PersonRepository personRepository;
  @Autowired
  private UserAccountManager userAccountManager;

  private Person p1;
  private Person p2;
  private Person p3;

  @Before
  public void setUp() {
    p1 = new Person("Firstname", "Lastname", LocalDate.now().minusYears(35),
        new Address("AeCh6ong8Kil1chai5aiX8OhYe2Tee", "", "", ""),
        new ContactInfo("", ""),
        userAccountManager.create("u1", "p", Role.of("EMPTY")));
    p2 = new Person("Bernd", "Jonas", LocalDate.now().minusYears(35),
        new Address("Oogiujeeho4aefigh7jae0uphiophi", "", "", ""),
        new ContactInfo("", ""),
        userAccountManager.create("u2", "p", Role.of("EMPTY")));
    p3 = new Person("Hans", "Wurst", LocalDate.now().minusYears(35),
        new Address("ushee5Fei9leigooful1ohr8Segee6", "", "", ""),
        new ContactInfo("", ""),
        userAccountManager.create("u3", "p", Role.of("EMPTY")));

    personRepository.save(p1);
    personRepository.save(p2);
    personRepository.save(p3);
  }

  @Test
  public void person() {
    personRepository.save(Arrays.asList(p1, p2, p3));

    assertEquals("AeCh6ong8Kil1chai5aiX8OhYe2Tee", p1.getAddress().getStreet());
    assertEquals("Oogiujeeho4aefigh7jae0uphiophi", p2.getAddress().getStreet());
    assertEquals("ushee5Fei9leigooful1ohr8Segee6", p3.getAddress().getStreet());
    assertEquals(new LinkedList<>(), p1.getDomain());
    assertEquals(new LinkedList<>(), p2.getDomain());
    assertEquals(new LinkedList<>(), p3.getDomain());
    assertEquals(LocalDate.now().minusYears(35), p1.getBirthday());
    assertEquals(LocalDate.now().minusYears(35), p2.getBirthday());
    assertEquals(LocalDate.now().minusYears(35), p3.getBirthday());
    assertEquals("Lastname, Firstname", p1.toString());
    assertEquals("Jonas, Bernd", p2.toString());
    assertEquals("Wurst, Hans", p3.toString());
    assert (Streamable.of(personRepository.findAll()).stream()
        .anyMatch(p -> p.getAddress().getStreet().equals("AeCh6ong8Kil1chai5aiX8OhYe2Tee")));
    assert (Streamable.of(personRepository.findAll()).stream()
        .anyMatch(p -> p.getAddress().getStreet().equals("Oogiujeeho4aefigh7jae0uphiophi")));
    assert (Streamable.of(personRepository.findAll()).stream()
        .anyMatch(p -> p.getAddress().getStreet().equals("ushee5Fei9leigooful1ohr8Segee6")));
  }

  @Test
  public void removeParcel() {
    Parcel p = new Parcel("ort", 255);
    p1.addParcel(p);
    List<Parcel> l = new LinkedList<>();
    l.add(p);
    assertEquals(l, p1.getDomain());

    l.remove(p);
    p1.removeParcel(p);
    assertEquals(l, p1.getDomain());
  }
}
