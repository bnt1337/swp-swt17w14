package de.gardenmanager.person;

import static org.junit.Assert.assertSame;

import org.junit.Test;

/**
 * Created by bennet on 02.01.2018.
 */
public class RolesTest {

  @Test
  public void byName() {
    assertSame(Roles.ROLE_CEO, Roles.byName("ROLE_CEO"));
    assertSame(Roles.ROLE_TREASURER, Roles.byName("ROLE_TREASURER"));
    assertSame(Roles.ROLE_CHAIRMAN, Roles.byName("ROLE_CHAIRMAN"));
    assertSame(Roles.ROLE_TENANTRY, Roles.byName("ROLE_TENANTRY"));
    assertSame(Roles.ROLE_TENANTRY, Roles.byName("ANY_OTHER_ROLE"));
  }
}
