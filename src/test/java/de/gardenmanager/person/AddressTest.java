package de.gardenmanager.person;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AddressTest {

  private Address address;

  @Before
  public void setUp() {
    this.address = new Address("street", "city", "01234", "2b");
  }

  @Test
  public void Adress() {
    assertEquals("street", address.getStreet());
    assertEquals("city", address.getCity());
    assertEquals("01234", address.getZipCode());
    assertEquals("2b", address.getStreetNumber());

    address.setStreet("Baumholzweg");
    address.setCity("Urwald");
    address.setZipCode("43210");
    address.setStreetNumber("300");

    assertEquals("Baumholzweg", address.getStreet());
    assertEquals("Urwald", address.getCity());
    assertEquals("43210", address.getZipCode());
    assertEquals("300", address.getStreetNumber());
  }
}
