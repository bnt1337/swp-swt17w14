package de.gardenmanager.person;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ContactInfoTest {

  private ContactInfo contactInfo;

  @Before
  public void setUp() {
    this.contactInfo = new ContactInfo("e@mail.com", "01234567890");
  }

  @Test
  public void ContactInfo() {
    assertEquals("e@mail.com", contactInfo.getEmail());
    assertEquals("01234567890", contactInfo.getPhoneNumber());

    contactInfo.setEmail("online@web.now");
    contactInfo.setPhoneNumber("09876543210");

    assertEquals("online@web.now", contactInfo.getEmail());
    assertEquals("09876543210", contactInfo.getPhoneNumber());
  }
}
