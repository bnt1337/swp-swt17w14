package de.gardenmanager.person;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import de.gardenmanager.validation.PersonCreationForm;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bennet on 02.01.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PersonManagementTest {

  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private ParcelRepository parcelRepository;

  private UserAccount useraccount;

  @Test
  @Transactional
  @Rollback
  public void createPerson() {
    long cnt = personManagement.findAll().count();
    useraccount = userAccountManager.create("user", "password", Role.of("DEF"));
    personManagement
        .createPerson("", "Lastname", LocalDate.MIN, new Address("", "", "", ""),
            new ContactInfo("", ""),
            useraccount);
    assertEquals(cnt + 1, personManagement.findAll().count());
  }

  @Test
  @Transactional
  @Rollback
  public void createPerson1() {
    Parcel parcel = parcelRepository.save(new Parcel("here", 200));
    PersonCreationForm form = new PersonCreationForm();
    form.setFirstname("");
    form.setLastname("Lastname");
    form.setBirthday(LocalDate.MIN);
    form.setPhoneNumber("");
    form.setEmail("");
    form.setCity("");
    form.setPassword("pass");
    form.setUsername("user");
    form.setZipCode("12321");
    form.setStreet("");
    form.setStreetNumber("");
    form.setParcel(parcel.getId());
    assertEquals(parcel.getId(), (long) form.getParcel());

    long cnt = personManagement.findAll().count();
    Person p = personManagement.createPerson(form);
    assertEquals(cnt + 1, personManagement.findAll().count());
    assertTrue(personManagement.findOne(p.getId()).getUserAccount().hasRole(Roles.ROLE_TENANTRY));
  }

  @Test
  @Transactional
  @Rollback
  public void TestSave() {
    useraccount = userAccountManager.create("user_neu", "p455w0rd", Role.of("DEF"));
    Person person = personManagement
        .createPerson("", "", LocalDate.MIN, new Address("", "", "", ""), new ContactInfo("", ""),
            useraccount);
    assertNotNull(personManagement.save(person));

    // test for findByLastName and findByUserAccount
    assertNotNull(personManagement.findByLastname("Lastname"));
    assertNotNull(personManagement.findByUserAccount(useraccount));
  }
}
