package de.gardenmanager.person;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.core.Streamable;
import org.salespointframework.useraccount.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class ParcelTest {

  private Person person;
  private Address address;
  private ContactInfo contactInfo;

  @Autowired
  private ParcelRepository parcels;

  @Before
  public void setUp() {
    contactInfo = new ContactInfo("me@example.com", "0123456789");
    address = new Address("Musterweg", "Mustercity", "01234", "1a");
    person = new Person("", "", LocalDate.now().minusYears(35), address, contactInfo,
        new UserAccount());
  }

  @Test
  public void Parcel() {
    Parcel parcel = new Parcel("here", 200);
    assertEquals("here", parcel.getLocation());
    assertTrue(200 == parcel.getSquaremeters());
    assertTrue(200.0 == parcel.getBiasedSize("1"));

    parcel.setState(new ParcelStateLeased());
    assertSame("ParcelState.Leased", parcel.getState().toString());
  }

  @Test
  public void setCurrentOwner() {
    Parcel parcel = new Parcel("here", 200);
    assertNull(parcel.getCurrentOwner());
    parcel.setCurrentOwner(person);
    assertEquals(person, parcel.getCurrentOwner());

    parcel.setCurrentOwner(
        new Person("", "", LocalDate.now().minusYears(35),
            new Address("", "", "", ""),
            new ContactInfo("", ""), new UserAccount()));

    assertTrue(parcel.getPastOwners().contains(person));
  }

  @Test
  public void saveParcelToRepository() {
    Parcel parcel = new Parcel("here", 200);
    parcels.save(parcel);

    //LinkedList<Parcel> parcelLinkedList = new LinkedList<>();
    assert (Streamable.of(parcels.findAll()).stream().anyMatch(p -> parcel == p));
  }
}
