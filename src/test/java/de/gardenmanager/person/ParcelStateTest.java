package de.gardenmanager.person;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.salespointframework.useraccount.UserAccount;

public class ParcelStateTest {

  Parcel parcel;

  @Before
  public void setUp() {
    parcel = new Parcel("Dahinten vorne hinten links", 300);
  }

  @Test
  public void sellFree() {
    try {
      parcel.sell();
      fail("A free Parcel should not be sellable");
    } catch (Exception ignore) {

    }
  }

  @Test
  public void sellLeased() {
    try {
      parcel.lease(new Person("", "", LocalDate.now().minusYears(35), new Address("", "", "", ""),
          new ContactInfo("", ""),
          new UserAccount()));
      parcel.sell();
    } catch (Exception ignore) {
      fail("A leased Parcel should be sellable");
    }
  }

  @Test
  public void leaseFree() {
    assertEquals("ParcelState.ForLease", parcel.getState().toString());

    try {
      parcel.lease(new Person("", "", LocalDate.now().minusYears(35), new Address("", "", "", ""),
          new ContactInfo("", ""),
          new UserAccount()));
    } catch (Exception ignore) {
      fail("A free Parcel should be leaseable");
    }
  }

  @Test
  public void leaseLeased() {
    try {
      parcel.lease(new Person("", "", LocalDate.now().minusYears(35), new Address("", "", "", ""),
          new ContactInfo("", ""),
          new UserAccount()));
      parcel.lease(new Person("", "", LocalDate.now().minusYears(35), new Address("", "", "", ""),
          new ContactInfo("", ""),
          new UserAccount()));
      fail("A leased Parcel should not be leaseable");
    } catch (Exception ignore) {

    }

    assertEquals("ParcelState.Leased", parcel.getState().toString());
  }
}
