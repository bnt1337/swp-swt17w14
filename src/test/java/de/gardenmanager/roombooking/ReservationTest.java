package de.gardenmanager.roombooking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;

public class ReservationTest {

  private Reservation reservation;

  @Before
  public void setUp() {
    this.reservation = new Reservation(LocalDateTime.of(2017, 12, 12, 12, 0),
        LocalDateTime.of(2017, 12, 12, 17, 0), "Ramses II",
        "Mumienschieben auf dem Parkett");
  }

  @Test
  public void creation() {
    assertEquals("Mumienschieben auf dem Parkett", this.reservation.getDesciption());
    assertEquals("Ramses II", this.reservation.getOwner());
    assertEquals(LocalDateTime.of(2017, 12, 12, 12, 0), this.reservation.getStart());
    assertEquals(LocalDateTime.of(2017, 12, 12, 17, 0), this.reservation.getEnd());
    assertNotNull(this.reservation.getId());
  }

  @Test
  public void compare() {
    Reservation reservation1 = new Reservation(LocalDateTime.of(2017, 12, 12, 12, 0),
        LocalDateTime.of(2017, 12, 12, 17, 0), "Ramses II",
        "Mumienschieben auf dem Parkett");
    assertEquals(0, this.reservation.compareTo(reservation1));

    Reservation reservation2 = new Reservation(LocalDateTime.of(2017, 12, 13, 12, 0),
        LocalDateTime.of(2017, 12, 13, 17, 0), "Ramses II",
        "Mumienschieben auf dem Parkett");
    assertTrue(this.reservation.compareTo(reservation2) < 0);

    Reservation reservation3 = new Reservation(LocalDateTime.of(2017, 12, 10, 12, 0),
        LocalDateTime.of(2017, 12, 10, 17, 0), "Ramses II",
        "Mumienschieben auf dem Parkett");
    assertTrue(this.reservation.compareTo(reservation3) > 0);
  }
}
