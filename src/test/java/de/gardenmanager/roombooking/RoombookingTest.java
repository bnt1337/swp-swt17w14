package de.gardenmanager.roombooking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.List;
import org.hibernate.QueryParameterException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
@ActiveProfiles("test")
public class RoombookingTest {

  @Autowired
  public RoomBookingsystem rbs;

  @Before
  public void setUp() {

  }

  @Test
  public void createReservation() {
    assertNotNull(rbs.createReservation(LocalDateTime.of(2017, 12, 12, 12, 0),
        LocalDateTime.of(2017, 12, 12, 17, 0),
        "Mumienschieben auf dem Parkett", "Ramses II"));

    //collusion
    try {
      rbs.createReservation(LocalDateTime.of(2017, 12, 12, 12, 0),
          LocalDateTime.of(2017, 12, 11, 17, 0),
          "Schnitzeljagd durch den Saal", "Hans Wurst");
    } catch (QueryParameterException e) {
    }
  }

  @Test
  public void getAllReservations() {
    rbs.createReservation(LocalDateTime.of(2017, 12, 12, 12, 0),
        LocalDateTime.of(2017, 12, 12, 17, 0),
        "Mumienschieben auf dem Parkett", "Ramses II");
    rbs.createReservation(LocalDateTime.of(2017, 12, 10, 17, 0),
        LocalDateTime.of(2017, 12, 11, 12, 0),
        "Schnitzeljagd durch den Saal", "Hans Wurst");

    List<Reservation> reservations = rbs.getAll();

    assertEquals(2, reservations.size());
  }

  @Test
  public void getReservation() {
    Reservation r = rbs.createReservation(LocalDateTime.of(2017, 12, 12, 12, 0),
        LocalDateTime.of(2017, 12, 12, 17, 0),
        "Mumienschieben auf dem Parkett", "Ramses II");

    assertEquals(r, rbs.getReservation(r.getId()));
  }

  @Test
  public void deleteReservation() {
    rbs.createReservation(LocalDateTime.of(2017, 12, 12, 12, 0),
        LocalDateTime.of(2017, 12, 12, 17, 0),
        "Mumienschieben auf dem Parkett", "Ramses II");
    rbs.createReservation(LocalDateTime.of(2017, 12, 10, 17, 0),
        LocalDateTime.of(2017, 12, 11, 12, 0),
        "Schnitzeljagd durch den Saal", "Hans Wurst");

    rbs.deleteReservation(rbs.getAll().get(0).getId());
    assertEquals(1, rbs.getAll().size());
  }
}
