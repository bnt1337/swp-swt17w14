//package de.gardenmanager.accounting;
//
//import static org.junit.Assert.assertEquals;
//
//import java.time.Instant;
//import java.util.Date;
//import org.junit.Before;
//import org.junit.Test;
//import org.salespointframework.useraccount.UserAccount;
//
//import de.gardenmanager.person.Address;
//import de.gardenmanager.person.ContactInfo;
//import de.gardenmanager.person.Parcel;
//import de.gardenmanager.person.Person;
//
//public class InvoiceTest {
//
//	private Person person;
//	private Invoice invoice;
//
//	@Before
//	public void setUp() {
//		person = new Person("","", Date.from(Instant.now()),
//        new Address("Schlauchweg", "Dresden", "01120", "8"),
//        new ContactInfo("blablo@freemail.de", "01239393"), new UserAccount());
//
//		invoice = new Invoice(new Parcel("", 123));
//
//		invoice.setFlaecheGroesse(200);
//		invoice.setStromVerbrauch(480);
//		invoice.setWasserVerbrauch(30);
//	}
//
//	@Test
//	public void getFlaecheGroeße() {
//		double flaecheBetrag = invoice.getFleacheBetrag();
//		assertEquals(36, flaecheBetrag, 0.01);
//	}
//
//	@Test
//	public void getStromVerbrauch() {
//		double stromVerbrauchBetrag = invoice.getStromVerbrauchBetrag();
//		assertEquals(96, stromVerbrauchBetrag, 0.01);
//	}
//
//	@Test
//	public void gerWasserVerbrauch() {
//		double wasserVerbrauchBetrag = invoice.getWasserVerbrauchBetrag();
//		assertEquals(58.5, wasserVerbrauchBetrag, 0.01);
//	}
//
//	@Test
//	public void getSumme() {
//		double summe = invoice.getSumme();
//		assertEquals(230.20, summe, 0.01);
//	}
//}
