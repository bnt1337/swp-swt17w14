package de.gardenmanager.accounting.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItem;
import de.gardenmanager.person.Parcel;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by bennet on 02.01.2018.
 */
public class InvoiceTest {

  private Invoice i1;

  @Before
  public void setUp() {
    i1 = new Invoice(new Parcel("TEST1", 1), new InvoiceRun(LocalDate.now()));
  }


  @Test
  public void setInvoiceDate() {
    assertSame(null, i1.getInvoiceDate());
    try {
      i1.setInvoiceDate(LocalDateTime.MIN);
    } catch (IllegalAccessException ignored) {
      fail("invoice should not be read-only at this point");
    }
    assertEquals(LocalDateTime.MIN, i1.getInvoiceDate());
    try {
      i1.setInvoiceDate(LocalDateTime.MAX);
      fail("invoice date should be read-only at this point");
    } catch (IllegalAccessException ignored) {
    }
    assertEquals(LocalDateTime.MIN, i1.getInvoiceDate());
  }

  @Test
  public void addInvoiceItem() {
    assertEquals(0, i1.getInvoiceItems().size());
    try {
      i1.addInvoiceItem(new InvoiceItem("test", 1.0));
    } catch (IllegalAccessException ignored) {
      fail("invoice should not be read-only at this point");
    }
    try {
      i1.setInvoiceDate(LocalDateTime.MIN);
    } catch (IllegalAccessException ignored) {
      fail("invoice should not be read-only at this point");
    }
    try {
      i1.addInvoiceItem(new InvoiceItem("test", 1.0));
      fail("invoice should be read-only at this point");
    } catch (IllegalAccessException ignored) {
    }
  }
}
