package de.gardenmanager.accounting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import com.google.common.collect.Iterables;
import de.gardenmanager.accounting.invoice.Invoice;
import de.gardenmanager.accounting.invoice.InvoiceManagement;
import de.gardenmanager.accounting.invoice.InvoiceRepository;
import de.gardenmanager.accounting.invoice.InvoiceRun;
import de.gardenmanager.accounting.invoice.InvoiceRunRepository;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.MeteredPricing;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.MeteredPricingRepository;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Random;
import mockit.Mock;
import mockit.MockUp;
import mockit.internal.startup.Startup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salespointframework.core.Streamable;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AccountingSystemTest {

  private Parcel parcel1;
  private Parcel parcel2;
  private Parcel parcel3;

  private Person person1;
  private Person person2;

  private Person treasurer;

  @Autowired
  private PersonManagement persons;
  @Autowired
  private InvoiceRepository invoices;
  @Autowired
  private ParcelRepository parcels;
  @Autowired
  private AccountingSystem accounting;
  @Autowired
  private UserAccountManager userAccountManager;
  @Autowired
  private MeteredPricingRepository meteredPricingRepository;
  @Autowired
  private InvoiceManagement invoiceManagement;
  @Autowired
  private InvoiceRunRepository invoiceRunRepository;

  @Before
  public void setUp() {
    Random r = new Random(System.currentTimeMillis());
    parcel1 = Streamable.of(parcels.findAll()).stream().skip(r.nextInt(50)).findFirst().get();
    parcel2 = Streamable.of(parcels.findAll()).stream().filter(p -> !p.equals(parcel1)).skip(r
        .nextInt
        (50))
        .findFirst().get();
    parcel3 = Streamable.of(parcels.findAll()).stream().filter(p -> !p.equals(parcel1) && !p
        .equals(parcel2))
        .skip(r.nextInt(50)).findFirst().get();

    person1 = persons.createPerson("Hans", "Wurst", LocalDate.of(1976, 8, 17),
        new Address("Musterstraße", "Da", "01928", "15z"),
        new ContactInfo("hans@fleischerei.de", "123455"),
        userAccountManager.create("hans", "p", Roles.ROLE_TENANTRY));

    person2 = persons.createPerson("Herbert", "Herberdtsøn", LocalDate.of(1965, 3, 13),
        new Address("Musterstraße", "Malmö", "91283", "42q"),
        new ContactInfo("hh@web.de", "902834762"),
        userAccountManager.create("hh", "p", Roles.ROLE_TENANTRY));

    treasurer = persons.createPerson("Dagobert", "Duck", LocalDate.of(1975, 6, 23),
        new Address("n-te Straße", "Entenhausen", "66666", "1"),
        new ContactInfo("speicher@geld.de", "43852934752"),
        userAccountManager.create("geizhals", "p", Roles.ROLE_TREASURER));

    parcel1.lease(person1);
    parcel2.lease(person2);
    parcels.save(parcel1);
    parcels.save(parcel2);
  }

  @Test
  @Transactional
  @Rollback
  public void createInvoices() {
    accounting.createInvoices();
    assertNotEquals(0, Iterables.size(invoices.findAllByParcel(parcel1)));
    assertNotEquals(0, Iterables.size(invoices.findAllByParcel(parcel2)));
    assertEquals(0, Iterables.size(invoices.findAllByParcel(parcel3)));
  }

  @Test
  @Transactional
  @Rollback
  public void addMeterReadingOnTime() {
    accounting.createInvoices();
    try {
      Invoice invoice = Iterables.get(invoices.findAllByParcel(parcel1), 0);
      accounting.addMeterReading(
          meteredPricingRepository.save(new MeteredPricing("test", "test", 0.15, true)),
          invoice.getId(), 500);

      assert (invoice.getAmount() > (500 * 0.15));
      assertSame(parcel1, invoice.getParcel());
    } catch (IllegalAccessException e) {
      fail("time sould not be up yet: " + e.getMessage());
    }
  }

  @Test
  @Transactional
  @Rollback
  public void addMeterReadingPastTime() {
    Startup.initializeIfPossible();
    accounting.createInvoices();
    long future = LocalDateTime.now().plusDays(21).atZone(ZoneId.systemDefault()).toInstant()
        .toEpochMilli();
    try {
      new MockUp<System>() {
        @Mock
        long currentTimeMillis() {
          return future;
        }
      };

      Invoice invoice = Iterables.get(invoices.findAllByParcel(parcel1), 0);
      accounting.addMeterReading(
          meteredPricingRepository.save(new MeteredPricing("test", "test", 0.15, true)),
          invoice.getId(), 500);

      fail("time is up, but addMeterReading not failed");
    } catch (IllegalAccessException ignored) {
    }
  }

  @Test
  @Transactional
  @Rollback
  public void estimateMeterReading() throws IllegalAccessException {
    MeteredPricing water = meteredPricingRepository.findByTagAndActiveIsTrue("water");
    MeteredPricing electricity = meteredPricingRepository.findByTagAndActiveIsTrue("electricity");

    InvoiceRun run1 = new InvoiceRun(LocalDate.of(2012, 10, 30));
    InvoiceRun run2 = new InvoiceRun(LocalDate.of(2013, 10, 30));
    InvoiceRun run3 = new InvoiceRun(LocalDate.of(2014, 10, 30));
    InvoiceRun run4 = new InvoiceRun(LocalDate.of(2015, 10, 30));
    invoiceRunRepository.save(Arrays.asList(run1, run2, run3, run4));

    Invoice invoice1 = invoiceManagement.createInvoice(parcel1, run1);
    invoice1.setMeterReadingDue(LocalDateTime.MAX);
    invoices.save(invoice1);

    //First Invoice - Check minimum
    accounting.addMeterReading(water, invoice1.getId(), 150.75);
    accounting.signInvoice(invoice1.getId());

    assertEquals(150.75, invoice1.getInvoiceItems().stream().filter(ii ->
        ii.getTag().equals("water")).findFirst().get().getUnits(), 0.1);

    assertEquals((double)AccountingSystem.METER_READING_MIN_UNITS,
        invoice1.getInvoiceItems().stream().filter(ii ->
        ii.getTag().equals("electricity")).findFirst().get().getUnits(), 0.1);

    //more Invoices
    Invoice invoice2 = invoiceManagement.createInvoice(parcel1, run2);
    invoice2.setMeterReadingDue(LocalDateTime.MAX);
    invoices.save(invoice2);
    accounting.addMeterReading(water, invoice2.getId(), 163.45);
    accounting.addMeterReading(electricity, invoice2.getId(), 133.7);
    accounting.signInvoice(invoice2.getId());
    Invoice invoice3 = invoiceManagement.createInvoice(parcel1, run3);
    invoice3.setMeterReadingDue(LocalDateTime.MAX);
    invoices.save(invoice3);
    accounting.addMeterReading(water, invoice3.getId(), 195.37);
    accounting.addMeterReading(electricity, invoice3.getId(), 285.9);
    accounting.signInvoice(invoice3.getId());

    //a future invoice - check average
    Invoice invoice4 = invoiceManagement.createInvoice(parcel1, run4);
    invoice4.setMeterReadingDue(LocalDateTime.MAX);
    invoices.save(invoice4);
    accounting.signInvoice(invoice4.getId());

    invoices.findAll();

    // (150.75 + 163.45 + 195.37)/3 should be somewhere near 169.856666667
    assertEquals(169.856666667, invoice4.getInvoiceItems().stream().filter(ii ->
        ii.getTag().equals("water")).findFirst().get().getUnits(), 0.1);
    // (100 + 133.7 + 285.9)/3 should be somewhere near 173.2
    assertEquals(173.2, invoice4.getInvoiceItems().stream().filter(ii ->
        ii.getTag().equals("electricity")).findFirst().get().getUnits(), 0.1);
  }

  @Test
  @Transactional
  @Rollback
  public void maikPaid(){
    InvoiceRun run = new InvoiceRun(LocalDate.of(2015, 10, 30));
    invoiceRunRepository.save(run);

    Invoice invoice1 = invoiceManagement.createInvoice(parcel1, run);
    accounting.markPaid(invoice1.getId());

    assertNotNull(invoice1.getPaidBy());
  }
}
