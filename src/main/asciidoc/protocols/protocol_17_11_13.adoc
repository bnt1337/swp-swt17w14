= Protokoll Gruppe 14

Treffen am 13.11.2017

Ort:      Andreas-Pfitzmann-Bau, TU Dresden +
Beginn:   14:50 Uhr +
Ende:     16:20 Uhr

__Schriftführer:__ Hannes Lienig

*Nächstes Treffen:* +
17.11.2017, 11:10 Uhr, Andreas-Pfitzmann-Bau, TU Dresden

__Teilnehmer:__
//Tabellarisch oder Aufzählung, Kennzeichnung von Teilnehmern mit besonderer Rolle (z.B. Kunde)

- Rico Bergmann (Tutor/Kunde)
- Lukas Gnauck (SCRUM-Master)
- Hannes Lienig (Schriftführer)
- Bennet Becker
- Nicos Lentzsch
- David Klein


== Bemerkungen
Erste Gruppenverwarnung, Organisation der nächsten Woche und des nächsten Sprints besprochen, weil
Entwurfsphase noch nicht abgeschlossen war, Freitag nächstes Treffen zum Abschluss der Entwurfsphase,
Beginn der Implementierung ab dem 17.11.17

== Retrospektive des letzten Sprints
*Issue referenziert die Issue ID von GitHub*
// Wie ist der Status der im letzten Sprint erstellten Issues/veteilten Aufgaben?

// See http://asciidoctor.org/docs/user-manual/=tables
[option="headers"]
|===
|Issue |Aufgabe |Status |Bemerkung
|#10|Anwendungsprototyp|In Bearbeitung|-
|#11|Testplan|In Bearbeitung|-
|#12|Entwicklerdokumentation|In Bearbeitung|-
|#14|Entwurfsklassendiagramm|In Bearbeitung|-
|===


== Aktueller Stand
In den Diagrammen fehlte die Einbindung des SalespointFrameworks. Außerdem fehlten neue State-Maschines für die
Entwurfsphase und der Testplan wurde ungenügend beschrieben. Im Prototypen fehlte die Raumbuchung und beim erstellen von
Menüs entstand ein Bug, der die Anbindung des Backends ans Frontend erschwerte.

== Planung des nächsten Sprints
*Issue referenziert die Issue ID von GitHub*

// See http://asciidoctor.org/docs/user-manual/=tables
[option="headers"]
|===
|Issue |Titel |Beschreibung |Verantwortlicher |Status
|#10|Anwendungsprototyp|Schreiben eines großen Prototypen als Team|Alle|In Bearbeitung
|#11|Testplan|Testplan aufstellen|Alle|In Bearbeitung
|#12|Entwicklerdokumentation|Erstellen der Entwicklerdoumentation|Alle|In Bearbeitung
|#14|Entwurfsklassendiagramm|Erstellen des Entwurfsklassendiagramms|Alle|In Bearbeitung
|===
