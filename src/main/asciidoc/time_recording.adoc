= Zeiterfassung für Gruppe 14

Notieren Sie die Zeit in Minuten für jeden Entwickler und für Team-Treffen pro Woche (Wochengranularität).
Die verwendete Zeit beinhaltet auch das Lesen von Dokumentationen, Besprechungen mit Teamkollegen und sonstige auf das Praktikum bezogene Aufgaben.

// See http://asciidoctor.org/docs/user-manual/#tables
[option="headers"]
|===
|Woche |Lukas Gnauck|Nicos Lentzsch|David Klein|Bennet Becker|Hannes Lienig|Meeting
|1     |245         |216           |180        |156          |185          |90
|2     |180         |391           |625        |300          |330          |210
|3     |1200        |903           |930        |684          |870          |180
|4     |300         |481           |600        |259          |930          |180
|5     |1045        |418           |675        |622          |1440         |180
|6     |1654        |1113          |1110       |1174         |1100         |270
|7     |1200        |897           |855        |300          |900          |90
|8     |325         |537           |1110       |312          |330          |90
|9     |1210        |931           |-          |824          |937          |180
|10    |430         |585           |660        |1294         |523          |210
|11    |925         |653           |300        |4099         |1200         |0
|12    |730         |347           |-          |1238         |600          |90
|13    |730         |464           |-          |620          |320          |90
|Summe |10165       |7936          |  -        |11882        |9665         |-
|Stunden/Woche|782  |610,5         | -         |914          |743,5        |-
|Gesamtsumme|-      |-             |3050       |-            |-            |-
|===

Bitte tragen Sie am Ende des Praktikums noch folgende weitere Werte ein:
Summe der Arbeitsstunden pro Student:

SUMME(SPALTE(DevX))

Arbeitsstunden pro Woche pro Student:

SUMME(SPALTE(DevX)) / 13

Durchschnittliche Arbeitzeit aller Studenten pro Woche:

(SUMME(SPALTE(Dev1)) + SUMME(SPALTE(Dev2)) + SUMME(SPALTE(Dev3)) + SUMME(SPALTE(Dev4)) + SUMME(SPALTE(Dev5)) +SUMME(SPALTE(Dev6))) / 6
