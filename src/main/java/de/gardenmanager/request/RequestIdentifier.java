package de.gardenmanager.request;

import java.io.Serializable;
import javax.persistence.Embeddable;
import org.salespointframework.core.SalespointIdentifier;

@Embeddable
public final class RequestIdentifier extends SalespointIdentifier implements Serializable {

  private static final long serialVersionUID = 201712080708L;

  /**
   * ctor.
   */
  public RequestIdentifier() {
    super();
  }

  /**
   * ctor. with custom id.
   */
  public RequestIdentifier(String id) {
    super(id);
  }
}
