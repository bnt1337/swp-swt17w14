package de.gardenmanager.request;

/**
 * status for an request.
 */
public enum RequestState {
  NEW, READ
}
