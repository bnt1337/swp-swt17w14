package de.gardenmanager.request;

import de.gardenmanager.person.Person;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Request {

  @EmbeddedId
  @AttributeOverride(name = "id", column = @Column(name = "id"))
  private RequestIdentifier id = new RequestIdentifier();

  @ManyToOne
  private Person requestor;

  private String title;
  private String text;
  private String firstname;
  private String lastname;

  private RequestState state;

  /**
   * create a new request, setting it's {@link Request#state} to {@link RequestState#NEW}.
   *
   * @param requestor sender. must not be {@literal null}
   * @param title title
   * @param text text
   */
  public Request(Person requestor, String title, String text) {
    this.requestor = requestor;
    this.firstname = requestor.getFirstname();
    this.lastname = requestor.getLastname();
    this.title = title;
    this.text = text;
    this.state = RequestState.NEW;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private Request() {
    //Because Spring needs it :(
  }

  public RequestIdentifier getId() {
    return id;
  }

  public Person getRequestor() {
    return requestor;
  }

  public String getTitle() {
    return title;
  }

  public String getText() {
    return text;
  }

  public RequestState getState() {
    return state;
  }

  public void setState(RequestState state) {
    this.state = state;
  }

  public String getFirstname() {
    return this.firstname;
  }

  public String getLastname() {
    return this.lastname;
  }
}
