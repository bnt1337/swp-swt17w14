package de.gardenmanager.request;

import de.gardenmanager.person.Person;
import de.gardenmanager.validation.RequestForm;
import org.salespointframework.core.Streamable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RequestManagement {

  private RequestRepository requestRepository;

  /**
   * autowired ctor.
   */
  public RequestManagement(RequestRepository requestRepository) {
    this.requestRepository = requestRepository;
  }

  /**
   * create a new Request by the Form-Validation Form.
   *
   * @param form {@link RequestForm}
   * @param requestor sender must be passed seperately, because it's part of the formular.
   * @return created request.
   */
  public Request createRequest(RequestForm form, Person requestor) {
    return createRequest(form.getTitle(), form.getText(), requestor);
  }

  /**
   * create new request by attributes.
   */
  public Request createRequest(String title, String text, Person requestor) {
    return requestRepository.save(new Request(requestor, title, text));
  }

  /**
   * get all requests.
   */
  public Streamable<Request> findAll() {
    return Streamable.of(requestRepository.findAll());
  }
}
