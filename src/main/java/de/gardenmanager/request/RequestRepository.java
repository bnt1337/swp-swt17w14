package de.gardenmanager.request;

import de.gardenmanager.person.Person;
import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<Request, RequestIdentifier> {

  //Iterable<Request> findAllByDescriptionMatchesRegex(String s);

  //Iterable<Request> findAllByTitleMatchesRegex(String s);

  Iterable<Request> findByRequestor(Person p);

  /**
   * Retrieves an entity by its id.
   *
   * @param requestIdentifier must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  Request findOne(RequestIdentifier requestIdentifier);

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param requestIdentifier must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  boolean exists(RequestIdentifier requestIdentifier);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<Request> findAll();

  /**
   * Returns all instances of the type with the given IDs.
   */
  @Override
  Iterable<Request> findAll(Iterable<RequestIdentifier> requestIdentifiers);

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   */
  @Override
  long count();

  /**
   * Deletes the entity with the given id.
   *
   * @param requestIdentifier must not be {@literal null}.
   * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
   */
  @Override
  void delete(RequestIdentifier requestIdentifier);

  /**
   * Deletes a given entity.
   *
   * @throws IllegalArgumentException in case the given entity is {@literal null}.
   */
  @Override
  void delete(Request entity);

  /**
   * Deletes the given entities.
   *
   * @throws IllegalArgumentException in case the given {@link Iterable} is {@literal null}.
   */
  @Override
  void delete(Iterable<? extends Request> entities);

  /**
   * Deletes all entities managed by the repository.
   */
  @Override
  void deleteAll();
}
