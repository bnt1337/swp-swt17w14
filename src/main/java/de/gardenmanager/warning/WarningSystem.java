package de.gardenmanager.warning;

import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import java.time.LocalDateTime;
import org.salespointframework.core.Streamable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * this class handles the warnings (add / remove / get).
 */
@Component
@Transactional
public class WarningSystem {

  private final WarningRepository warningRepository;
  private final PersonManagement personManagement;

  /**
   * constructor of the class.
   *
   * @param warningRepository the warning repository object to access the warnings
   * @param personManagement autowired {@link PersonManagement}
   */
  public WarningSystem(WarningRepository warningRepository,
      PersonManagement personManagement) {
    this.warningRepository = warningRepository;
    this.personManagement = personManagement;
  }

  /**
   * creates a new warning and returns the created warning-object.
   *
   * @param date the date of issuing an warning
   * @param description an short description (why was the warning issued)
   * @param tenantId the tenant id to assign the warning
   * @param registrar the registrar who issued the warning (i.e. an chairman)
   * @return the created warning object
   */
  public Warning createWarning(LocalDateTime date, String description, long tenantId,
      Person registrar) {
    Person tenant = personManagement.findOne(tenantId);

    return this.warningRepository
        .save(new Warning(date, description, tenant, registrar));
  }

  /**
   * deletes the warning given by the specified id.
   *
   * @param id the id of the warning to delete
   */
  public void deleteWarning(Long id) {
    warningRepository.delete(id);
  }

  /**
   * deletes all warnings.
   */
  public void deleteAllWarnings() {
    this.warningRepository.deleteAll();
  }

  /**
   * creates a new warning and returns the created warning-object.
   *
   * @return an iterable about all existing warnings
   */
  public Streamable<Warning> showWarnings() {
    return Streamable.of(warningRepository.findAll());
  }

  /**
   * returns the warning specified by the Id.
   *
   * @param id the id to search for
   * @return the warning which was found
   */
  public Warning findOne(long id) {
    return warningRepository.findOne(id);
  }
}
