package de.gardenmanager.warning;

import de.gardenmanager.person.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * the repository to store the warnings.
 */
@Repository
public interface WarningRepository extends CrudRepository<Warning, Long> {

  /**
   * returns the warning specified by the Id.
   *
   * @param id the id to search for
   * @return the warning which was found
   */
  Warning findOne(Long id);

  Integer countAllByTenant(Person person);

  Iterable<Warning> findAllByTenant(Person tenant);
}