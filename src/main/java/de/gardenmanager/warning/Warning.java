package de.gardenmanager.warning;

import de.gardenmanager.person.Person;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * this class describes an  warning-object.
 */
@Entity
public class Warning {

  @Id
  @GeneratedValue
  private long id;
  @Column(columnDefinition = "TIMESTAMP")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime date;
  private String description;
  @ManyToOne
  private Person tenant;
  @ManyToOne
  private Person registrar;

  /**
   * constructor of the class.
   *
   * @param date the date where the warning was issued
   * @param description an short description (why was the warning issued)
   * @param tenant the tenant to assign the warning
   * @param registrar the registrar who issued the warning (i.e. an chairman)
   */
  public Warning(LocalDateTime date, String description, Person tenant, Person registrar) {
    this.date = date;
    this.description = description;
    this.tenant = tenant;
    this.registrar = registrar;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private Warning() {
    //this my never be used. But Spring want's it
  }

  public long getId() {
    return this.id;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public String getDescription() {
    return description;
  }

  public Person getTenant() {
    return tenant;
  }

  public Person getRegistrar() {
    return registrar;
  }
}
