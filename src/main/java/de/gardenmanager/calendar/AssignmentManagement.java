package de.gardenmanager.calendar;

import de.gardenmanager.person.Person;
import org.salespointframework.core.Streamable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bennet on 28.12.2017.
 */
@Service
@Transactional
@Component
public class AssignmentManagement {

  private final AssignmentRepository assignmentRepository;
  private final WorkingHoursRepository workingHoursRepository;

  /**
   * autowired ctor.
   */
  public AssignmentManagement(
      AssignmentRepository assignmentRepository,
      WorkingHoursRepository workingHoursRepository) {
    this.assignmentRepository = assignmentRepository;
    this.workingHoursRepository = workingHoursRepository;
  }

  /**
   * create an new {@link Assignment} for an {@link Event}.
   *
   * @param event Corresponding Event
   * @return the assignment created.
   */
  public Assignment createAssignment(Event event) {
    return assignmentRepository.save(new Assignment(event));
  }

  /**
   * userapplication for an assignment.
   *
   * @param assignment to apply for.
   * @param person applicant.
   * @param hours desired hours.
   */
  public void applyForAssignment(Assignment assignment, Person person, short hours) {
    assignment.getWorkingHoursList()
        .add(workingHoursRepository.save(new WorkingHours(assignment, person, hours)));
    assignmentRepository.save(assignment);
  }

  /**
   * Validate hours for Person.
   *
   * @param assignment must not be {@literal null}
   * @param person {@link Person} to validate for
   * @param validator validating Chairman. must be unequal to <code>person</code>
   * @param hours hours to validate. forced positive with {@link Math#abs(int)}.
   */
  public void validateAssignmentHours(Assignment assignment, Person person, Person validator,
      short hours) {
    if (person == validator) {
      throw new IllegalArgumentException("can no validate own hours!");
    }
    workingHoursRepository.findByAssignmentAndAndPerson(assignment, person)
        .ifPresent(workingHours -> {
          workingHours.validate(validator, (short) Math.abs(hours));
          workingHoursRepository.save(workingHours);
        });
  }

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  public Streamable<Assignment> findAllAssignments() {
    return Streamable.of(assignmentRepository.findAll());
  }

  public Assignment findOneAssignment(Long id) {
    return assignmentRepository.findOne(id);
  }

  public void deleteWorkingHour(WorkingHours wh) {
    workingHoursRepository.delete(wh);
  }

  public void deleteAssignment(Assignment a) {
    assignmentRepository.delete(a);
  }
}
