package de.gardenmanager.calendar;

import de.gardenmanager.person.Person;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by bennet on 28.12.2017.
 */
@Repository
public interface WorkingHoursRepository extends CrudRepository<WorkingHours, Long> {

  /**
   * Saves a given entity. Use the returned instance for further operations as the save operation
   * might have changed the entity instance completely.
   *
   * @return the saved entity
   */
  @Override
  WorkingHours save(WorkingHours entity);

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  WorkingHours findOne(Long id);

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  boolean exists(Long id);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<WorkingHours> findAll();

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   */
  @Override
  long count();

  /**
   * Find all working hours by an assignment.
   */
  Iterable<WorkingHours> findAllByAssignment(Assignment assignment);

  /**
   * Get Person's WorkingHours for specific Assignment.
   *
   * @param assignment must not be {@literal null}.
   * @param person must no be {@literal null}.
   * @return WorkingHours or Empty.
   */
  Optional<WorkingHours> findByAssignmentAndAndPerson(Assignment assignment, Person person);
}
