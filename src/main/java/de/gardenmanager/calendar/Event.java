package de.gardenmanager.calendar;

import de.gardenmanager.util.LocalDateTimeConverter;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Event implements Comparable<Event> {

  @Id
  @GeneratedValue
  private long id;

  private String name;
  private boolean published;
  @Column(columnDefinition = "TIMESTAMP", name = "startdate")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime start;

  @Column(columnDefinition = "TIMESTAMP", name = "enddate")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime end;
  @Column(columnDefinition = "TEXT")
  private String description;

  @OneToOne
  private Assignment assignment;


  @Deprecated
  @SuppressWarnings("unused")
  private Event() {
    //Because Spring needs it :(
  }

  /**
   * Create new Event Object.
   *
   * @param name name
   * @param description description
   * @param start Start date and time
   * @param end End date and time
   * @param published event is public
   */
  public Event(String name, String description, LocalDateTime start, LocalDateTime end,
      Boolean published) {
    if (end.isBefore(start) || start.equals(end)) {
      throw new InvalidParameterException();
    }

    this.published = published;
    this.name = name;
    this.start = start;
    this.end = end;
    this.description = description;
  }

  /**
   * Create new Event Object with <code>published</code> set to {@literal false}.
   *
   * @param name name
   * @param description description
   * @param start Start date and time
   * @param end End date and time
   */
  public Event(String name, String description, LocalDateTime start, LocalDateTime end) {
    this(name, description, start, end, false);
  }

  /**
   * Create new Event Object with an assignment and <code>published</code> set to {@literal false}
   * because assignments are never public.
   *
   * @param name name
   * @param description description
   * @param start Start date and time
   * @param end End date and time
   * @param assignment reference
   */
  public Event(String name, String description, LocalDateTime start, LocalDateTime end,
      Assignment assignment) {
    this(name, description, start, end, false);
    this.assignment = assignment;
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDateTime getStart() {
    return start;
  }

  /**
   * Start start (with sanity check).
   *
   * @param start start to set
   */
  public void setStart(LocalDateTime start) {
    if (start.isAfter(end) || start.equals(end)) {
      throw new InvalidParameterException();
    }
    this.start = start;
  }

  public LocalDateTime getEnd() {
    return end;
  }

  /**
   * set end (with sanity check).
   *
   * @param end end to set
   */
  public void setEnd(LocalDateTime end) {
    if (end.isBefore(start) || start.equals(end)) {
      throw new InvalidParameterException();
    }
    this.end = end;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isPublished() {
    return published;
  }

  public void setPublished(boolean published) {
    this.published = published;
  }

  public Assignment getAssignment() {
    return assignment;
  }

  public void setAssignment(Assignment assignment) {
    this.assignment = assignment;
  }

  public boolean hasAssignment() {
    return assignment != null;
  }

  @Override
  public int compareTo(Event other) {
    return this.start.compareTo(other.start);
  }

  @Override
  public String toString() {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM HH:mm");
    return String
        .format("%s (%s - %s)", name, format.format(start), format.format(end));
  }

  /**
   * Format start Date.
   *
   * @return Formated <code>start</code>
   */
  public String startString() {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm");
    return String
        .format("%s", format.format(start));
  }

  /**
   * Format end Date.
   *
   * @return Formated <code>end</code>
   */
  public String endString() {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm");
    return String
        .format("%s", format.format(end));
  }
}
