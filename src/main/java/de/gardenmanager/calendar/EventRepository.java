package de.gardenmanager.calendar;

import java.time.LocalDateTime;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

  Event findByNameOrderByNameAsc(String name);

  Event findById(Long id);

  Iterable<Event> findAllByPublishedIsTrue();

  Iterable<Event> findAllByStartIsAfterAndAssignmentIsNotNull(LocalDateTime start);

  Iterable<Event> findAllByAssignmentIsNotNull();
}
