package de.gardenmanager.calendar;

import org.springframework.data.repository.CrudRepository;

public interface AssignmentRepository extends CrudRepository<Assignment, Long> {

  /**
   * Returns all instances of the type with the given IDs.
   */
  @Override
  Iterable<Assignment> findAll(Iterable<Long> longs);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<Assignment> findAll();

  /**
   * Find by Event.
   *
   * @param event {@link Event} to search by
   * @return Ordered Events
   */
  Assignment findByEventOrderByEventAsc(Event event);
}
