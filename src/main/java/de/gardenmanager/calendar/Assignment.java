package de.gardenmanager.calendar;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Assignment {

  @GeneratedValue
  @Id
  private Long id;

  @OneToOne
  private Event event;

  /*@ElementCollection
  @MapKeyColumn(name = "name")
  @Column(name = "workingHours")
  @CollectionTable(name = "workingHoursMap", joinColumns = @JoinColumn(name = "wh_id"))
  private Map<Person, WorkingHours> workingHoursMap;*/
  @OneToMany
  private Set<WorkingHours> workingHoursList;

  /**
   * Create assignment for Event.
   *
   * @param event {@link Event}
   */
  public Assignment(Event event) {
    this.event = event;
    event.setAssignment(this);
    //this.workingHoursMap = new HashMap<>();
    workingHoursList = new HashSet<>();
  }

  @Deprecated
  @SuppressWarnings("unused")
  private Assignment() {
    //Because Spring needs it :(
  }

  public Long getId() {
    return id;
  }

  public Event getEvent() {
    return event;
  }

  /*public Map<Person, WorkingHours> getWorkingHoursMap() {
    return workingHoursMap;
  }*/

  public Set<WorkingHours> getWorkingHoursList() {
    return workingHoursList;
  }

  @Override
  public String toString() {
    return event.toString();
  }
}
