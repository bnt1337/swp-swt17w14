package de.gardenmanager.calendar;

import de.gardenmanager.person.Person;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"person", "assignment"})
})
public class WorkingHours {

  public static final double MIN_HOURS_PER_YEAR = 4.0;
  public static final int AGE_THRESHOLD = 70;

  @ManyToOne
  @JoinColumn(name = "person")
  private final Person person;
  @ManyToOne
  @JoinColumn(name = "assignment")
  private final Assignment assignment;
  @Id
  @GeneratedValue
  @SuppressWarnings("unused")
  private Long id;
  private short hoursApplied;
  private short hoursDone;
  @ManyToOne
  private Person validator;

  WorkingHours(Assignment assignment, Person person,
      short hoursApplied) {
    this.assignment = assignment;
    this.person = person;
    this.hoursApplied = hoursApplied;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private WorkingHours() {
    //bla bla, No default constructor for entity, bla bla
    person = null;
    assignment = null;
  }

  /**
   * Validate ("sign") done Working hours.
   *
   * @param validator Chairman Validator
   * @param hoursDone hours to validate
   */
  public void validate(Person validator, short hoursDone) {
    if (validator == null || hoursDone < 0) {
      throw new IllegalArgumentException();
    }

    this.validator = validator;
    this.hoursDone = hoursDone;
  }

  public short getHoursApplied() {
    return this.hoursApplied;
  }

  public short getHoursDone() {
    return this.hoursDone;
  }

  public Person getValidator() {
    return this.validator;
  }

  public Person getPerson() {
    return person;
  }

  public Assignment getAssignment() {
    return assignment;
  }
}
