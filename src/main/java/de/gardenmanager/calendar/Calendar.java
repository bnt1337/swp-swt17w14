package de.gardenmanager.calendar;

import java.time.LocalDateTime;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Component
@Service
@Transactional
public class Calendar {

  private final EventRepository eventRepository;
  private final AssignmentManagement assignments;

  /**
   * Initialize the Calendar with AutoWired beans.
   */
  Calendar(EventRepository eventRepository, AssignmentManagement assignments) {
    this.eventRepository = eventRepository;
    this.assignments = assignments;
  }

  /**
   * Create a new Event with <code>published</code> set to {@literal false}.
   *
   * @param name Name
   * @param description Description
   * @param start Start Date and Time
   * @param end End Date and Time
   * @return Created Event Object
   */
  public Event createEvent(String name, String description, LocalDateTime start,
      LocalDateTime end) {
    return createEvent(name, description, start, end, false);
  }

  /**
   * Create a new Event.
   *
   * @param name Name
   * @param description Description
   * @param start Start Date and Time
   * @param end End Date and Time
   * @param publicvisible event is public
   * @return Created Event Object
   */
  public Event createEvent(String name, String description, LocalDateTime start, LocalDateTime end,
      Boolean publicvisible) {
    if (name.equals("")) {
      throw new IllegalArgumentException();
    }

    Event event = new Event(name, description, start, end, publicvisible);
    return this.eventRepository.save(event);
  }

  /**
   * Edit an existing Event.
   *
   * @param idOfEvent Id to Edit
   * @param name new Name
   * @param description new Description
   * @param start new Start
   * @param end new End
   * @return Modified Event
   */
  public Event changeEvent(Long idOfEvent, String name, String description,
      LocalDateTime start, LocalDateTime end, boolean assignment, boolean publicvisible) {

    Event editEvent = eventRepository.findOne(idOfEvent);

    /* if one of the parameters is null or a void string than we don't change something */
    if (editEvent == null) {
      return null;
    }

    editEvent.setName(name.equals("") ? editEvent.getName() : name);

    /* we must look here in which order we change the sates to avoid exceptions */
    if (start != null && end != null) {
      if (start.isBefore(editEvent.getEnd())) {
        editEvent.setStart(start);
        editEvent.setEnd(end);
      } else {
        editEvent.setEnd(end);
        editEvent.setStart(start);
      }
    } else if (start != null) {
      editEvent.setStart(start);
    } else if (end != null) {
      editEvent.setEnd(end);
    }

    editEvent.setDescription(description.equals("") ? editEvent.getDescription() : description);
    editEvent.setPublished(publicvisible);

    if (editEvent.hasAssignment() && !assignment) {

      editEvent.setPublished(false);
      //we have to delete the assignment
      Assignment a = editEvent.getAssignment();
      editEvent.setAssignment(null);
      eventRepository.save(editEvent);

      if (a.getWorkingHoursList() != null) {
        a.getWorkingHoursList().forEach(assignments::deleteWorkingHour);
      }
      assignments.deleteAssignment(a);
    } else if (!editEvent.hasAssignment() && assignment) {
      //we need to create a new assignment
      editEvent.setAssignment(assignments.createAssignment(editEvent));
    }

    return eventRepository.save(editEvent);
  }

  /**
   * Delete Event by name.
   *
   * @param name Event name
   */
  public void deleteEvent(String name) {
    if (name.equals("")) {
      throw new IllegalArgumentException();
    }

    this.eventRepository.delete(eventRepository.findByNameOrderByNameAsc(name));
  }

  public Assignment createAssignment(String name, String description, LocalDateTime start,
      LocalDateTime end) {

    return assignments.createAssignment(createEvent(name, description, start, end));
  }

  public Iterable<Event> showEvents() {
    return this.eventRepository.findAll();
  }
}
