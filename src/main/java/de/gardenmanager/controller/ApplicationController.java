package de.gardenmanager.controller;

import de.gardenmanager.application.ApplicationManagement;
import de.gardenmanager.controller.Message.Severity;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.validation.ApplicationCreationForm;
import javax.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class ApplicationController {

  private final ApplicationManagement applicationManagement;
  private final ParcelRepository parcelRepository;

  /**
   * ctor.
   */
  public ApplicationController(ApplicationManagement applicationManagement,
      ParcelRepository parcelRepository) {
    this.applicationManagement = applicationManagement;
    this.parcelRepository = parcelRepository;
  }


  /**
   * application form. (Frontend)
   */
  @GetMapping("/application")
  public String getApplication(@RequestParam long id, Model model) {
    ApplicationCreationForm applicationCreationForm = new ApplicationCreationForm();
    applicationCreationForm.setParcel(parcelRepository.findOne(id).getLocation());
    model.addAttribute("applicationCreationForm", applicationCreationForm);

    return "fe_application";
  }

  /**
   * Process Application HTTP Form. (Frontend)
   */
  @PostMapping("/application")
  public String postApplicationCreate(
      @ModelAttribute("applicationCreationForm") @Valid
          ApplicationCreationForm applicationCreationForm,
      BindingResult result, RedirectAttributes redirectAttributes) {

    if (result.hasErrors()) {
      return "fe_application";
    }

    if (applicationManagement.createApplication(applicationCreationForm) == null) {
      result.rejectValue("parcel", "applicationCreationForm.parcel.nonexsistent");
      return "fe_application";
    }
    redirectAttributes
        .addFlashAttribute("message", new Message("application.success", Severity.SUCCESS));
    return "redirect:/";
  }

  /**
   * application overview.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/application/view")
  public String getDashboardApplicationView(Model model) {
    model.addAttribute("applications", applicationManagement.findAll());
    return "db_application";
  }

  /**
   * delete an application.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/application/delete/{id}")
  public String getDashboardApplicationDeleteId(@PathVariable Long id) {
    applicationManagement.deleteApplication(id);
    return "redirect:/dashboard/application/view";
  }

  /**
   * accept an application.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/application/accept/{id}")
  public String getDashboardApplicationAcceptId(@PathVariable Long id,
      RedirectAttributes redirectAttributes) {
    applicationManagement.acceptApplication(id);

    redirectAttributes.addAttribute("application", id);

    return "redirect:/dashboard/accounts/add";
  }

  /**
   * decline an application.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/application/decline/{id}")
  public String getDashboardApplicationDeclineId(@PathVariable Long id) {
    applicationManagement.declineApplication(id);
    return "redirect:/application/view";
  }
}
