package de.gardenmanager.controller;

import de.gardenmanager.calendar.Assignment;
import de.gardenmanager.calendar.AssignmentManagement;
import de.gardenmanager.calendar.AssignmentRepository;
import de.gardenmanager.calendar.Calendar;
import de.gardenmanager.calendar.Event;
import de.gardenmanager.calendar.EventRepository;
import de.gardenmanager.controller.Message.Severity;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.validation.AssignmentApplicationForm;
import de.gardenmanager.validation.AssignmentValidationForm;
import de.gardenmanager.validation.EventCreationForm;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;
import javax.validation.Valid;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.web.LoggedIn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class EventController {

  private final Calendar calendar;
  private final EventRepository eventRepository;
  private final AssignmentRepository assignments;
  private final PersonManagement persons;
  private final AssignmentManagement assignmentManagement;

  /**
   * ctor.
   */
  public EventController(Calendar calendar,
      EventRepository eventRepository,
      AssignmentRepository assignments, PersonManagement persons,
      AssignmentManagement assignmentManagement) {
    this.calendar = calendar;
    this.eventRepository = eventRepository;
    this.assignments = assignments;
    this.persons = persons;
    this.assignmentManagement = assignmentManagement;
  }

  /**
   * list event in calendar.
   *
   * @param model model for view
   * @return view
   */
  @GetMapping("/dashboard/calendar")
  public String getDashboardCalendar(@RequestParam("assignments") Optional<String> assignments,
      Model model) {
    model.addAttribute("events", calendar.showEvents());
    String a = assignments.orElse("");

    if ("all".equals(a)) {
      model.addAttribute("events_a", eventRepository.findAllByAssignmentIsNotNull());

    } else {
      model.addAttribute("events_a", eventRepository.findAllByStartIsAfterAndAssignmentIsNotNull(
          LocalDateTime.now()));
    }
    return "db_calendar";
  }

  /**
   * http form for adding an event.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/events/add")
  public String getDashboardEventsAdd(Model model) {
    model.addAttribute("eventCreationForm", new EventCreationForm());
    return "db_addEvent";
  }

  /**
   * Process Event creation HTTP-Form.
   *
   * @param eventCreationForm form
   * @param result result
   * @return rdr
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @PostMapping("/dashboard/events/add")
  public String postDashboardEventsCreate(@Valid EventCreationForm eventCreationForm,
      BindingResult result) {
    if (result.hasErrors()) {
      return "db_addEvent";
    }

    try {
      if (eventCreationForm.getAssignment()) {
        calendar.createAssignment(eventCreationForm.getName(), eventCreationForm.getDescription(),
            eventCreationForm.getStart().toLocalDateTime(),
            eventCreationForm.getEnd().toLocalDateTime());
      } else {
        calendar.createEvent(eventCreationForm.getName(), eventCreationForm.getDescription(),
            eventCreationForm.getStart().toLocalDateTime(),
            eventCreationForm.getEnd().toLocalDateTime(), eventCreationForm.getPublicvisible());
      }
    } catch (InvalidParameterException e) {
      result.reject("events.add.invalid");
      return "db_addEvent";
    }

    return "redirect:/dashboard/calendar";
  }

  /**
   * Edit an existing Event (pre filling the form).
   *
   * @param id id
   * @param eventEditForm form
   * @return view
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/events/{id}/edit")
  public String getDashboardEventsIdEdit(EventCreationForm eventEditForm, RedirectAttributes
      redirectAttributes, Model model, @PathVariable(value = "id") Long id) {

    Event editEvent = this.eventRepository.findById(id);
    if (editEvent == null) {
      redirectAttributes.addFlashAttribute("message", new Message("event.edit.nonexsistent",
          Severity.WARNING));
      return "redirect:/dashboard/events/add";
    }

    if (editEvent.getStart().isBefore(LocalDateTime.now())) {
      redirectAttributes.addFlashAttribute("message", new Message("event.edit.ispast", Severity
          .DANGER));
      return "redirect:/dashboard/calendar";
    }

    model.addAttribute("id", editEvent.getId());

    eventEditForm.setName(editEvent.getName());
    eventEditForm.setDescription(editEvent.getDescription());

    eventEditForm.setStart(editEvent.getStart().atZone(ZoneOffset.UTC));
    eventEditForm.setEnd(editEvent.getEnd().atZone(ZoneOffset.UTC));

    eventEditForm.setAssignment(editEvent.hasAssignment());
    eventEditForm.setPublicvisible(editEvent.isPublished());

    model.addAttribute("eventEditForm", eventEditForm);

    return "db_editEvent";
  }

  /**
   * Edit an existing Event (pre filling the form).
   *
   * @param id id
   * @return view
   */

  @GetMapping("/dashboard/events/{id}")
  public String getDashboardEventsId(RedirectAttributes
      redirectAttributes, Model model, @PathVariable(value = "id") Long id) {

    Event editEvent = this.eventRepository.findById(id);
    if (editEvent == null) {
      redirectAttributes.addFlashAttribute("message", new Message("event.edit.nonexsistent",
          Severity.WARNING));
      return "redirect:/dashboard/events/add";
    }

    model.addAttribute("editEvent", editEvent);

    return "db_infoEvent";
  }

  /**
   * Process Edited Event.
   *
   * @param id id
   * @param eventEditForm form
   * @param result result
   * @return rdr
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @PostMapping("/dashboard/events/{id}/edit")
  public String postDashboardEventsIdEdit(
      @ModelAttribute("eventEditForm") @Valid EventCreationForm eventEditForm,
      BindingResult
          result, @PathVariable("id") Long id, Model model) {
    if (result.hasErrors()) {
      model.addAttribute("id", id);
      return "db_editEvent";
    }

    calendar.changeEvent(id, eventEditForm.getName(), eventEditForm.getDescription(),
        eventEditForm.getStart().toLocalDateTime(), eventEditForm.getEnd().toLocalDateTime(),
        eventEditForm.getAssignment(),
        eventEditForm.getPublicvisible());

    return "redirect:/dashboard/calendar";
  }

  /**
   * remove an event.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/events/remove/{name}")
  public String getDashboardEventsRemoveName(@PathVariable("name") String name) {
    calendar.deleteEvent(name);
    return "redirect:/dashboard/calendar";
  }

  /**
   * assignments overview.
   */
  @GetMapping("/dashboard/assignments")
  public String getDashboardAssignments(Model model) {
    model.addAttribute("assignments", assignments.findAll());
    return "db_assignments";
  }

  /**
   * assignment.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/assignments/{id}")
  public String getDashboardAssignmentsId(@PathVariable("id") Long id, Model model) {
    model.addAttribute("assignment", assignments.findOne(id));
    return "db_assignment";
  }

  /**
   * assignment application form.
   */
  @GetMapping("/dashboard/assignments/{assignmentid}/apply")
  public String getDashboardAssignmentsAssignmentidApply(Model model,
      @PathVariable("assignmentid") String assignmentid) {
    model.addAttribute("assignmentApplicationForm", new AssignmentApplicationForm());
    model.addAttribute("assignment", assignments.findOne(Long.parseLong(assignmentid)));
    return "db_applyAssignment";
  }

  /**
   * process assignment application.
   */
  @PostMapping("/dashboard/assignments/{assignmentid}/apply")
  public String postDashboardAssignmentsAssignmentidApplyApply(@Valid AssignmentApplicationForm
      assignmentApplicationForm, BindingResult result, Model model,
      @PathVariable("assignmentid") String assignmentid,
      @LoggedIn UserAccount userAccount,
      RedirectAttributes redirectAttributes) {
    if (result.hasErrors()) {
      model.addAttribute("assignment", assignments.findOne(Long.parseLong(assignmentid)));
      return "db_applyAssignment";
    }

    Person person = persons.findByUserAccount(userAccount);

    Assignment a = assignments.findOne(Long.parseLong(assignmentid));

    assignmentManagement.applyForAssignment(a, person, assignmentApplicationForm.getHours());

    redirectAttributes
        .addFlashAttribute("message", new Message(String.format("events.assignment.applied"
            + "('%s')", a), Severity.SUCCESS));

    return "redirect:/dashboard/assignments";
  }

  /**
   * form to validate hours. (chairman only)
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/assignments/{assignmentid}/validate/{personid}")
  public String getDashboardAssigmentsAssigmentidValidatePersonid(Model model,
      @PathVariable("assignmentid") String assignmentid,
      @PathVariable("personid") String personid) {
    model.addAttribute("assignmentValidationForm", new AssignmentValidationForm());
    model.addAttribute("personid", personid);
    model.addAttribute("assignmentid", assignmentid);
    return "db_validateAssignments";
  }

  /**
   * process validation form.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @PostMapping("/dashboard/assignments/{assignmentid}/validate/{personid}")
  public String postDashboardAssigmentsAssigmentidValidatePersonidValid(
      @ModelAttribute @Valid AssignmentValidationForm assignmentValidationForm,
      BindingResult result, @PathVariable("assignmentid") String assignmentid,
      @PathVariable("personid") String personid, @LoggedIn UserAccount userAccount, Model model,
      RedirectAttributes redirectAttributes) {

    if (result.hasErrors()) {
      model.addAttribute("personid", personid);
      model.addAttribute("assignmentid", assignmentid);
      return "db_validateAssignments";
    }

    Person validator = persons.findByUserAccount(userAccount);
    Person tanent = persons.findOne(Long.parseLong(personid));

    Assignment a = assignments.findOne(Long.parseLong(assignmentid));

    assignmentManagement
        .validateAssignmentHours(a, tanent, validator, assignmentValidationForm.getHoursDone());

    redirectAttributes.addFlashAttribute("message", new Message(String
        .format("events.assignment.validation('%d', '%s')", assignmentValidationForm.getHoursDone(),
            tanent), Severity.SUCCESS));

    return "redirect:/dashboard/assignments/" + assignmentid;
  }

  /**
   * list public events. (frontend)
   */
  @GetMapping("/events")
  public String getEvents(Model model) {
    model.addAttribute("events", eventRepository.findAllByPublishedIsTrue());
    return "fe_events";
  }

  @GetMapping("/events/{eventid}")
  public String getEventsEventid(@PathVariable("eventid") long id, Model model) {
    model.addAttribute("event", eventRepository.findById(id));
    model.addAttribute("events", eventRepository.findAllByPublishedIsTrue());
    return "fe_events_detail";
  }

}
