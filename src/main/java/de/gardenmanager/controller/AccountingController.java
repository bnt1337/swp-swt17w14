package de.gardenmanager.controller;

import de.gardenmanager.accounting.AccountingSystem;
import de.gardenmanager.accounting.invoice.Invoice;
import de.gardenmanager.accounting.invoice.InvoiceManagement;
import de.gardenmanager.accounting.invoice.InvoiceRepository;
import de.gardenmanager.accounting.invoice.InvoiceRun;
import de.gardenmanager.accounting.invoice.InvoiceRunRepository;
import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItem;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.MeteredPricingRepository;
import de.gardenmanager.controller.Message.Severity;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import de.gardenmanager.validation.MeterReadingForm;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.validation.Valid;
import org.salespointframework.core.Streamable;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.web.LoggedIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class AccountingController {

  @Autowired
  private InvoiceRepository invoiceRepository;
  @Autowired
  private InvoiceManagement invoiceManagement;
  @Autowired
  private AccountingSystem accountingSystem;
  @Autowired
  private PersonManagement personManagement;
  @Autowired
  private ParcelRepository parcelRepository;
  @Autowired
  private MeteredPricingRepository meteredPricingRepository;
  @Autowired
  private InvoiceRunRepository invoiceRunRepository;

  /**
   * Initialize the AccountingController with AutoWired beans.
   */
  @Autowired
  public AccountingController(InvoiceRepository invoiceRepository,
      InvoiceManagement invoiceManagement,
      AccountingSystem accountingSystem, PersonManagement personManagement,
      ParcelRepository parcelRepository,
      MeteredPricingRepository meteredPricingRepository,
      InvoiceRunRepository invoiceRunRepository) {
    this.invoiceRepository = invoiceRepository;
    this.invoiceManagement = invoiceManagement;
    this.accountingSystem = accountingSystem;
    this.personManagement = personManagement;
    this.parcelRepository = parcelRepository;
    this.meteredPricingRepository = meteredPricingRepository;
    this.invoiceRunRepository = invoiceRunRepository;
  }

  /**
   * overview of all Invoices and total meter reading of water and electricity. (filtered by user
   * role)
   */
  //@PreAuthorize("hasRole('ROLE_TREASURER')")
  @GetMapping("/dashboard/accounting")
  public String getDashboardAccounting(Model model, @LoggedIn UserAccount userAccount) {
    if (userAccount.hasRole(Roles.ROLE_TREASURER) || userAccount.hasRole(Roles.ROLE_CEO)
        || userAccount.hasRole(
        Role.of("ROLE_ADMIN"))) {

      InvoiceRun currIr = invoiceRunRepository.findFirstByOrderByDateAsc();
      HashMap<String, Double> totals = new HashMap<>();
      Streamable.of(meteredPricingRepository.findTagDistinctByActiveIsTrue()).stream()
          .filter(t -> !t.equals("lease"))
          .forEach(t ->
              totals.put(t, Streamable.of(invoiceRepository.findAllByInvoiceRun(currIr)).stream()
                  .mapToDouble(invoice ->
                      invoice.getInvoiceItems().stream()
                          .filter(invoiceItem -> invoiceItem.getTag().equals(t))
                          .mapToDouble(InvoiceItem::getUnits)
                          .sum())
                  .sum()));

      model.addAttribute("totals", totals);

      model.addAttribute("AllInvoices", invoiceRepository.findAll());
    } else {
      model.addAttribute("AllInvoices",
          invoiceRepository.findAllByParcel(
              parcelRepository.findByCurrentOwner(
                  personManagement.findByUserAccount(userAccount)
              )
          )
      );
    }
    return "db_accounting";
  }

  //  /**
  //   * show one invoice.
  //   */
  //  //@PreAuthorize("hasRole('ROLE_TREASURER')")
  //  @GetMapping("/dashboard/accounting/{invoice}")
  //  public String getDashboardAccountingInvoice(Model model, @PathVariable Long invoice,
  //      @LoggedIn UserAccount userAccount) {
  //    Invoice i = invoiceRepository.findOne(invoice);
  //    Person p = personManagement.findByUserAccount(userAccount);
  //    if (i.getParcel() != parcelRepository.findByCurrentOwner(p) && !userAccount
  //        .hasRole(Roles.ROLE_TREASURER)) {
  //      throw new AccessDeniedException("Insufficend Permissions!");
  //    }
  //    model.addAttribute("Invoice", i);
  //    return "db_invoice";
  //  }

  /**
   * sign invoice.
   */
  @PreAuthorize("hasRole('ROLE_TREASURER')")
  @GetMapping("/dashboard/accounting/{invoice}/sign")
  public String getDashboardAccountingInvoiceSign(@PathVariable Long invoice,
      RedirectAttributes redirectAttributes) {
    try {
      invoiceManagement.signInvoice(invoice);
    } catch (IllegalAccessException ignore) {
      redirectAttributes.addFlashAttribute("message", new Message("accounting.already_signed",
          Severity.DANGER));
    }

    return "redirect:/dashboard/accounting";
  }

  /**
   * mark invoice paid.
   */
  @PreAuthorize("hasRole('ROLE_TREASURER')")
  @GetMapping("/dashboard/accounting/{invoice}/paid")
  public String getDashboardAccountingInvoicePaid(@PathVariable Long invoice) {
    Invoice i = invoiceRepository.findOne(invoice);

    i.setPaidBy(LocalDateTime.now());
    invoiceRepository.save(i);

    return "redirect:/dashboard/accounting";
  }

  /**
   * write all invoiceRepository.
   *
   * @return the mapped resource
   */
  @PreAuthorize("hasRole('ROLE_TREASURER')")
  @GetMapping("/dashboard/accounting/createinvoices")
  public String getDashboardAccountingCreateinvoices() {
    accountingSystem.createInvoices();
    return "redirect:/dashboard/accounting";
  }

  /**
   * Form for adding meter reading.
   */
  @GetMapping("/dashboard/accounting/meterreading")
  public String getDashboardAccountingMeterreading(Model model, @LoggedIn UserAccount
      userAccount, RedirectAttributes redirectAttributes) {
    Person p = personManagement.findByUserAccount(userAccount);
    Parcel pa = parcelRepository.findByCurrentOwner(p);
    if (pa == null) {
      redirectAttributes.addFlashAttribute("message", new Message("accounting.noparcel",
          Severity.WARNING));
      return "redirect:/dashboard/accounting";
    }
    if (invoiceRepository.findByParcelAndInvoiceDateIsNull(pa) == null) {
      redirectAttributes.addFlashAttribute("message", new Message("accounting.noinvoice",
          Severity.WARNING));
      return "redirect:/dashboard/accounting";
    }
    model.addAttribute("meterReadingForm", new MeterReadingForm());
    return "db_addReadings";
  }

  /**
   * Process meter reading form.
   */
  @PostMapping("/dashboard/accounting/meterreading/add")
  public String postDashboardAccountingMeterreadingAdd(@Valid MeterReadingForm meterReadingForm,
      BindingResult bindingResult, @LoggedIn UserAccount userAccount,
      RedirectAttributes redirectAttributes) {
    AtomicBoolean s = new AtomicBoolean(false);

    invoiceRepository.findByParcelAndInvoiceDateIsNull(
        parcelRepository.findByCurrentOwner(personManagement.findByUserAccount(userAccount)))
        .ifPresent(i -> {
          try {
            accountingSystem
                .addMeterReading(meteredPricingRepository.findByTagAndActive("water", true),
                    i.getId(), meterReadingForm.getWater().doubleValue());
            accountingSystem
                .addMeterReading(meteredPricingRepository.findByTagAndActive("electricity", true),
                    i.getId(), meterReadingForm.getElectricity().doubleValue());
            s.set(true);
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
        });

    if (s.get() || bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute("message", new Message("meterreading.add.error",
          Severity.WARNING));
    } else {
      redirectAttributes.addFlashAttribute("message", new Message("meterreading.add.success",
          Severity.SUCCESS));
    }

    return "redirect:/dashboard/accounting";
  }

  @GetMapping("/dashboard/accounting/{invoices}/myinvoice")
  public String myInvoice(@PathVariable("invoices") String invoices, Model model) {
    Invoice invoice = invoiceManagement.findOne(Long.parseLong(invoices));

    model.addAttribute("myinvoice", invoice);

    return "db_invoices";
  }

}
