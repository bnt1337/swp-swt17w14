package de.gardenmanager.controller;

import de.gardenmanager.controller.Message.Severity;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import de.gardenmanager.request.Request;
import de.gardenmanager.request.RequestIdentifier;
import de.gardenmanager.request.RequestManagement;
import de.gardenmanager.request.RequestRepository;
import de.gardenmanager.request.RequestState;
import de.gardenmanager.validation.RequestForm;
import javax.validation.Valid;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.web.LoggedIn;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class RequestController {

  private RequestManagement requestManagement;
  private RequestRepository requestRepository;
  private PersonManagement personManagement;

  /**
   * Initialize the Request Controller with Autowired beans.
   */
  public RequestController(RequestRepository requestRepository, PersonManagement personManagement) {
    this.requestManagement = new RequestManagement(requestRepository);
    this.requestRepository = requestRepository;
    this.personManagement = personManagement;
  }

  /**
   * form for creating a request.
   */
  @PreAuthorize("hasRole('ROLE_TENANTRY')")
  @GetMapping("/dashboard/requests/add")
  public String getDashboardRequestAdd(Model model) {
    model.addAttribute("requestForm", new RequestForm());
    return "db_addRequest";
  }

  /**
   * Create a new Request and Redirect to Dashboard.
   */
  @PreAuthorize("hasRole('ROLE_TENANTRY')")
  @PostMapping("/dashboard/requests/new")
  public String postDashboardRequestNew(@ModelAttribute("requestForm") @Valid RequestForm form,
      BindingResult bindingResult, @LoggedIn UserAccount userAccount,
      RedirectAttributes redirectAttributes) {
    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute("messsage", new Message("request.add.error",
          Severity.DANGER));
    } else {
      Person p = personManagement.findByUserAccount(userAccount);
      requestManagement.createRequest(form, p);
      redirectAttributes.addFlashAttribute("messsage", new Message("request.add.success",
          Severity.SUCCESS));
    }
    return "redirect:/dashboard/requests";
  }

  /**
   * List Requests. (Own or all)
   */
  @PreAuthorize("hasRole('ROLE_TENANTRY')")
  @GetMapping("/dashboard/requests")
  public String getDashboardRequests(Model model, @LoggedIn UserAccount userAccount) {
    if (userAccount.hasRole(Roles.ROLE_CEO)) {
      model.addAttribute("requests", requestRepository.findAll());
      requestManagement.findAll().stream().filter(r -> r.getState() == RequestState.NEW)
          .forEach(r -> r.setState(RequestState.NEW));
    } else {
      Person p = personManagement.findByUserAccount(userAccount);
      model.addAttribute("requests", requestRepository.findByRequestor(p));
    }
    return "db_request";
  }

  /**
   * Open a single Request. CEOs can see all other only their own.
   */
  @PreAuthorize("hasRole('ROLE_TENANTRY')")
  @GetMapping("/dashboard/requests/{request}")
  public String getDashboardRequestsRequest(@PathVariable Request request, Model model,
      @LoggedIn UserAccount userAccount) {
    Request r = requestRepository.findOne(request.getId());
    if (r.getRequestor() != personManagement.findByUserAccount(userAccount) && !userAccount
        .hasRole(Roles.ROLE_CEO)) {
      throw new AccessDeniedException("Insufficend Permissions!");
    }
    if (userAccount.hasRole(Roles.ROLE_CEO)) {
      r.setState(RequestState.READ);
    }
    model.addAttribute("request", r);
    return "db_requestInfo";
  }

  /**
   * remove a Request.
   */
  @PreAuthorize("hasRole('ROLE_TENANTRY')")
  @GetMapping("/dashboard/requests/delete/{id}")
  public String getDashboardRequestsIdDelete(@PathVariable("id") RequestIdentifier id) {
    requestRepository.delete(id);
    return "redirect:/dashboard/requests";
  }

  /**
   * accept a Request.
   * @remarks this mapping is not used, because if an request is done it should be removed
   * @todo if required use this function to accept requests, otherwise remove this mapping
   */
  @PreAuthorize("hasRole('ROLE_TENANTRY')")
  @GetMapping("/dashboard/requests/accept/{id}")
  public String getDashboardRequestAccept(@PathVariable("id") RequestIdentifier id) {

    //TODO Accept Requests

    return "db_request";
  }
}
