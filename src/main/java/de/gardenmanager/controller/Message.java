package de.gardenmanager.controller;

public class Message {

  private String text;
  private Severity severity;

  public Message(String text, Severity severity) {
    this.text = text;
    this.severity = severity;
  }

  public String getText() {
    return text;
  }

  public Severity getSeverity() {
    return severity;
  }

  /**
   * Get the Bootstrap/AdminLTE CSS-Class suffix.
   *
   * @return css-class suffix
   */
  public String getSeverityCss() {
    String r;
    switch (severity) {
      case DANGER:
        r = "danger";
        break;
      case SUCCESS:
        r = "success";
        break;
      case WARNING:
        r = "warning";
        break;
      default:
        r = "info";
        break;
    }
    return r;
  }

  public enum Severity {
    INFO, SUCCESS, WARNING, DANGER
  }
}
