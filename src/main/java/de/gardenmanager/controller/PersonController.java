package de.gardenmanager.controller;

import com.google.common.collect.Streams;
import de.gardenmanager.application.Application;
import de.gardenmanager.application.ApplicationManagement;
import de.gardenmanager.controller.Message.Severity;
import de.gardenmanager.person.Address;
import de.gardenmanager.person.ContactInfo;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.ParcelStateForLease;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.PersonRepository;
import de.gardenmanager.person.Roles;
import de.gardenmanager.validation.ParcelDomainAddForm;
import de.gardenmanager.validation.PasswordChangeForm;
import de.gardenmanager.validation.PersonCreationForm;
import de.gardenmanager.validation.PersonRoleAddForm;
import de.gardenmanager.warning.WarningRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.salespointframework.useraccount.web.LoggedIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class PersonController {

  private final PersonManagement personManagement;
  private final PersonRepository personRepository;
  private final UserAccountManager userAccountManager;
  private final ApplicationManagement applicationManagement;
  private final ParcelRepository parcelRepository;
  private final WarningRepository warningRepository;
  private final MailSender mailSender;

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * Initialize the PersonController with Autowired Beans.
   */
  public PersonController(PersonManagement personManagement,
      PersonRepository personRepository,
      UserAccountManager userAccountManager,
      ApplicationManagement applicationManagement,
      ParcelRepository parcelRepository,
      WarningRepository warningRepository, MailSender mailSender) {
    this.personManagement = personManagement;
    this.personRepository = personRepository;
    this.userAccountManager = userAccountManager;
    this.applicationManagement = applicationManagement;
    this.parcelRepository = parcelRepository;
    this.warningRepository = warningRepository;
    this.mailSender = mailSender;
  }

  /**
   * List persons.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/accounts")
  public String getDashboardAccounts(Model model, @LoggedIn UserAccount userAccount) {
    if (userAccount.hasRole(Roles.ROLE_CEO) || userAccount.hasRole(Role.of("ROLE_ADMIN"))) {
      model.addAttribute("persons", personManagement.findAll().collect(Collectors.toList()));
    } else {
      Person chairman = personManagement.findByUserAccount(userAccount);
      Iterator<Long> pids = chairman.getDomain().stream()
          .map(p -> p.getCurrentOwner().getId()).iterator();
      model.addAttribute("persons", personManagement.findAll(() -> pids)
          .collect(Collectors.toList()));
    }
    model.addAttribute("warningRepository", warningRepository);
    return "db_person";
  }

  @PreAuthorize("hasRole('ROLE_CEO')")
  @GetMapping("/dashboard/accounts/chairman")
  public String getDashboardAccountsChairman(Model model) {
    model.addAttribute("chairmen", personManagement.findAll().filter(p -> p.getUserAccount()
        .hasRole(Roles.ROLE_CHAIRMAN)).collect(Collectors.toList()));

    List<Parcel> ap = personManagement.findAll().filter(p -> p.getUserAccount()
        .hasRole(Roles.ROLE_CHAIRMAN)).flatMap(person -> person.getDomain().stream()).collect(
        Collectors.toList());

    model.addAttribute("parcels", Streams.stream(parcelRepository.findAll()).filter(p -> ap
        .stream().noneMatch(pp -> pp == p)).collect(Collectors.toList()));

    model.addAttribute("parcelDomainAddForm", new ParcelDomainAddForm());
    return "db_person_chairman";
  }

  @PreAuthorize("hasRole('ROLE_CEO')")
  @PostMapping("/dashboard/accounts/chairman")
  public String postDashboardAccountsChairman(@Valid ParcelDomainAddForm parcelDomainAddForm) {
    Person c = personManagement.findOne(parcelDomainAddForm.getChairman());
    parcelDomainAddForm.getParcels().forEach(p -> {
      c.addParcel(parcelRepository.findOne(p));
    });
    personManagement.save(c);
    return "redirect:/dashboard/accounts/chairman";
  }


  /**
   * Form to create a new Person.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/accounts/add")
  public String getDashboardAccountsAdd(Model model,
      @RequestParam("application") Optional<Long> application, @LoggedIn UserAccount userAccount) {
    PersonCreationForm personCreationForm = new PersonCreationForm();
    application.ifPresent(ap -> {
      Application a = applicationManagement.fineOne(ap);
      personCreationForm.setEmail(a.getEmail());
      personCreationForm.setFirstname(a.getFirstname());
      personCreationForm.setLastname(a.getName());
      personCreationForm.setPhoneNumber(a.getTelephone());
      personCreationForm.setParcel(a.getParcel().getId());
    });
    model.addAttribute("personCreationForm", personCreationForm);
    if (userAccount.hasRole(Roles.ROLE_CHAIRMAN)) {
      Person chairman = personManagement.findByUserAccount(userAccount);
      model.addAttribute("parcels", chairman.getDomain().stream()
          .filter(p -> p.getState() instanceof ParcelStateForLease).collect(Collectors.toList()));
    } else {
      model.addAttribute("parcels", Streams.stream(parcelRepository.findAll())
          .filter(p -> p.getState() instanceof ParcelStateForLease).collect(Collectors.toList()));
    }

    return "db_addTenant";
  }

  /**
   * Admins and CEOs new add new Users.
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @PostMapping("/dashboard/accounts/add")
  public String postDashboardAccountsAdd(
      @ModelAttribute("personCreationForm") @Valid PersonCreationForm personCreationForm,
      BindingResult bindingResult, Model model, @LoggedIn UserAccount userAccount) {
    if (bindingResult.hasErrors()) {
      if (userAccount.hasRole(Roles.ROLE_CHAIRMAN)) {
        Person chairman = personManagement.findByUserAccount(userAccount);
        model.addAttribute("parcels", chairman.getDomain().stream()
            .filter(p -> p.getState() instanceof ParcelStateForLease).collect(Collectors.toList()));
      } else {
        model.addAttribute("parcels", Streams.stream(parcelRepository.findAll())
            .filter(p -> p.getState() instanceof ParcelStateForLease).collect(Collectors.toList()));
      }
      return "db_addTenant";
    }

    personManagement.createPerson(personCreationForm);

    return "redirect:/dashboard/accounts";
  }

  /**
   * Admins and CEOs may disable Users.
   */
  @PreAuthorize("hasRole('ROLE_CEO')")
  @GetMapping("/dashboard/accounts/{account}/disable")
  public String getDashboardAccountsAccountDisable(@PathVariable("account") String account) {
    Person p = personRepository.findOne(Long.parseLong(account));
    UserAccount u = p.getUserAccount();
    u.setEnabled(false);

    userAccountManager.save(u);
    personRepository.save(p);

    return "redirect:/dashboard/accounts";
  }

  /**
   * Admins and CEOs may enable Users.
   */
  @PreAuthorize("hasRole('ROLE_CEO')")
  @GetMapping("/dashboard/accounts/{account}/enable")
  public String getDashboardAccountsAccountEnable(@PathVariable("account") String account) {
    Person p = personRepository.findOne(Long.parseLong(account));
    UserAccount u = p.getUserAccount();
    u.setEnabled(true);

    userAccountManager.save(u);
    personRepository.save(p);

    return "redirect:/dashboard/accounts";
  }

  @GetMapping("/dashboard/accounts/{account}/sellparcel")
  public String getDashboardAccountsAccountSellparcel(@PathVariable("account") String account,
      @LoggedIn UserAccount userAccount) {
    Person p = personRepository.findOne(Long.parseLong(account));
    UserAccount u = p.getUserAccount();

    if (userAccount.hasRole(Role.of("ROLE_ADMIN")) || userAccount.hasRole(Roles.ROLE_CEO)
        || userAccount.hasRole(Roles.ROLE_CHAIRMAN) || userAccount == u) {
      Parcel pa = parcelRepository.findByCurrentOwner(p);
      try {
        pa.sell();
        parcelRepository.save(pa);
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
    return "redirect:/dashboard/accounts";
  }

  /**
   * Retrive Person Infos. Chairman and Admins may view Infos for all Users. Normal Users can only
   * see their own info
   */
  @GetMapping("/dashboard/accounts/{account}")
  public String getDashboardAccountsAccount(@PathVariable("account") Long account, Model model,
      @LoggedIn UserAccount userAccount) {

    Person person = personManagement.findOne(account);
    if (userAccount.hasRole(Role.of("ROLE_ADMIN")) || userAccount.hasRole(Roles.ROLE_CEO)
        || userAccount.hasRole(Roles.ROLE_CHAIRMAN) || userAccount == person.getUserAccount()) {
      model.addAttribute("person", person);
      model.addAttribute("warningRepository", warningRepository);
      model.addAttribute("parcel", parcelRepository.findByCurrentOwner(person));

      if (userAccount.hasRole(Role.of("ROLE_ADMIN")) || userAccount.hasRole(Roles.ROLE_CEO)) {
        model.addAttribute("personRoleAddForm", new PersonRoleAddForm());
      }

      model.addAttribute("passwordChangeForm", new PasswordChangeForm());

      return "db_userAccount";
    } else {
      throw new AccessDeniedException("Insufficient Permissions");
    }
  }

  @PostMapping("/dashboard/accounts/{account}/chpasswd")
  public String postDashboardAccountsAccountChpasswd(@PathVariable("account") Long account,
      @ModelAttribute("passwordChangeForm") @Valid PasswordChangeForm passwordChangeForm,
      BindingResult bindingResult, @LoggedIn UserAccount userAccount, RedirectAttributes
      redirectAttributes) {

    UserAccount u = personManagement.findOne(account).getUserAccount();

    if (userAccount.hasRole(Role.of("ROLE_ADMIN")) || userAccount.hasRole(Roles.ROLE_CEO)
        || userAccount.hasRole(Roles.ROLE_CHAIRMAN) || userAccount.equals(u)) {
      AtomicBoolean r = new AtomicBoolean(true);

      log.debug(String.format("Check old password match: %s %s %s %s",
          BCrypt.checkpw(passwordChangeForm.getCurrentpassword(), u.getPassword().toString()),
          userAccount.hasRole(Role.of("ROLE_ADMIN")),
          userAccount.hasRole(Roles.ROLE_CEO),
          userAccount.hasRole(Roles.ROLE_CHAIRMAN)
      ));

      if (!BCrypt.checkpw(passwordChangeForm.getCurrentpassword(), u.getPassword().toString())
          && !userAccount.hasRole(Role.of("ROLE_ADMIN")) && !userAccount.hasRole(Roles.ROLE_CEO)
          && !userAccount.hasRole(Roles.ROLE_CHAIRMAN)) {
        redirectAttributes.addFlashAttribute("message",
            new Message("useraccount.changepassword.missmatch.old", Severity.DANGER));
        log.debug("old password missmatches");
        r.set(false);
      }

      log.debug("checking new passwords match");
      if (!passwordChangeForm.getNewpassword().equals(passwordChangeForm.getNewpasswordagain())) {
        redirectAttributes.addFlashAttribute("message",
            new Message("useraccount.changepassword.missmatch.new", Severity.DANGER));
        log.debug("new password mismatch");
        r.set(false);
      }

      log.debug("check binding result (password length)");
      if (bindingResult.hasErrors()) {
        redirectAttributes.addFlashAttribute("message",
            new Message("useraccount.changepassword.error", Severity.DANGER));
        log.debug("binding result has errors");
        r.set(false);
      }

      if (r.get()) {
        userAccountManager.changePassword(u, passwordChangeForm.getNewpassword());
        redirectAttributes.addFlashAttribute("message",
            new Message("useraccount.changepassword.success", Severity.SUCCESS));

        String email = personManagement.findOne(account).getContactInfo().getEmail();
        if (email != null && !email.equals("")) {
          log.debug("trying to send confirmation mail to " + email);
          Person p = personManagement.findOne(account);
          SimpleMailMessage mailMessage = new SimpleMailMessage();
          mailMessage.setFrom("noreply@gardenmanager.example.com");
          mailMessage.setTo(email);
          mailMessage.setSubject("Your password for Gardenmanager was changed");
          mailMessage.setText(
              String.format(
                  "Hello %s %s,\n"
                      + "You recently changed the password for your Account %s.\n"
                      + "If you were the one who performed the change you can safely ignore this "
                      + "e-mail, if not, your account may have been compromised.\n"
                      + "\n"
                      + "Regards,\n"
                      + "the Java GardenManager", p.getFirstname(), p.getLastname(), u.getUsername()
              ));
          mailSender.send(mailMessage);
        }
      }

      return "redirect:/dashboard/accounts/{account}";
    } else {
      AccessDeniedException e = new AccessDeniedException("Insufficient Permissions");
      log.error("The user has insuffient permission for password change", e);
      throw e;
    }
  }

  /**
   * Add Role to User.
   *
   * @param personRoleAddForm Formular to parse
   * @return redirect back to account
   */
  @PreAuthorize("hasRole('ROLE_CEO')")
  @PostMapping("/dashboard/accounts/manage")
  public String postDashboardAccountsManage(@Valid PersonRoleAddForm personRoleAddForm) {

    Person p = personRepository.findOne(personRoleAddForm.getPerson());
    UserAccount u = p.getUserAccount();

    u.getRoles().stream().collect(Collectors.toList()).forEach(u::remove);
    personRoleAddForm.getRoles().forEach(r -> u.add(Roles.byName(r)));

    if (!u.hasRole(Roles.ROLE_CHAIRMAN)) {
      p.getDomain().clear();
    }

    userAccountManager.save(u);
    personRepository.save(p);

    return "redirect:/dashboard/accounts/" + personRoleAddForm.getPerson();
  }

  /**
   * Generate a Random Person with specified Role.
   *
   * @return username
   */
  private String generateUserWithRole(Role role) {
    Random rand = new Random(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
    String user = String.format("user%d", rand.nextInt());
    personManagement
        .createPerson("Person", "Demo", LocalDate.now().minusYears(Math.abs(rand.nextInt(500))),
            new Address("Demostraße", "Demoort", String.format("%05d", rand.nextInt(99999)), "3a"),
            new ContactInfo(String.format("%s@example.com", user),
                String.format("%d", Math.abs(rand.nextLong()))),
            userAccountManager.create(user, "password123", role));
    return user;
  }

  /**
   * Generate a User with {@link Roles#ROLE_TENANTRY}.
   */
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/dashboard/accounts/generateperson")
  public String getDashboardAccountsGenerateperson(RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute("message",
        new Message(
            String.format("created %s", generateUserWithRole(Roles.ROLE_TENANTRY)),
            Severity.INFO));
    return "redirect:/dashboard/accounts";
  }

  /**
   * Generate a User with {@link Roles#ROLE_CHAIRMAN}.
   */
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/dashboard/accounts/generatechairman")
  public String getDashboardAccountsGeneratechairman(RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute("message",
        new Message(
            String.format("created %s", generateUserWithRole(Roles.ROLE_CHAIRMAN)),
            Severity.INFO));
    return "redirect:/dashboard/accounts";
  }

  /**
   * Generate a User with {@link Roles#ROLE_TREASURER}.
   */
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/dashboard/accounts/generatetreasurer")
  public String getDashboardAccountsGeneratetreasurer(RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute("message",
        new Message(
            String.format("created %s", generateUserWithRole(Roles.ROLE_TREASURER)),
            Severity.INFO));
    return "redirect:/dashboard/accounts";
  }

  /**
   * Generate a User with {@link Roles#ROLE_CEO}.
   */
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/dashboard/accounts/generateceo")
  public String getDashboardAccountsGenerateceo(RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute("message",
        new Message(
            String.format("created %s", generateUserWithRole(Roles.ROLE_CEO)),
            Severity.INFO));
    return "redirect:/dashboard/accounts";
  }

}
