package de.gardenmanager.controller;

import de.gardenmanager.controller.Message.Severity;
import de.gardenmanager.roombooking.ReservationIdentifier;
import de.gardenmanager.roombooking.RoomBookingsystem;
import de.gardenmanager.validation.ReservationCreationForm;
import javax.validation.Valid;
import org.hibernate.QueryParameterException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by bennet on 14.11.17.
 */
@Controller
class RoombookingController {

  private final RoomBookingsystem roomBookingsystem;

  /**
   * ctor.
   */
  public RoombookingController(RoomBookingsystem roomBookingsystem) {
    this.roomBookingsystem = roomBookingsystem;
  }

  /**
   * roombooking frontend form.
   */
  @GetMapping("/booking")
  public String getBooking(Model model) {
    model.addAttribute("reservationCreationForm", new ReservationCreationForm());
    return "fe_r_booking";
  }

  /**
   * Process Booking Creation HTTP Form.
   */
  @PostMapping("/booking")
  public String postBooking(@ModelAttribute @Valid
      ReservationCreationForm reservationCreationForm, BindingResult result) {

    if (result.hasErrors()) {
      return "fe_r_booking";
    }
    try {
      roomBookingsystem.createReservation(
          reservationCreationForm.getStart().toLocalDateTime(),
          reservationCreationForm.getEnd().toLocalDateTime(),
          reservationCreationForm.getDescription(),
          reservationCreationForm.getOwner());
      return "fe_r_booking_successful";
    } catch (QueryParameterException e) {
      result.reject("roombooking.error.collusion",
          "Es wurde eine Überschneidung von Terminen festgestellt. "
              + "Bitte ändern Sie das Start- und/oder End-Datum");
      return "fe_r_booking";
    }
  }


  /**
   * roombooking backend form.
   */
  @GetMapping("/dashboard/restaurant/booking/add")
  public String getDashboardRestaurantBookingAdd(Model model) {
    model.addAttribute("reservationCreationForm", new ReservationCreationForm());
    return "db_addBooking";
  }

  /**
   * Process Booking Creation HTTP Form.
   */
  @PostMapping("/dashboard/restaurant/booking/add")
  public String postDashboardRestaurantBookingAdd(@ModelAttribute @Valid
      ReservationCreationForm reservationCreationForm, BindingResult result,
      RedirectAttributes redirectAttributes) {

    if (result.hasErrors()) {
      return "db_addBooking";
    }

    try {
      roomBookingsystem.createReservation(
          reservationCreationForm.getStart().toLocalDateTime(),
          reservationCreationForm.getEnd().toLocalDateTime(),
          reservationCreationForm.getDescription(),
          reservationCreationForm.getOwner());
      redirectAttributes.addFlashAttribute("message", new Message("booking.created", Severity
          .SUCCESS));
      return "redirect:/dashboard/restaurant/booking";
    } catch (QueryParameterException e) {
      result.reject("roombooking.error.collusion",
          "Es wurde eine Überschneidung von Terminen festgestellt. "
              + "Bitte ändern Sie das Start- und/oder End-Datum");
      return "db_addBooking";
    }
  }

  /**
   * list reservations.
   */
  @GetMapping("/dashboard/restaurant/booking")
  public String getDashboardRestaurantBooking(Model model) {
    model.addAttribute("bookings", roomBookingsystem.getAll());
    return "db_booking";
  }

  /**
   * delete reservation.
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/booking/delete/{id}")
  public String getDashboardRestaurantBookingDeleteId(
      @PathVariable("id") ReservationIdentifier id) {
    roomBookingsystem.deleteReservation(id);
    return "redirect:/dashboard/restaurant/booking";
  }
}
