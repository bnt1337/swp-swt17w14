package de.gardenmanager.controller;

import de.gardenmanager.restaurant.Dish;
import de.gardenmanager.restaurant.DishCatalog;
import de.gardenmanager.restaurant.Preorder;
import de.gardenmanager.restaurant.PreorderRepository;
import de.gardenmanager.restaurant.Restaurant;
import de.gardenmanager.validation.PreorderCreationForm;
import java.util.Optional;
import javax.validation.Valid;
import org.salespointframework.catalog.ProductIdentifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@SessionAttributes("preorder")
public class PreorderController {

  private PreorderRepository preorderRepository;
  private DishCatalog dishCatalog;
  private Restaurant restaurant;

  /**
   * @param restaurant the restaurant
   * @param dishCatalog the dishCatalog
   * @param preorderRepository the PreorderRepository
   */

  @Autowired
  public PreorderController(Restaurant restaurant, DishCatalog dishCatalog,
      PreorderRepository preorderRepository) {
    this.preorderRepository = preorderRepository;
    this.restaurant = restaurant;
    this.dishCatalog = dishCatalog;
  }

  @ModelAttribute("preorder")
  public Preorder initializePreorder() {
    return new Preorder();
  }

  /**
   * Mapping for /preorder (GUI for adding OrderItem).
   *
   * @param model the model
   * @param preorder the preorder (ModelAttribute)
   */

  @GetMapping("/preorder")
  public String getPreorder(Model model, @ModelAttribute Preorder preorder) {
    model.addAttribute("preorders", preorder.getOrderItems());
    model.addAttribute("starters", restaurant.showPublicStarters());
    model.addAttribute("maincourses", restaurant.showPublicMaincourses());
    model.addAttribute("desserts", restaurant.showPublicDesserts());
    return "fe_r_preorder_add";
  }

  /**
   * Mapping for /preorder/data (GUI for adding date, firstname, lastname).
   *
   * @param model the model
   */

  @GetMapping("/preorder/data")
  public String getPreorderData(Model model) {
    model.addAttribute("preorderCreationForm", new PreorderCreationForm());
    return "fe_r_preorder_data";
  }

  /**
   * Mapping for /preorder/addData (set date, firstname, lastname in preorder).
   *
   * @param preorder the preorder (session attribute)
   * @param preorderCreationForm the preorderForm object for data exchange
   * @param result the result
   */

  @PostMapping("/preorder/addData")
  public String postPreorderAddData(@SessionAttribute Preorder preorder,
      @ModelAttribute @Valid PreorderCreationForm preorderCreationForm, BindingResult result) {
    if (result.hasErrors()) {
      return "fe_r_preorder_data";
    }
    preorder.setFirstname(preorderCreationForm.getFirstname());
    preorder.setDate(preorderCreationForm.getDate());
    preorder.setLastname(preorderCreationForm.getLastname());
    restaurant.saveOrderItems(preorder);
    preorderRepository.save(preorder);
    return "redirect:/preorder/successful";
  }

  /**
   * Mapping for /preorder/addItem (add new orderItem in preorder).
   *
   * @param preorder the preorder (modelAttribute)
   * @param id the id
   * @param number the number of ordered dishes
   */

  @PostMapping("/preorder/addItem")
  public String postPreorderAddItem(@RequestParam("id") ProductIdentifier id,
      @RequestParam("number") Integer number, @ModelAttribute Preorder preorder) {
    Optional<Dish> dish = dishCatalog.findOne(id);
    if (dish.isPresent() & number != null & number > 0) {
      preorder.addDish(dish.get(), number);
    }
    return "redirect:/preorder";
  }

  /**
   * Mapping for /preorder/delateItem (delate orderItem).
   *
   * @param preorder the preorder (modelAttribute)
   * @param id the id of an orderItem
   */

  @PostMapping("/preorder/delateItem")
  public String postPreorderDalateItem(@RequestParam("id") long id,
      @ModelAttribute Preorder preorder) {
    preorder.delateItem(id);
    return "redirect:/preorder";
  }

  /**
   * Mapping for /preorder/successful (GUI for show success).
   *
   * @param model the model
   * @param preorder the preorder (ModelAttribute for to show hole preorder in GUI)
   * @param status the status of the session for set complete
   */

  @GetMapping("/preorder/successful")
  public String getPreorderSuccessful(@ModelAttribute Preorder preorder, Model model,
      SessionStatus status) {
    if (preorder.getOrderItems().size() == 0) {
      return "redirect:/preorder";
    } else if (preorder.getFirstname() == null | preorder.getLastname() == null) {
      return "redirect:/preorder/data";
    }
    model.addAttribute("preorder", preorder);
    status.setComplete();
    return "fe_r_preorder_successful";
  }

  @GetMapping("/dashboard/restaurant/preorder/")
  public String getPreorderShowAll(Model model) {
    model.addAttribute("preorders", preorderRepository.findAll());

    return "db_viewPreorders";
  }
}