package de.gardenmanager.controller;

import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.ParcelStateForLease;
import de.gardenmanager.person.ParcelStateLeased;
import de.gardenmanager.person.PersonRepository;
import de.gardenmanager.validation.AssignPersonParcelForm;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.validation.Valid;
import org.salespointframework.core.Streamable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
class ParcelController {

  private final ParcelRepository parcelRepository;
  private final PersonRepository personRepository;

  /**
   * Initialize the ParcelController with Autowired beans.
   */
  public ParcelController(ParcelRepository parcelRepository,
      PersonRepository personRepository) {
    this.parcelRepository = parcelRepository;
    this.personRepository = personRepository;
  }

  /**
   * list all free parcels in the Frontend with the option to apply for it.
   */
  @GetMapping("/freeParcels")
  public String getFreeparcels(Model model) {
    model.addAttribute("parcels", Streamable.of(parcelRepository.findAll()).stream()
        .filter(p -> p.getState() instanceof ParcelStateForLease).collect(Collectors.toList()));
    return "fe_freeParcels";
  }

  /**
   * Administrative Parcel-Owner override. (Form Processing)
   */
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping("/dashboard/parcels/{parcelid}/assign")
  public String postDashboardParcelsParcelidAssign(
      @ModelAttribute @Valid AssignPersonParcelForm assignPersonParcelForm, BindingResult
      bindingResult, @PathVariable("parcelid") String parcelid, Model model) {

    Parcel p = parcelRepository.findOne(Long.parseLong(parcelid));

    if (bindingResult.hasErrors()) {
      model.addAttribute("parcel", p);
      model.addAttribute("persons", personRepository.findAll());
      return "db_parcelOverride";
    }

    p.setCurrentOwner(
        personRepository.findOne(Long.parseLong(assignPersonParcelForm.getPersonId())));
    p.setState(new ParcelStateLeased());

    parcelRepository.save(p);

    return "redirect:/dashboard/parcels";
  }

  /**
   * form for adminitrative parcel owner override.
   */
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/dashboard/parcels/{parcelid}/assign")
  public String getDashboardParcelsParcelidAssign(Model model,
      @PathVariable("parcelid") Long parcelid) {

    Parcel p = parcelRepository.findOne(parcelid);

    model.addAttribute("assignPersonParcelForm", new AssignPersonParcelForm());
    model.addAttribute("parcel", p);
    model.addAttribute("persons", personRepository.findAll());

    return "db_parcelOverride";
  }

  /**
   * list all parcels in backend.
   */
  @GetMapping("/dashboard/parcels")
  public String getDashboardParcels() {
    return "db_parcel";
  }

  /**
   * list all parcels in backend.
   */
  @GetMapping(value = "/dashboard/parcels/json", produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getDashboardParcelsJson() {
    Map<String, Object> config = new HashMap<String, Object>();
    //if you need pretty printing
    config.put("javax.json.stream.JsonGenerator.prettyPrinting", Boolean.TRUE);
    JsonBuilderFactory factory = Json.createBuilderFactory(config);
    JsonArrayBuilder arrayBuilder = factory.createArrayBuilder();
    Streamable.of(parcelRepository.findAll()).stream().forEach(p -> {
      arrayBuilder.add(factory.createArrayBuilder()
          .add(p.getLocation())
          .add(p.getCurrentOwner() == null ? "" : p.getCurrentOwner().toString())
          .add(p.getSquaremeters())
          .add(p.getState().toString())
          .add(p.getPastOwners() == null ? "" : p.getPastOwners().toString())
          .add("<a href=\"/dashboard/parcels/" + p.getId() + "/assign\" class=\"btn btn-danger\">"
                  + "<i class=\"fa fa-bolt\" aria-hidden=\"true\"></i></a>"));
    });

    JsonObject value = factory.createObjectBuilder()
        .add("data", arrayBuilder).build();
    //model.addAttribute("parcels", parcelRepository.findAll());
    return value.toString();
  }

  //  /**
  //   * get info for specific parcel. (NIY)
  //   */
  //  @GetMapping("/dashboard/parcels/{parcel}")
  //  public String getDashboardParcelsParcel(@PathVariable Long parcel, Model model) {
  //    model.addAttribute("parcel", parcelRepository.findOne(parcel));
  //    return "db_parcelInfo";
  //  }
}
