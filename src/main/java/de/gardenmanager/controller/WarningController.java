package de.gardenmanager.controller;

import de.gardenmanager.controller.Message.Severity;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.validation.WarningCreationForm;
import de.gardenmanager.warning.WarningSystem;
import java.time.LocalDateTime;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.web.LoggedIn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * controller-class for showing, adding and remiving warnings.
 */
@Controller
class WarningController {

  private final WarningSystem warningSystem;
  private final PersonManagement personManagement;

  /**
   * Initialize WarningController with Autowired beans.
   */
  public WarningController(WarningSystem warningSystem,
      PersonManagement personManagement) {
    this.warningSystem = warningSystem;
    this.personManagement = personManagement;
  }

  /**
   * main page of the warning module where actual warnings are showed.
   *
   * @param model the model
   * @return the mapped resource
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/warning")
  public String getDashboardWarning(Model model) {
    model.addAttribute("warnings", warningSystem.showWarnings());

    return "db_warning";
  }

  /**
   * add an warning - prepare adding.
   *
   * @param model the model
   * @return the mapped resource
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/warning/add")
  public String getDashboardWarningAdd(Model model) {
    model.addAttribute("warningCreationForm", new WarningCreationForm());
    model.addAttribute("tenants", personManagement.findAll().collect(Collectors.toList()));

    return "db_addWarning";
  }

  /**
   * mapping add an warning.
   *
   * @param warningForm the warningCreationForm with the required data
   * @return the mapped resource
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @PostMapping("/dashboard/warning/add")
  public String postDashboardWaningAdd(
      @ModelAttribute("warningCreationForm") @Valid WarningCreationForm warningForm,
      BindingResult result, @LoggedIn UserAccount userAccount, RedirectAttributes
      redirectAttributes) {

    if (result.hasErrors()) {
      redirectAttributes.addFlashAttribute("message", new Message("warning.add.error", Severity
          .WARNING));
      return "redirect:/dashboard/warning/add";
    }

    warningSystem.createWarning(LocalDateTime.now(),
        warningForm.getDescription(), warningForm.getTenantId(),
        personManagement.findByUserAccount(userAccount));

    redirectAttributes.addFlashAttribute("message", new Message(String.format("warning.add.success"
        + "(%s)", personManagement.findOne(warningForm.getTenantId())), Severity
        .SUCCESS));
    return "redirect:/dashboard/warning";
  }

  /**
   * delete a warning.
   *
   * @param id the id of the warning to delete
   * @return the mapped resource
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @GetMapping("/dashboard/warning/delete/{id}")
  public String getDashboardWarningDeleteId(@PathVariable(value = "id") long id,
      RedirectAttributes redirectAttributes) {

    warningSystem.deleteWarning(id);
    redirectAttributes.addFlashAttribute("message", new Message("warning.deleted",
        Severity.INFO));

    return "redirect:/dashboard/warning";
  }

  /**
   * delete all warnings.
   *
   * @return the mapped resource
   */
  @PreAuthorize("hasRole('ROLE_CHAIRMAN')")
  @RequestMapping("/dashboard/warning/delete/all")
  public String getDashboardWarningDeleteAll(RedirectAttributes redirectAttributes) {

    warningSystem.deleteAllWarnings();
    redirectAttributes.addFlashAttribute("message",
        new Message("roombooking.event.deleted.all", Severity.INFO));

    return "redirect:/dashboard/warning";
  }
}
