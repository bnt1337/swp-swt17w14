package de.gardenmanager.controller;

import de.gardenmanager.accounting.invoice.InvoiceManagement;
import de.gardenmanager.calendar.WorkingHours;
import de.gardenmanager.calendar.WorkingHoursRepository;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicBoolean;
import org.salespointframework.core.Streamable;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.web.LoggedIn;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@SuppressWarnings("unused")
class MainController {

  private final PersonManagement personManagement;
  private final InvoiceManagement invoiceManagement;
  private final ParcelRepository parcelRepository;
  private final WorkingHoursRepository workingHoursRepository;

  /**
   * Initialize the MainController with Autowired beans.
   */
  MainController(PersonManagement personManagement,
      InvoiceManagement invoiceManagement,
      ParcelRepository parcelRepository,
      WorkingHoursRepository workingHoursRepository) {
    this.personManagement = personManagement;
    this.invoiceManagement = invoiceManagement;
    this.parcelRepository = parcelRepository;
    this.workingHoursRepository = workingHoursRepository;
  }

  /**
   * The Main Frontend Page View.
   */
  @GetMapping("/")
  public String getIndex() {
    return "fe_welcome";
  }

  /**
   * Dashboard Mainpage.
   */
  @GetMapping("/dashboard")
  public String getDashboard(Model model, @LoggedIn UserAccount userAccount) {
    Person p = personManagement.findByUserAccount(userAccount);
    AtomicBoolean invoiceavailable = new AtomicBoolean(false);

    if (p != null) {
      Parcel par = parcelRepository.findByCurrentOwner(p);
      invoiceManagement.findAllByParcel(par).stream()
          .filter(i -> i.getInvoiceDate() == null && i.getMeterReadingDue()
              .isAfter(
                  LocalDateTime.now()))
          .findAny()
          .ifPresent(i -> {
            invoiceavailable.set(true);
            model.addAttribute("invoice", i);
            model.addAttribute("invoicepercentage",
                ((double) Duration.between(i.getMeterReadingDue(), LocalDateTime.now()).toHours()
                    / (
                    14.0 * 24.0)) * 100.0);
          });
      if (par != null && par.getCurrentOwner().getBirthday().plusYears(WorkingHours.AGE_THRESHOLD)
          .isAfter(LocalDate.now())) {
        double hoursDone = Streamable.of(workingHoursRepository.findAll()).stream()
            .filter(wh -> wh.getPerson() == par.getCurrentOwner())
            .filter(wh -> wh.getAssignment()
            .getEvent().getStart().getYear() == LocalDate.now().getYear()).mapToDouble(
            WorkingHours::getHoursDone).sum();
        model.addAttribute("hoursdone", hoursDone);
      } else {
        model.addAttribute("hoursdone", null);
      }
    }
    model.addAttribute("invoiceavailable", invoiceavailable.get());

    return "db_home";
  }
}
