package de.gardenmanager.controller;

import de.gardenmanager.person.Person;
import de.gardenmanager.person.PersonManagement;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.codec.digest.DigestUtils;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.web.LoggedIn;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Created by bennet on 21.12.2017.
 */
@ControllerAdvice
class AdvisedController {

  private final PersonManagement personManagement;

  public AdvisedController(PersonManagement personManagement) {
    this.personManagement = personManagement;
  }

  /**
   * Access the current logged in UserAccount from anywhere.
   */
  @ModelAttribute("useraccount")
  public String getUseraccount(@LoggedIn Optional<UserAccount> userAccount) {
    AtomicReference<String> name = new AtomicReference<>("none");
    userAccount.ifPresent(u -> name.set(u.getUsername()));

    return name.get();
  }

  /**
   * Access the current logged in UserAccount from anywhere.
   */
  @ModelAttribute("userid")
  public String getUserid(@LoggedIn Optional<UserAccount> userAccount) {
    return Long.toString(userAccount.map(u -> {
      Person p = personManagement.findByUserAccount(u);
      return p == null ? -1L : p.getId();
    }).orElse(-1L));
  }

  /**
   * Get the Users full name (from any where).
   */
  @ModelAttribute("username")
  public String getUsername(@LoggedIn Optional<UserAccount> userAccount) {
    AtomicReference<String> name = new AtomicReference<>("Doe, John");

    userAccount.ifPresent(u -> {
      Person p = personManagement.findByUserAccount(u);
      if (p != null) {
        name.set(p.toString());
      } else if (u.getFirstname() != null && !u.getFirstname().isEmpty()
          || u.getLastname() != null && !u.getLastname().isEmpty()) {
        name.set(String.format("%s, %s", u.getLastname(), u.getFirstname()));
      }
    });

    return name.get();
  }

  @ModelAttribute("gravatarurl")
  public String getGravatarurl(@LoggedIn Optional<UserAccount> userAccount) {
    String md5 = userAccount.map(u -> {
      Person p = personManagement.findByUserAccount(u);
      if (p != null) {
        String email = p.getContactInfo().getEmail();
        return DigestUtils.md5Hex(email == null ? "" : email);
      } else {
        return "00000000000000000000000000000000";
      }
    }).orElse("00000000000000000000000000000000");

    return "https://www.gravatar.com/avatar/" + md5 + "?d=robohash";
  }
}
