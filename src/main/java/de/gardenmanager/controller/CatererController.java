package de.gardenmanager.controller;

import de.gardenmanager.restaurant.CatererManagement;
import de.gardenmanager.validation.CatererRegistrationForm;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by bennet on 10.11.17.
 */

/**
 * Controller between {@link CatererManagement} and UI.
 */
@Controller
class CatererController {

  private final CatererManagement catererManagement;

  /**
   * ctor.
   */
  CatererController(CatererManagement catererManagement) {
    Assert.notNull(catererManagement, "CatererManagement must not be null!");
    this.catererManagement = catererManagement;
  }

  /**
   * Process Caterer Registration HTTP Form.
   *
   * @param catererRegistrationForm Form
   * @param result result
   * @return rdr
   */
  @PostMapping("/dashboard/caterer/register")
  public String postDashboardCatererRegister(
      @ModelAttribute("catererRegistrationForm") @Valid
          CatererRegistrationForm catererRegistrationForm,
      BindingResult result) {
    if (result.hasErrors()) {
      return "db_catererRegistration";
    }

    this.catererManagement
        .createCustomer(catererRegistrationForm.getName(), catererRegistrationForm.getPassword());

    return "redirect:/dashboard";
  }

  /**
   * http form to add caterer.
   */
  @GetMapping("/dashboard/caterer/add")
  public String getDashboardCatererAdd(Model model) {
    model.addAttribute("catererRegistrationForm", new CatererRegistrationForm());
    return "db_catererRegistration";
  }
}
