package de.gardenmanager.controller;

import static org.salespointframework.core.Currencies.EURO;

import de.gardenmanager.restaurant.Restaurant;
import de.gardenmanager.validation.DishCreationForm;
import de.gardenmanager.validation.MenuCreationForm;
import javax.validation.Valid;
import org.javamoney.moneta.Money;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * controller for the Restaurant-Controller.
 */
@Controller
class RestaurantController {

  private final Restaurant restaurant;

  /**
   * frontend menu listing.
   */
  @GetMapping("/menu")
  public String getMenu(Model model) {
    model.addAttribute("starters", restaurant.showPublicStarters());
    model.addAttribute("maincourses", restaurant.showPublicMaincourses());
    model.addAttribute("desserts", restaurant.showPublicDesserts());
    return "fe_r_menu";
  }

  /**
   * preorder form.
   */
  @RequestMapping("/preorder")
  public String getPreorder() {
    return "fe_r_preorder";
  }

  /**
   * the constructor.
   *
   * @param restaurant restaurant für the business-logic
   */
  RestaurantController(Restaurant restaurant) {
    this.restaurant = restaurant;
  }

  /**
   * Mapping for /dish (show all dishs).
   *
   * @param model the model
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/dish")
  public String getDashboardResturantDish(Model model) {
    model.addAttribute("dishList", restaurant.showDishs());
    return "db_dish";
  }

  /**
   * Mapping for /menu (show all menus).
   *
   * @param model the model
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/menu")
  public String getDashboardRestaurantMenu(Model model) {
    model.addAttribute("menuList", restaurant.showMenus());
    return "db_menu";
  }

  /**
   * frontend restaurant opening hours.
   */
  @GetMapping("/opening")
  public String getOpening() {
    return "fe_r_opening";
  }

  /**
   * Mapping for /dish/add (GUI for adding a new menu).
   *
   * @param model the model
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/dish/add")
  public String getDashboardRestaurantDishAdd(Model model) {
    model.addAttribute("dishForm", new DishCreationForm());
    return "db_addDish";
  }

  /**
   * Mapping for /dish/add/new.
   *
   * @param dishForm the dishForm with the required data
   * @param result the result
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @PostMapping("/dashboard/restaurant/dish/new")
  public String postDashboardRestaurantDishNew(
      @ModelAttribute("dishForm") @Valid DishCreationForm dishForm,
      BindingResult result) {
    if (result.hasErrors()) {
      return "redirect:/dashboard/restaurant/dish/add";
    }

    double amount = Math.abs(dishForm.getPrice().doubleValue());

    if (amount < 0) {
      amount = 0.0;
    }

    restaurant.createDish(dishForm.getName(), dishForm.getAllergen(),
        Money.of(amount, EURO), dishForm.getDishType());

    return "redirect:/dashboard/restaurant/dish";
  }

  /**
   * Mapping for /dish/remove/{name} (remove an existing dish).
   *
   * @param name the name of the dish which we would remove
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/dish/remove/{name}")
  public String getDashboardRestaurantDishRemoveName(@PathVariable(value = "name") String name) {
    //TODO Delete from Meal first
    restaurant.deleteDish(name);
    return "redirect:/dashboard/restaurant/dish";
  }

  /**
   * Mapping for /menu/add (add an new (temporary) menu).
   *
   * @param model the model
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/menu/add")
  public String getDashboardRestaurantMenuAdd(Model model) {
    model.addAttribute("starters", restaurant.getStarters());
    model.addAttribute("maincourses", restaurant.getMainCourses());
    model.addAttribute("desserts", restaurant.getDesserts());
    model.addAttribute("menuForm", new MenuCreationForm());
    return "db_addMenu";
  }

  /**
   * Mapping for /menu/add/new (add dish to temporary menu).
   *
   * @param result the result
   * @param menuForm the menuForm object for data exchange
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @PostMapping("/dashboard/restaurant/menu/new")
  public String postDashboardRestaurantMenuNew(
      @ModelAttribute("menuForm") @Valid MenuCreationForm menuForm,
      BindingResult result, Model model) {
    if (result.hasErrors()) {
      model.addAttribute("starters", restaurant.getStarters());
      model.addAttribute("maincourses", restaurant.getMainCourses());
      model.addAttribute("desserts", restaurant.getDesserts());
      return "db_addMenu";
    }

    restaurant.createMenu(menuForm.getName(), menuForm.getStarters(),
        menuForm.getMaincourses(), menuForm.getDesserts());

    return "redirect:/dashboard/restaurant/menu";
  }

  /**
   * Mapping for /menu/remove (remove dish from temporary menu).
   *
   * @param name the name of the menu to store
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/menu/remove/{name}")
  public String getDashboardRestaurantMenuRemoveName(@PathVariable(value = "name") String name) {
    restaurant.deleteMenu(name);
    return "redirect:/dashboard/restaurant/menu";
  }

  /**
   * Mapping for /menu/publish/{name} (publish menu).
   *
   * @param name the name of the menu to store
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/menu/publish/{name}")
  public String getDashboardRestaurentMenuPublishName(@PathVariable(value = "name") String name) {
    restaurant.setMenuState(name, true);
    return "redirect:/dashboard/restaurant/menu";
  }

  /**
   * Mapping for /menu/publish/{name} (un-publish menu).
   *
   * @param name the name of the menu to store
   */
  @PreAuthorize("hasRole('ROLE_CATERER')")
  @GetMapping("/dashboard/restaurant/menu/unpublish/{name}")
  public String getDashboardRestaurantMenuUnpublishName(@PathVariable(value = "name") String name) {
    restaurant.setMenuState(name, false);
    return "redirect:/dashboard/restaurant/menu";
  }
}