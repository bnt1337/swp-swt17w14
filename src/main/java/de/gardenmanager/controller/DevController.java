package de.gardenmanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by bennet on 01.01.2018.
 */
@Controller
class DevController {

  @GetMapping("/dashboard/dev/usercreation")
  public String getDashboardDevUsercreation() {
    return "db_generateperson";
  }
}
