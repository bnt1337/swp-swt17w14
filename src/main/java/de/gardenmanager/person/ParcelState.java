package de.gardenmanager.person;

import java.io.Serializable;

public abstract class ParcelState implements Serializable {

  /**
   * Change Parcel state by leasing it.
   *
   * @param person new owner
   * @return new State
   */
  public abstract ParcelState lease(Person person, Parcel parcel);

  /**
   * Change Parcel state by selling it.
   *
   * @return new State
   */
  public abstract ParcelState sell(Parcel parcel);

  /**
   * Return state name.
   */
  public abstract String toString();
}

