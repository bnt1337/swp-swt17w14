package de.gardenmanager.person;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

  //public static final long serialVersionUID = 201712021739L;

  private String street;
  private String city;
  private String zipCode;
  private String streetNumber;

  /**
   * Create new Address with it's Parameters.
   *
   * @param street Street name with out number
   * @param city City name
   * @param zipCode ZIP Code (5 Digits in Germany)
   * @param streetNumber Streetnumber with all appendings
   */
  public Address(String street, String city, String zipCode, String streetNumber) {
    this.street = street;
    this.city = city;
    this.zipCode = zipCode;
    this.streetNumber = streetNumber;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private Address() {
    //Because Spring needs it :(
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }
}
