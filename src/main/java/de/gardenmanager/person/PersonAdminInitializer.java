package de.gardenmanager.person;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import org.salespointframework.core.DataInitializer;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.stereotype.Component;

@Component
class PersonAdminInitializer implements DataInitializer {

  //private static final Logger logger = LoggerFactory.getLogger(PersonAdminInitializer.class);
  private final UserAccountManager userAccountManager;
  private final PersonManagement personManagement;

  /**
   * autowired ctor.
   */
  public PersonAdminInitializer(UserAccountManager userAccountManager,
      PersonManagement personManagement) {

    this.userAccountManager = userAccountManager;
    this.personManagement = personManagement;
  }

  /**
   * create an admin-account on application startup.
   */
  @Override
  public void initialize() {
    //logger.info("\n\nCreating User 'admin' with 'supersicher'\n\n");
    if (!userAccountManager.findByUsername("admin").isPresent()) {
      UserAccount admin = userAccountManager.create("admin", "supersicher", Role.of("ROLE_ADMIN"));
      admin.setFirstname("Administrator");
      admin.setLastname("Super");
      UserAccount account = userAccountManager.save(admin);

      DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
          FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

      LocalDate date = LocalDate.parse("01.01.2000", germanFormatter);

      personManagement.createPerson("Administrator", "Super", date,
          new Address("Administratorallee", "Managementtown", "00000", "42"),
          new ContactInfo("toor@root.com", "0123456789"), account);
    }
  }
}
