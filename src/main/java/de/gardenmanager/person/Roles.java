package de.gardenmanager.person;

import org.salespointframework.useraccount.Role;

public final class Roles {

  public static final Role ROLE_TENANTRY = Role.of("ROLE_TENANTRY");
  public static final Role ROLE_CEO = Role.of("ROLE_CEO");
  public static final Role ROLE_TREASURER = Role.of("ROLE_TREASURER");
  public static final Role ROLE_CHAIRMAN = Role.of("ROLE_CHAIRMAN");

  /**
   * Get a Role by it's String-name. Defaults to {@link Roles#ROLE_TENANTRY}.
   *
   * @param role Role String
   * @return {@link org.salespointframework.useraccount.Role}
   */
  public static Role byName(String role) {
    Role r;
    switch (role) {
      case "ROLE_CEO":
        r = ROLE_CEO;
        break;
      case "ROLE_TREASURER":
        r = ROLE_TREASURER;
        break;
      case "ROLE_CHAIRMAN":
        r = ROLE_CHAIRMAN;
        break;
      default:
        r = ROLE_TENANTRY;
    }
    return r;
  }
}