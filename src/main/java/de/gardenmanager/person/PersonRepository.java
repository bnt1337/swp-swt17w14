package de.gardenmanager.person;

import java.util.Optional;
import org.salespointframework.useraccount.UserAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

  /**
   * Retrives Persons by their lastname.
   *
   * @param name must not be {@literal null}
   * @return List of Persons
   */
  Iterable<Person> findByLastname(String name);

  /**
   * Retrive a Person by its {@link org.salespointframework.useraccount.UserAccount}.
   *
   * @param userAccount Account. must not be {@literal null}
   * @return Person or none
   */
  Optional<Person> findByUserAccount(UserAccount userAccount);


  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  Person findOne(Long id);

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  boolean exists(Long id);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<Person> findAll();

  /**
   * Returns all instances of the type with the given IDs.
   */
  @Override
  Iterable<Person> findAll(Iterable<Long> longs);

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   */
  @Override
  long count();
}
