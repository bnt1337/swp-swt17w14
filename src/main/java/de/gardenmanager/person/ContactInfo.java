package de.gardenmanager.person;

import javax.persistence.Embeddable;

@Embeddable
public class ContactInfo {

  //public static final long serialVersionUID = 201712021739L;

  private String email;
  private String phoneNumber;

  /**
   * Initialize ContactInfo for a {@link Person}.
   */
  public ContactInfo(String email, String phoneNumber) {
    this.email = email;
    this.phoneNumber = phoneNumber;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private ContactInfo() {
    //Because Spring needs it :(
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
}
