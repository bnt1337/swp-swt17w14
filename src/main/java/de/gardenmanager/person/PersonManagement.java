package de.gardenmanager.person;

import com.google.common.collect.Streams;
import de.gardenmanager.validation.PersonCreationForm;
import java.time.LocalDate;
import java.util.stream.Stream;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PersonManagement {

  private final PersonRepository persons;
  private final UserAccountManager userAccounts;
  private final ParcelRepository parcelRepository;

  /**
   * autowired ctor.
   *
   * @param persons autowired bean.
   * @param userAccounts autowired bean.
   */
  public PersonManagement(PersonRepository persons,
      UserAccountManager userAccounts, ParcelRepository parcelRepository) {
    this.persons = persons;
    this.userAccounts = userAccounts;
    this.parcelRepository = parcelRepository;
  }

  /**
   * Create new Person by Creation-Validation-Form and save it.
   */
  public Person createPerson(PersonCreationForm form) {
    //DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    LocalDate birth = form.getBirthday();

    UserAccount user = userAccounts
        .create(form.getUsername(), form.getPassword(), Roles.ROLE_TENANTRY);

    Address address = new Address(form.getStreet(), form.getCity(), form.getZipCode(),
        form.getStreetNumber());
    ContactInfo contactInfo = new ContactInfo(form.getEmail(), form.getPhoneNumber());

    Person r = createPerson(form.getFirstname(), form.getLastname(), birth, address, contactInfo,
        user);

    if (form.getParcel() != null) {
      parcelRepository.save(parcelRepository.findOne(form.getParcel()).lease(r));
    }

    return r;
  }

  /**
   * Create new Person by single parameters and save it.
   */
  public Person createPerson(String firstname, String lastname, LocalDate birthday, Address address,
      ContactInfo contactInfo, UserAccount userAccount) {

    return persons.save(
        new Person(firstname, lastname, birthday, address, contactInfo, userAccount));
  }

  /**
   * Returns all {@link Person}s currently available in the system.
   */
  public Stream<Person> findAll() {
    return Streams.stream(persons.findAll());
  }

  /**
   * Returns all instances of the type with the given IDs.
   */
  public Stream<Person> findAll(Iterable<Long> longs) {
    return Streams.stream(persons.findAll(longs));
  }

  /**
   * Retrives Persons by their lastname.
   *
   * @param name must not be {@literal null}
   * @return List of Persons
   */
  public Stream<Person> findByLastname(String name) {
    return Streams.stream(persons.findByLastname(name));
  }

  /**
   * Retrive a Person by its {@link org.salespointframework.useraccount.UserAccount}.
   *
   * @param userAccount Account. must not be {@literal null}
   * @return Person or {@literal null}
   */
  public Person findByUserAccount(UserAccount userAccount) {
    return persons.findByUserAccount(userAccount).orElse(null);
  }

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  public Person findOne(Long id) {
    return persons.findOne(id);
  }

  /**
   * Save an Person Entity.
   *
   * @param person Person. must not be {@literal null}
   * @return the saved Person
   */
  public Person save(Person person) {
    return persons.save(person);
  }
}
