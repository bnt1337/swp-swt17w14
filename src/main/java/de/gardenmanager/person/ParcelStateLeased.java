package de.gardenmanager.person;

public final class ParcelStateLeased extends ParcelState {

  /**
   * {@inheritDoc}
   *
   * @see ParcelState#lease(Person, Parcel)
   */
  @Override
  public ParcelState lease(Person person, Parcel parcel) {
    throw new IllegalStateException();
  }

  /**
   * {@inheritDoc}
   *
   * @see ParcelState#sell(Parcel)
   */
  @Override
  public ParcelState sell(Parcel parcel) {
    parcel.setCurrentOwner(null);
    return new ParcelStateForLease();
  }

  /**
   * {@inheritDoc}
   *
   * @see ParcelState#toString()
   */
  @Override
  public String toString() {
    return "ParcelState.Leased";
  }
}
