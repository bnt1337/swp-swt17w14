package de.gardenmanager.person;

import org.springframework.data.repository.CrudRepository;

public interface ParcelRepository extends CrudRepository<Parcel, Long> {

  Parcel findByLocation(String location);

  Parcel findByCurrentOwner(Person person);
}
