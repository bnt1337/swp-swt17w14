package de.gardenmanager.person;

public final class ParcelStateForLease extends ParcelState {

  /**
   * {@inheritDoc}
   *
   * @see ParcelState#lease(Person, Parcel)
   */
  @Override
  public ParcelState lease(Person person, Parcel parcel) {
    parcel.setCurrentOwner(person);
    return new ParcelStateLeased();
  }

  /**
   * {@inheritDoc}
   *
   * @see ParcelState#sell(Parcel)
   */
  @Override
  public ParcelState sell(Parcel parcel) {
    throw new IllegalStateException();
  }

  /**
   * {@inheritDoc}
   *
   * @see ParcelState#toString()
   */
  @Override
  public String toString() {
    return "ParcelState.ForLease";
  }
}
