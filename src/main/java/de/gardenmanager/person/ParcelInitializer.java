package de.gardenmanager.person;

import com.google.common.collect.Iterables;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Random;
import org.salespointframework.core.DataInitializer;
import org.springframework.stereotype.Component;

@Component
class ParcelInitializer implements DataInitializer {

  private static final int PARCEL_COUNT = 200;
  private static final int PARCEL_COUNT_PER_GROUP = 25;
  private static final int PARCEL_MAX_SIZE = 300;
  private static final int PARCEL_MIN_SIZE = 150;

  private final ParcelRepository parcelRepository;

  /**
   * autowired ctor.
   */
  public ParcelInitializer(ParcelRepository parcelRepository) {
    this.parcelRepository = parcelRepository;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {
    initializeParcels();
  }

  /**
   * create all 200 Parcels.
   */
  private void initializeParcels() {
    if (Iterables.size(parcelRepository.findAll()) > 1) {
      //don't blow up repository when running with persistent database
      return;
    }

    Random rnd = new Random(ZonedDateTime.now(ZoneOffset.systemDefault()).toEpochSecond());

    final char startName = 'A';

    for (int i = 0; i < PARCEL_COUNT; i++) {
      parcelRepository
          .save(new Parcel(String.format("%c%d",
              (char) (startName + (int) Math.floor((double) i / (double) PARCEL_COUNT_PER_GROUP)),
              i % PARCEL_COUNT_PER_GROUP),
              rnd.nextInt(PARCEL_MAX_SIZE - PARCEL_MIN_SIZE) + PARCEL_MIN_SIZE));
    }
  }
}
