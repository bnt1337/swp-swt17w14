package de.gardenmanager.person;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public final class Parcel {

  @Id
  @GeneratedValue
  @Column(name = "parcelid")
  private long id;

  @OneToOne
  @JoinColumn(unique = true)
  private Person currentOwner;

  @ManyToMany(fetch = FetchType.EAGER)
  private List<Person> pastOwners;

  //@Column(columnDefinition = "VARBINARY")
  private ParcelState state;

  @Column(unique = true)
  private String location;

  private float squaremeters;

  @Deprecated
  @SuppressWarnings("unused")
  private Parcel() {
    //Because Spring needs it :(
  }

  /**
   * Create a new Parcel.
   *
   * @param location Location description
   * @param squaremeters size in squaremeters
   */
  public Parcel(String location, float squaremeters) {
    this.location = location;
    this.squaremeters = squaremeters;
    pastOwners = new LinkedList<>();
    state = new ParcelStateForLease();
  }

  public Person getCurrentOwner() {
    return currentOwner;
  }

  /**
   * Set the new current Owner, automatically saving the old one to {@link Parcel#pastOwners}.
   *
   * @param currentOwner the new current Owner to set
   */
  public void setCurrentOwner(Person currentOwner) {
    if (this.currentOwner != null) {
      pastOwners.add(this.currentOwner);
    }
    this.currentOwner = currentOwner;
  }

  public long getId() {
    return id;
  }

  public List<Person> getPastOwners() {
    return pastOwners;
  }

  public ParcelState getState() {
    return state;
  }

  public void setState(ParcelStateLeased state) {
    this.state = state;
  }

  public String getLocation() {
    return location;
  }

  public float getSquaremeters() {
    return squaremeters;
  }

  /**
   * Convert squaremeter size to an other unit with conversion factor. Used for l10n and i18n.
   *
   * @param bias conversion factor. must be parseable by {@link Float#parseFloat(String)}.
   */
  public Float getBiasedSize(String bias) {
    return squaremeters * Float.parseFloat(bias);
  }

  public void sell() {
    state = state.sell(this);
  }

  /**
   * Lease the Parcel.
   *
   * @param person new owener
   * @return current parcel instance
   */
  public Parcel lease(Person person) {
    state = state.lease(person, this);
    //person.addParcel(this);
    return this;
  }

  @Override
  public String toString() {
    return String.format("%s (%d)", location, id);
  }
}
