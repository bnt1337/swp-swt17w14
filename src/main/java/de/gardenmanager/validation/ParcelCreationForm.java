package de.gardenmanager.validation;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class ParcelCreationForm {

  @NotNull
  private float size;
  @NotEmpty
  @Length(max = 255)
  private String location;

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public float getSize() {
    return size;
  }

  public void setSize(float size) {
    this.size = size;
  }
}
