package de.gardenmanager.validation;

import java.time.LocalDate;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class PersonCreationForm {

  @NotEmpty
  @Length(max = 255)
  private String firstname;
  @NotEmpty
  @Length(max = 255)
  private String lastname;

  //@NotEmpty
  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate birthday;

  @NotEmpty
  @Length(max = 255)
  private String street;
  @NotEmpty
  @Length(max = 255)
  private String city;
  @NotEmpty
  @Length(min = 5, max = 5)
  private String zipCode;
  @NotEmpty
  @Length(max = 255)
  private String streetNumber;
  @NotEmpty
  @Length(max = 255)
  private String phoneNumber;
  @Email
  @Length(max = 255)
  private String email;
  @NotEmpty
  @Length(max = 255)
  private String username;
  @NotEmpty
  @Length(min = 8)
  private String password;

  private Long parcel;

  public LocalDate getBirthday() {
    return birthday;
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Long getParcel() {
    return parcel;
  }

  public void setParcel(Long parcel) {
    this.parcel = parcel;
  }
}
