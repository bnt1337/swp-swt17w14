package de.gardenmanager.validation;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * form-class for connecting frontend and backend.
 */
public class WarningCreationForm {

  @NotEmpty(message = "{WarningCreationForm.description.NotEmpty}")
  @Length(max = 255)
  private String description;

  @NotNull(message = "{WarningCreationForm.tenant.NotEmpty}")
  private Long tenantId;

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getTenantId() {
    return this.tenantId;
  }

  public void setTenantId(Long tenant) {
    this.tenantId = tenant;
  }
}
