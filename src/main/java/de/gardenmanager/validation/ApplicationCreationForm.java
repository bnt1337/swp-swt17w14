package de.gardenmanager.validation;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class ApplicationCreationForm {

  @NotEmpty
  @Length(max = 255)
  private String name;

  @NotEmpty
  @Length(max = 255)
  private String firstname;

  @NotEmpty
  @Length(max = 255)
  private String telephone;

  @Email
  @NotEmpty
  @Length(max = 255)
  private String email;

  @NotEmpty
  @Length(max = 255)
  private String parcel;

  public String getName() {
    return name;
  }

  public void setName(String lastname) {
    this.name = lastname;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getParcel() {
    return parcel;
  }

  public void setParcel(String parcel) {
    this.parcel = parcel;
  }
}