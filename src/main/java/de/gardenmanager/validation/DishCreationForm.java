package de.gardenmanager.validation;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class DishCreationForm {

  @NotEmpty(message = "{DishCreationForm.name.NotEmpty}")
  @Length(max = 255)
  private String name;

  @NotEmpty(message = "{DishCreationForm.allergen.NotEmpty}")
  @Length(max = 255)
  private String allergen;

  @NotNull(message = "{DishCreationForm.price.NotNull}")
  @Min(0)
  private Number price;

  @NotEmpty(message = "{DishCreationForm.Type.NotEmpty}")
  private String dishType;

  public String getDishType() {
    return dishType;
  }

  public void setDishType(String dishType) {
    this.dishType = dishType;
  }

  public Number getPrice() {
    return price;
  }

  public void setPrice(Number price) {
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAllergen() {
    return allergen;
  }

  public void setAllergen(String allergen) {
    this.allergen = allergen;
  }

}
