package de.gardenmanager.validation;

import java.util.List;
import javax.validation.constraints.NotNull;

public class ParcelDomainAddForm {
  @NotNull
  private Long chairman;
  @NotNull
  private List<Long> parcels;

  public Long getChairman() {
    return chairman;
  }

  public void setChairman(Long chairman) {
    this.chairman = chairman;
  }

  public List<Long> getParcels() {
    return parcels;
  }

  public void setParcels(List<Long> parcels) {
    this.parcels = parcels;
  }
}
