package de.gardenmanager.validation;

import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * Created by bennet on 09.12.2017.
 */
public class MeterReadingForm {

  @NotNull
  @DecimalMin(value = "0.0", inclusive = true)
  private BigDecimal water;
  @NotNull
  @DecimalMin(value = "0.0", inclusive = true)
  private BigDecimal electricity;

  public BigDecimal getWater() {
    return water;
  }

  public void setWater(BigDecimal water) {
    this.water = water;
  }

  public BigDecimal getElectricity() {
    return electricity;
  }

  public void setElectricity(BigDecimal electricity) {
    this.electricity = electricity;
  }
}
