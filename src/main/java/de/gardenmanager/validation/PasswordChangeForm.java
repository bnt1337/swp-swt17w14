package de.gardenmanager.validation;

import javax.validation.constraints.Size;

public class PasswordChangeForm {
  private String currentpassword;
  @Size(min = 8)
  private String newpassword;
  @Size(min = 8)
  private String newpasswordagain;

  public String getCurrentpassword() {
    return currentpassword;
  }

  public void setCurrentpassword(String currentpassword) {
    this.currentpassword = currentpassword;
  }

  public String getNewpassword() {
    return newpassword;
  }

  public void setNewpassword(String newpassword) {
    this.newpassword = newpassword;
  }

  public String getNewpasswordagain() {
    return newpasswordagain;
  }

  public void setNewpasswordagain(String newpasswordagain) {
    this.newpasswordagain = newpasswordagain;
  }
}
