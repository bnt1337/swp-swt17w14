package de.gardenmanager.validation;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Created by bennet on 14.11.17.
 */
public class PreorderCreationForm {

  @NotEmpty(message = "{EventCreationForm.firstname.NotEmpty}")
  @Length(max = 255)
  public String firstname;

  @NotEmpty(message = "{EventCreationForm.lastname.NotEmpty}")
  @Length(max = 255)
  public String lastname;

  @NotNull(message = "{EventCreationForm.date.NotEmpty}")
  @DateTimeFormat(iso = ISO.DATE_TIME)
  public LocalDateTime date;

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }
}