package de.gardenmanager.validation;

import java.util.List;
import javax.validation.constraints.NotNull;

public class PersonRoleAddForm {
  @NotNull
  private Long person;
  @NotNull
  private List<String> roles;

  public Long getPerson() {
    return person;
  }

  public void setPerson(Long person) {
    this.person = person;
  }

  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }
}
