package de.gardenmanager.validation;

import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Created by bennet on 14.11.17.
 */
public class EventCreationForm {

  @NotEmpty(message = "{EventCreationForm.name.NotEmpty}")
  @Length(max = 255)
  private String name;

  @NotEmpty(message = "{EventCreationForm.description.NotEmpty}")
  private String description;

  @NotNull(message = "{EventCreationForm.start.NotEmpty}")
  @DateTimeFormat(iso = ISO.DATE_TIME)
  private ZonedDateTime start;

  @NotNull(message = "{EventCreationForm.end.NotEmpty}")
  @DateTimeFormat(iso = ISO.DATE_TIME)
  private ZonedDateTime end;

  private Boolean assignment;

  private Boolean publicvisible;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ZonedDateTime getStart() {
    return start;
  }

  public void setStart(ZonedDateTime start) {
    this.start = start;
  }

  public ZonedDateTime getEnd() {
    return end;
  }

  public void setEnd(ZonedDateTime end) {
    this.end = end;
  }

  public Boolean getAssignment() {
    return assignment;
  }

  public void setAssignment(Boolean assignment) {
    this.assignment = assignment;
  }

  public Boolean getPublicvisible() {
    return publicvisible;
  }

  public void setPublicvisible(Boolean publicvisible) {
    this.publicvisible = publicvisible;
  }
}