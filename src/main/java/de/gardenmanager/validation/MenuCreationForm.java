package de.gardenmanager.validation;

import java.util.List;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * form to access menu data.
 */
public class MenuCreationForm {

  @NotEmpty(message = "{MenuCreationForm.name.NotEmpty}")
  @Length(max = 255)
  private String name;

  @NotEmpty(message = "{MenuCreationForm.starters.NotEmpty}")
  private List<String> starters;

  @NotEmpty(message = "{MenuCreationForm.maincourses.NotEmpty}")
  private List<String> maincourses;

  @NotEmpty(message = "{MenuCreationForm.desserts.NotEmpty}")
  private List<String> desserts;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getStarters() {
    return this.starters;
  }

  public void setStarters(List<String> starters) {
    this.starters = starters;
  }

  public List<String> getMaincourses() {
    return this.maincourses;
  }

  public void setMaincourses(List<String> maincourses) {
    this.maincourses = maincourses;
  }

  public List<String> getDesserts() {
    return this.desserts;
  }

  public void setDesserts(List<String> desserts) {
    this.desserts = desserts;
  }
}
