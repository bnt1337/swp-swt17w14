package de.gardenmanager.validation;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class RequestForm {

  @NotEmpty
  @Length(max = 255)
  private String title;
  @NotEmpty
  @Length(max = 255)
  private String text;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
