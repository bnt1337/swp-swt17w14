package de.gardenmanager.validation;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AssignmentApplicationForm {

  @NotNull
  @Min(0)
  public Short hours;

  public Short getHours() {
    return hours;
  }

  public void setHours(Short hours) {
    this.hours = hours;
  }
}
