package de.gardenmanager.validation;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AssignmentValidationForm {

  @NotNull
  @Min(0)
  public Short hoursDone;

  public Short getHoursDone() {
    return hoursDone;
  }

  public void setHoursDone(Short hoursDone) {
    this.hoursDone = hoursDone;
  }
}
