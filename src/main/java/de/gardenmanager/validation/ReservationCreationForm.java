package de.gardenmanager.validation;

import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * form-class for connecting frontend and backend.
 */
public class ReservationCreationForm {

  @NotEmpty(message = "{ReservationCreationForm.description.NotEmpty}")
  @Length(max = 255)
  private String description;

  @NotEmpty(message = "{ReservationCreationForm.owner.NotEmpty}")
  @Length(max = 255)
  private String owner;

  @NotNull(message = "{ReservationCreationForm.start.NotEmpty}")
  @DateTimeFormat(iso = ISO.DATE_TIME)
  private ZonedDateTime start;

  @NotNull(message = "{ReservationCreationForm.end.NotEmpty}")
  @DateTimeFormat(iso = ISO.DATE_TIME)
  private ZonedDateTime end;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public ZonedDateTime getStart() {
    return start;
  }

  public void setStart(ZonedDateTime start) {
    this.start = start;
  }

  public ZonedDateTime getEnd() {
    return end;
  }

  public void setEnd(ZonedDateTime end) {
    this.end = end;
  }
}