package de.gardenmanager.restaurant;

import de.gardenmanager.restaurant.Dish.DishType;
import java.util.ArrayList;
import java.util.List;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;

/**
 * business-logic for the Restaurant-Controller.
 */
@Component
public class Restaurant {

  private final MenuCatalog menuCatalog;
  private final DishCatalog dishCatalog;
  private OrderItemRepository orderItemRepository;

  /**
   * constructor of the class.
   *
   * @param menu reference to the menu
   * @param dishCatalog the dish-catalog where the dishes are stored
   */
  public Restaurant(MenuCatalog menu, DishCatalog dishCatalog,
      OrderItemRepository orderItemRepository) {
    this.menuCatalog = menu;
    this.dishCatalog = dishCatalog;
    this.orderItemRepository = orderItemRepository;
  }

  /**
   * creates an new dish with the given parameters.
   *
   * @param name the name of the dish
   * @param allergens allergens in the dish
   * @param price price of the dish
   * @param type type of the dish as string
   * @return the new dish
   */
  public Dish createDish(String name, String allergens, Money price, String type) {

    switch (type) {
      case "Hauptgericht": {
        Dish dish = new Dish(name, allergens, price, DishType.MAINCOURSE);
        dishCatalog.save(dish);
        return dish;
      }
      case "Dessert": {
        Dish dish = new Dish(name, allergens, price, DishType.DESSERT);
        dishCatalog.save(dish);
        return dish;
      }
      default: {  // type is starter
        Dish dish = new Dish(name, allergens, price, DishType.STARTER);
        dishCatalog.save(dish);
        return dish;
      }
    }
  }

  /**
   * deletes the dish given by the name of the dish.
   *
   * @param name the dish of the dish which should be removed.
   */
  public void deleteDish(String name) {
    dishCatalog.delete(dishCatalog.findByNameOrderByNameAsc(name));
  }

  /**
   * returns all dishs with all their properties.
   *
   * @return the dishs with all their properties.
   */
  public Iterable<Dish> showDishs() {
    return this.dishCatalog.findAll();
  }

  /**
   * returns all dishes with the STARTER property.
   *
   * @return all starters.
   */
  public Iterable<Dish> getStarters() {
    return this.dishCatalog.findByDishType(DishType.STARTER);
  }

  /**
   * returns all dishes with the MAINCOURSE property.
   *
   * @return all main courses.
   */
  public Iterable<Dish> getMainCourses() {
    return this.dishCatalog.findByDishType(DishType.MAINCOURSE);
  }

  /**
   * returns all dishes with the DESSERT property.
   *
   * @return all desserts.
   */
  public Iterable<Dish> getDesserts() {
    return this.dishCatalog.findByDishType(DishType.DESSERT);
  }

  /**
   * creates a new menu with the given name and dishes.
   *
   * @param name name of the new menu.
   * @param starters the list of all starters for the menu.
   * @param maincourses the list of all main courses for the menu.
   * @param desserts the list of all desserts for the menu.
   */
  public void createMenuByObjects(String name, List<Dish> starters, List<Dish> maincourses,
      List<Dish> desserts) {
    Menu menu = new Menu(name, new ArrayList<>(starters), new ArrayList<>(maincourses),
        new ArrayList<>(desserts));

    this.menuCatalog.save(menu);
  }

  /**
   * creates a new menu with the given name and dishes.
   *
   * @param name name of the new menu.
   * @param starters the list of all starters for the menu.
   * @param maincourses the list of all main courses for the menu.
   * @param desserts the list of all desserts for the menu.
   */
  public void createMenu(String name, List<String> starters, List<String> maincourses,
      List<String> desserts) {
    ArrayList<Dish> starterList = new ArrayList<>();
    ArrayList<Dish> mainCourseList = new ArrayList<>();
    ArrayList<Dish> dessertList = new ArrayList<>();

    for (String starter : starters) {
      starterList.add(dishCatalog.findFirstByName(starter));
    }
    for (String mainCourse : maincourses) {
      mainCourseList.add(dishCatalog.findFirstByName(mainCourse));
    }
    for (String dessert : desserts) {
      dessertList.add(dishCatalog.findFirstByName(dessert));
    }

    this.menuCatalog.save(new Menu(name, starterList, mainCourseList, dessertList));
  }

  /**
   * deletes the menu given by name.
   *
   * @param name the name of the menu to delete.
   */
  public void deleteMenu(String name) {
    Iterable<Menu> m = this.menuCatalog.findByName(name);
    this.menuCatalog.delete(m);
  }

  /**
   * returns all menus.
   *
   * @return returns all menus created.
   */
  public Iterable<Menu> showMenus() {
    return this.menuCatalog.findAll();
  }

  /**
   * find all {@link Dish}es with {@link Dish#dishType} {@link DishType#STARTER} which are in public
   * menus. For the method and their usage to make sense, we have to asume, that only one {@link
   * Menu} is public at a time.
   */
  public ArrayList<Dish> showPublicStarters() {
    ArrayList<Dish> starters = new ArrayList<Dish>();
    for (Menu menu : this.menuCatalog.findByStatePublic(true)) {
      starters.addAll(menu.getStarters());
    }
    return starters;
  }

  /**
   * find all {@link Dish}es with {@link Dish#dishType} {@link DishType#MAINCOURSE} which are in
   * public menus. For the method and their usage to make sense, we have to asume, that only one
   * {@link Menu} is public at a time.
   */
  public ArrayList<Dish> showPublicMaincourses() {
    ArrayList<Dish> maincourses = new ArrayList<Dish>();
    for (Menu menu : this.menuCatalog.findByStatePublic(true)) {
      maincourses.addAll(menu.getMainCourses());
    }
    return maincourses;
  }

  /**
   * find all {@link Dish}es with {@link Dish#dishType} {@link DishType#DESSERT} which are in public
   * menus. For the method and their usage to make sense, we have to asume, that only one {@link
   * Menu} is public at a time.
   */
  public ArrayList<Dish> showPublicDesserts() {
    ArrayList<Dish> desserts = new ArrayList<Dish>();
    for (Menu menu : this.menuCatalog.findByStatePublic(true)) {
      desserts.addAll(menu.getDesserts());
    }
    return desserts;
  }

  /**
   * sets the state of the menu.
   *
   * @param name the name of the menu to change the state
   * @param statePublic the state to set (true: public, private otherwise)
   */
  public void setMenuState(String name, boolean statePublic) {
    this.menuCatalog.findFirstByName(name).setStatePublic(statePublic);
  }

  /**
   * returns the state (public or ptivate).
   *
   * @param name the name of the menu to change the state
   * @return the current satate true if public, false otherwise
   */
  public boolean getMenuState(String name) {
    return this.menuCatalog.findFirstByName(name).getStatePublic();
  }

  public void saveOrderItems(Preorder preorder) {
    List<OrderItem> orderItems = preorder.getOrderItems();
    for (OrderItem orderItem : orderItems) {
      orderItemRepository.save(orderItem);
    }
  }
}
