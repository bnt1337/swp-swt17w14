package de.gardenmanager.restaurant;

import java.util.Optional;
import org.salespointframework.catalog.Catalog;
import org.salespointframework.catalog.ProductIdentifier;

/**
 * the menu catalog to store the menus.
 */
public interface MenuCatalog extends Catalog<Menu> {

  //static final Sort DEFAULT_SORT = new Sort(Direction.DESC, "productIdentifier");

  Iterable<Menu> findByStatePublic(boolean state);

  Iterable<Menu> findAll();

  Menu findFirstByName(String name);

  //Iterable<Dish> findById(ProductIdentifier id, Sort sort);

  //default Iterable<Dish> findById(ProductIdentifier id) {
  //  return findById(id, DEFAULT_SORT);
  //}


  /**
   * Retrieves an entity by its id.
   *
   * @param productIdentifier must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  Optional<Menu> findOne(ProductIdentifier productIdentifier);
}
