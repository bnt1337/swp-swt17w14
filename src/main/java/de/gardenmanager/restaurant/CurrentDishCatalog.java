package de.gardenmanager.restaurant;

import org.salespointframework.catalog.Catalog;

public interface CurrentDishCatalog extends Catalog<Dish> {

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  Iterable<Dish> findAll();
}
