package de.gardenmanager.restaurant;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Preorder implements Serializable {

  private static final long serialVersionUID = 2139940606084898331L;

  @GeneratedValue
  @Id
  private long id;

  @ManyToMany
  private List<OrderItem> orderItems;
  private String firstname;
  private String lastname;
  @Column(columnDefinition = "TIMESTAMP", name = "startdate")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime date;

  public Preorder() {
    orderItems = new ArrayList<OrderItem>();
  }

  public void addDish(Dish dish, int number) {
    for (OrderItem item : orderItems) {
      if (item.getName().equals(dish.getName())) {
        item.increaseQuantity(number);
        return;
      }
    }
    OrderItem orderItem = new OrderItem(dish.getName(), number);
    orderItems.add(orderItem);
  }

  public void delateItem(long id) {
    int counter = 0;
    for (OrderItem orderItem : orderItems) {
      if (orderItem.getId() == id) {
        orderItems.remove(counter);
        return;
      }
      counter++;
    }
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public String dateString() {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyy");
    return String
        .format("%s", format.format(date));
  }
}