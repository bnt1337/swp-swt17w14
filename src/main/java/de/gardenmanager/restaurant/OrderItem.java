package de.gardenmanager.restaurant;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class OrderItem implements Serializable {

  private static final long serialVersionUID = -6266240770553570796L;

  @GeneratedValue
  @Id
  private long id;
  
  private String name;
  private int quantity;

  @SuppressWarnings("unused")
  @Deprecated
  private OrderItem() {
    // Because Spring needs it :(
  }

  public OrderItem(String name, int quantity) {
    this.name = name;
    this.quantity = quantity;
  }

  public void increaseQuantity(int number) {
    quantity += number;
  }

  public String getName() {
    return name;
  }

  public int getQuantity() {
    return quantity;
  }

  public long getId() {
    return id;
  }
}
