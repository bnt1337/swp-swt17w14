package de.gardenmanager.restaurant;

import de.gardenmanager.restaurant.Dish.DishType;
import org.salespointframework.catalog.Catalog;

/**
 * the catalog to store the dishs.
 */
public interface DishCatalog extends Catalog<Dish> {

  Iterable<Dish> findByDishType(DishType type);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  Iterable<Dish> findAll();

  Iterable<Dish> findByNameOrderByNameAsc(String name);

  Dish findFirstByName(String name);
}
