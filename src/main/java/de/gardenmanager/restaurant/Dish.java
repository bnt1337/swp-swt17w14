package de.gardenmanager.restaurant;

import javax.persistence.Entity;
import org.javamoney.moneta.Money;
import org.salespointframework.catalog.Product;

/**
 * this is the object dish.
 */
@Entity
public class Dish extends Product {

  private static final long serialVersionUID = 201711121347L;
  private String allergens;
  private DishType dishType;

  /**
   * default constructor.
   */
  @Deprecated
  @SuppressWarnings("unused")
  private Dish() {
    //Because Spring needs it :(
  }

  /**
   * the constructor to create a dish.
   *
   * @param name Dish name. must not be {@literal null} or empty.
   * @param allergens must not be {@literal null} or empty.
   * @param price the price of the {@link Dish}, must not be {@literal null}.
   * @param dishType {@link Dish.DishType} of the {@link Dish}.
   */
  public Dish(String name, String allergens, Money price, DishType dishType) {
    super(name, price);
    this.allergens = allergens;
    this.dishType = dishType;
  }

  public String getAllergens() {
    return allergens;
  }

  public DishType getDishType() {
    return dishType;
  }

  public void setDishType(DishType dishType) {
    this.dishType = dishType;
  }

  /**
   * the enum dish type describes the txpe of the dish (starter / maincourse / dessert).
   */
  public enum DishType {
    STARTER, DESSERT, MAINCOURSE
  }

}