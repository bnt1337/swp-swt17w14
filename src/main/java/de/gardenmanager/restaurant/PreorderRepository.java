package de.gardenmanager.restaurant;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PreorderRepository extends CrudRepository<Preorder, Long> {

  @Override
  Iterable<Preorder> findAll();
}
