package de.gardenmanager.restaurant;

import static org.salespointframework.core.Currencies.EURO;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import org.javamoney.moneta.Money;
import org.salespointframework.catalog.Product;

/**
 * the class menu describes the object menu.
 */
@Entity
public class Menu extends Product {

  @Id
  private static final long serialVersionUID = 201712071005L;

  @ManyToMany(cascade = CascadeType.ALL)
  private List<Dish> starterList = new ArrayList<>();
  @ManyToMany(cascade = CascadeType.ALL)
  private List<Dish> maincourseList = new ArrayList<>();
  @ManyToMany(cascade = CascadeType.ALL)
  private List<Dish> dessertList = new ArrayList<>();

  private boolean statePublic;

  /**
   * default constructor.
   */
  @Deprecated
  @SuppressWarnings("unused")
  private Menu() {
    //Because Spring needs it :(
  }

  /**
   * constructor to create a new menu.
   *
   * @param name Menu name. must not be {@literal null} or empty.
   * @param starterList must not be {@literal null}.
   * @param maincourseList must not be {@literal null}.
   * @param dessertList must not be {@literal null}.
   */
  public Menu(String name, ArrayList<Dish> starterList, ArrayList<Dish> maincourseList,
      ArrayList<Dish> dessertList) {
    super(name, Money.zero(EURO));

    this.starterList = starterList;
    this.maincourseList = maincourseList;
    this.dessertList = dessertList;
    this.statePublic = false;
  }

  /**
   * getter for the state.
   *
   * @return true if public, false otherwise
   */
  public boolean getStatePublic() {
    return this.statePublic;
  }

  /**
   * setter for the state.
   *
   * @param statePublic {@link Boolean} public state
   */
  public void setStatePublic(boolean statePublic) {
    this.statePublic = statePublic;
  }

  /**
   * getter for the starters.
   *
   * @return list of saved starter
   */
  public List<Dish> getStarters() {
    return this.starterList;
  }

  /**
   * getter for the main courses.
   *
   * @return list of saved maincourses.
   */
  public List<Dish> getMainCourses() {
    return this.maincourseList;
  }

  /**
   * getter for the desserts.
   *
   * @return list of saved desserts.
   */
  public List<Dish> getDesserts() {
    return this.dessertList;
  }
}
