package de.gardenmanager.restaurant;

import java.util.Arrays;
import org.salespointframework.core.Streamable;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Class wrapping the UserAccount of restaurant employee.
 */
@Service
@Transactional
public class CatererManagement {

  private final CatererRepository caterers;
  private final UserAccountManager userAccounts;

  /**
   * Initialize the Caterer-Management.
   *
   * @param caterers must not be {@literal null}.
   * @param userAccounts must not be {@literal null}.
   */
  CatererManagement(CatererRepository caterers, UserAccountManager userAccounts) {

    Assert.notNull(caterers, "CatererRepository must not be null!");
    Assert.notNull(userAccounts, "UserAccountManager must not be null!");

    this.caterers = caterers;
    this.userAccounts = userAccounts;
  }

  /**
   * Creates a new {@link UserAccount} using the information given in the {@link
   * de.gardenmanager.validation.CatererRegistrationForm}.
   *
   * @param name must not be {@literal null}.
   * @param password must not be {@literal null}.
   * @return the created {@link UserAccount}
   */
  public UserAccount createCustomer(String name, String password) {

    Assert.notNull(name, "Name must not be null!");
    Assert.notNull(password, "Password must not be null!");

    UserAccount userAccount = userAccounts.create(name, password, Role.of("ROLE_CATERER"));

    return caterers.save(userAccount);
  }

  /**
   * Returns all instances of the type {@link UserAccount} in {@link CatererRepository}.
   *
   * @return all entities
   * @see CatererRepository#findAll()
   */
  public Streamable<UserAccount> findAll() {
    return Streamable.of(caterers.findAll());
  }

  /**
   * Returns all instances of the type with the given IDs.
   *
   * @param longs IDs to find.
   * @return found entities
   * @see CatererRepository#findAll(Iterable)
   */
  public Streamable<UserAccount> findAll(Iterable<Long> longs) {
    return Streamable.of(caterers.findAll(longs));
  }

  /**
   * Returns all instances of the type with the given IDs.
   *
   * @param ids IDs to find.
   * @return found entities
   * @see CatererRepository#findAll(Iterable)
   */
  public Streamable<UserAccount> findAll(Long... ids) {
    Iterable<Long> longs = Arrays.asList(ids);
    return Streamable.of(caterers.findAll(longs));
  }

  /**
   * Saves a given entity. Use the returned instance for further operations as the save operation
   * might have changed the entity instance completely.
   *
   * @param entity {@link UserAccount} to save
   * @return the saved entity on success otherwise {@literal null}
   * @see CatererRepository#save(UserAccount)
   */
  public UserAccount save(UserAccount entity) {
    return caterers.save(entity);
  }

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   * @see CatererRepository#findOne(Long)
   */
  public UserAccount findOne(Long id) {
    return caterers.findOne(id);
  }

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   * @see CatererRepository#exists(Long)
   */
  public boolean exists(Long id) {
    return caterers.exists(id);
  }

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   * @see CatererRepository#count()
   */
  long count() {
    return caterers.count();
  }
}
