package de.gardenmanager.restaurant;

import org.salespointframework.useraccount.UserAccount;
import org.springframework.data.repository.CrudRepository;

public interface CatererRepository extends CrudRepository<UserAccount, Long> {

  /**
   * Saves a given entity. Use the returned instance for further operations as the save operation
   * might have changed the entity instance completely.
   *
   * @return the saved entity
   */
  @Override
  UserAccount save(UserAccount entity);

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  UserAccount findOne(Long id);

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  boolean exists(Long id);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<UserAccount> findAll();

  /**
   * Returns all instances of the type with the given IDs.
   */
  @Override
  Iterable<UserAccount> findAll(Iterable<Long> longs);

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   */
  @Override
  long count();
}
