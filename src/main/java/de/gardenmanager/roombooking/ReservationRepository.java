package de.gardenmanager.roombooking;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, ReservationIdentifier> {

  /**
   * find reservation which overlap/collide with the specified start and end.
   *
   * @param start Start of the time frame to be checked
   * @param end end of the time frame to be checked
   * @return all colliding reservations
   */
  @Query("select id from Reservation r where "
      + "(r.start <= :startdate and r.end >= :startdate) or "
      + "(r.start <= :enddate and r.end >= :enddate) or "
      + "(r.start >= :startdate and r.end <= :enddate)")
  List<Reservation> getCollusions(@Param("startdate") LocalDateTime start,
      @Param("enddate") LocalDateTime end);
}
