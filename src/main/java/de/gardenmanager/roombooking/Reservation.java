package de.gardenmanager.roombooking;

import java.time.LocalDateTime;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import org.salespointframework.core.AbstractEntity;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * class to manage the bookings of the bookable room of the restaurant.
 */
@Entity
public class Reservation extends AbstractEntity<ReservationIdentifier> implements
    Comparable<Reservation> {

  private static final long serialVersionUID = 201711152149L;
  @EmbeddedId
  @AttributeOverride(name = "id", column = @Column(name = "id"))
  private ReservationIdentifier id = new ReservationIdentifier();
  @Column(columnDefinition = "TIMESTAMP", name = "startdate")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime start;
  @Column(columnDefinition = "TIMESTAMP", name = "enddate")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime end;
  private String owner;
  private String desciption;

  /**
   * default constructor.
   */
  @SuppressWarnings("unused")
  @Deprecated
  private Reservation() {
    //Because Spring needs it :(
  }

  /**
   * constructor to create a new reservation.
   */
  public Reservation(LocalDateTime start, LocalDateTime end, String owner,
      String desciption) {
    this.start = start;
    this.end = end;
    this.owner = owner;
    this.desciption = desciption;
  }

  public ReservationIdentifier getId() {
    return id;
  }

  public LocalDateTime getStart() {
    return start;
  }

  public LocalDateTime getEnd() {
    return end;
  }

  public String getOwner() {
    return owner;
  }

  public String getDesciption() {
    return desciption;
  }

  @Override
  public int compareTo(Reservation other) {
    return this.start.compareTo(other.start);
  }
}
