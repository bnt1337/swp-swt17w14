package de.gardenmanager.roombooking;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.QueryParameterException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * business logic to manage the reservations.
 */
@Service
@Transactional
public class RoomBookingsystem {

  private final ReservationRepository reservationRepository;

  /**
   * the constructor.
   */
  public RoomBookingsystem(ReservationRepository reservationRepository) {
    this.reservationRepository = reservationRepository;
  }

  /**
   * returns all reservations.
   *
   * @return all reservations an a list
   */
  public List<Reservation> getAll() {
    List<Reservation> list = new LinkedList<>();
    reservationRepository.findAll().forEach(list::add);
    return list;
  }

  /**
   * returns the reservation given by the reservationIdentifier.
   *
   * @param reservationIdentifier the identifer
   * @return the requested reservation
   */
  public Reservation getReservation(ReservationIdentifier reservationIdentifier) {
    return reservationRepository.findOne(reservationIdentifier);
  }

  /**
   * create a new reservation.
   *
   * @param start the start time of the reservation
   * @param end the end time of the reservation
   * @param description the description of the reservation
   * @param owner the owner of the reservation
   * @return the requested reservation
   */
  public Reservation createReservation(LocalDateTime start, LocalDateTime end,
      String description, String owner) {
    if (reservationRepository.getCollusions(start, end).size() > 0) {
      throw new QueryParameterException("collusion");
    }

    return reservationRepository.save(new Reservation(start, end, owner, description));
  }

  /**
   * delete the reservation given by the reservationIdentifier.
   *
   * @param reservationIdentifier the identifer
   */
  public void deleteReservation(ReservationIdentifier reservationIdentifier) {
    reservationRepository.delete(reservationIdentifier);
  }

}
