///*
// * Copyright 2013-2015 the original author or authors.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
package de.gardenmanager.util;
//
//import static org.salespointframework.core.Currencies.EURO;
//
//import java.time.Instant;
//import java.util.ArrayList;
//import java.util.Date;
//
//import org.javamoney.moneta.Money;
//import org.salespointframework.core.DataInitializer;
//import org.salespointframework.useraccount.Role;
//import org.salespointframework.useraccount.UserAccountManager;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import de.gardenmanager.accounting.AccountingSystem;
//import de.gardenmanager.person.Address;
//import de.gardenmanager.person.ContactInfo;
//import de.gardenmanager.person.Parcel;
//import de.gardenmanager.person.ParcelRepository;
//import de.gardenmanager.person.Person;
//import de.gardenmanager.person.PersonManagement;
//import de.gardenmanager.person.PersonRepository;
//import de.gardenmanager.restaurant.Dish;
//import de.gardenmanager.restaurant.Dish.DishType;
//import de.gardenmanager.restaurant.DishCatalog;
//import de.gardenmanager.restaurant.Menu;
//import de.gardenmanager.restaurant.MenuCatalog;
//
//@Component
//public class GardenManagerDataInitializer implements DataInitializer {
//
//  private DishCatalog dishCatalog;
//  private MenuCatalog menuCatalog;
//  private ParcelRepository parcelRepository;
//  private UserAccountManager userAccountManager;
//  private PersonRepository personRepository;
//  private AccountingSystem accounting;
//  private PersonManagement personManagement;
//
//  @Autowired
//  public GardenManagerDataInitializer(DishCatalog dishCatalog, MenuCatalog menuCatalog,
//      ParcelRepository parcelRepository, UserAccountManager userAccountManager,
//      PersonRepository personRepository,
//      PersonManagement personManagement, AccountingSystem accounting) {
//    this.dishCatalog = dishCatalog;
//    this.menuCatalog = menuCatalog;
//    this.parcelRepository = parcelRepository;
//    this.userAccountManager = userAccountManager;
//    this.personRepository = personRepository;
//    this.personManagement = personManagement;
//    this.accounting = accounting;
//  }
//
//  @Override
//  public void initialize() {
//    initializeRestaurant();
//    initializeParcels();
//    initializePersonsAndParcels();
//    initializeTenants();
//  }
//
//  private void initializeRestaurant() {
//    Dish dessert1 = new Dish("Erdbeeren", "lecker", Money.of(20, EURO), DishType.DESSERT);
//    Dish dessert2 = new Dish("Birnen", "nicht so lecker", Money.of(15, EURO), DishType.DESSERT);
//    Dish dessert3 = new Dish("Vanilleeis", "lecker", Money.of(20, EURO), DishType.DESSERT);
//    Dish dessert4 = new Dish("Fischpudding", "nicht so lecker", Money.of(15, EURO),
//        DishType.DESSERT);
//    dishCatalog.save(dessert1);
//    dishCatalog.save(dessert2);
//    dishCatalog.save(dessert3);
//    dishCatalog.save(dessert4);
//
//    Dish starter1 = new Dish("Hühnersuppe", "lecker", Money.of(20, EURO), DishType.STARTER);
//    Dish starter2 = new Dish("Brot", "nicht so lecker", Money.of(15, EURO), DishType.STARTER);
//    Dish starter3 = new Dish("Brut mit Ei", "lecker", Money.of(20, EURO), DishType.STARTER);
//    dishCatalog.save(starter1);
//    dishCatalog.save(starter2);
//    dishCatalog.save(starter3);
//
//    Dish main1 = new Dish("Pommes mit Bratkartoffeln", "lecker", Money.of(20, EURO),
//        DishType.MAINCOURSE);
//    Dish main2 = new Dish("Bartoffelbrei mit Leberkäse", "nicht so lecker", Money.of(15, EURO),
//        DishType.MAINCOURSE);
//    Dish main3 = new Dish("Pellkartoffeln mit Qaurk", "lecker", Money.of(20, EURO),
//        DishType.MAINCOURSE);
//    dishCatalog.save(main1);
//    dishCatalog.save(main2);
//    dishCatalog.save(main3);
//
//    ArrayList<Dish> starters1 = new ArrayList<Dish>() {
//      {
//        add(starter1);
//        add(starter2);
//        add(starter3);
//      }
//    };
//
//    ArrayList<Dish> mains1 = new ArrayList<Dish>() {
//      {
//        add(main1);
//        add(main2);
//        add(main3);
//      }
//    };
//
//    ArrayList<Dish> desserts1 = new ArrayList<Dish>() {
//      {
//        add(dessert1);
//        add(dessert2);
//      }
//    };
//    ArrayList<Dish> desserts2 = new ArrayList<Dish>() {
//      {
//        add(dessert3);
//        add(dessert4);
//      }
//    };
//    Menu menu1 = new Menu("Lecker Essen", starters1, new ArrayList<Dish>(), desserts1);
//    Menu menu2 = new Menu("Lecker Essen", new ArrayList<Dish>(), mains1, desserts2);
//    menu1.setStatePublic(true);
//    menu2.setStatePublic(true);
//    menuCatalog.save(menu1);
//    menuCatalog.save(menu2);
//  }
//
//  private void initializeParcels() {
//    Parcel parcel1 = new Parcel("A1", 250f);
//    Parcel parcel2 = new Parcel("A2", 400f);
//    Parcel parcel3 = new Parcel("A3", 375f);
//    parcelRepository.save(parcel1);
//    parcelRepository.save(parcel2);
//    parcelRepository.save(parcel3);
//  }
//
//  public void initializePersonsAndParcels() {
//    Parcel parcel1 = new Parcel("A4", 140);
//    Parcel parcel2 = new Parcel("A5", 234);
//    Parcel parcel3 = new Parcel("A6", 736);
//    parcelRepository.save(parcel1);
//    parcelRepository.save(parcel2);
//    parcelRepository.save(parcel3);
//
//    Person person1 = new Person("Heinz", "Abendbrot", Date.from(Instant.now()),
//        new Address("AeCh6ong8Kil1chai5aiX8OhYe2Tee", "", "", ""), new ContactInfo("", ""),
//        userAccountManager.create("t", "123", Role.of("ROLE_TENANTRY")));
//    Person person2 = new Person("Friedrich", "Schnürsocke", Date.from(Instant.now()),
//        new Address("AeCh6ong8Kil1chai5aiX8OhYe2Tee", "", "", ""), new ContactInfo("", ""),
//        userAccountManager.create("s", "123", Role.of("ROLE_TREASURER")));
//    personRepository.save(person1);
//    personRepository.save(person2);
//    parcel1.lease(person1);
//    parcel2.lease(person2);
//    accounting.writeAllInvoices();
//  }
//
//  private void initializeTenants() {
//    personManagement.createPerson("firstname", "lastname", java.util.Date.from(Instant.now()),
//        new Address("Ich-mag-Kekse-Allee", "Krümelstadt", "01234", "42"),
//        new ContactInfo("e@mail.com", "0123456789"),
//        userAccountManager.create("u1", "p", Role.of("EMPTY")));
//  }
//}
