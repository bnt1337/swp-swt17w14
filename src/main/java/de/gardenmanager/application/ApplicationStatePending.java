package de.gardenmanager.application;

public final class ApplicationStatePending extends ApplicationState {

  public ApplicationStatePending() {
    super();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#process(Application)
   */
  @Override
  public ApplicationState process(Application application) {
    throw new IllegalStateException();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#accept(Application)
   */
  @Override
  public ApplicationState accept(Application application) {
    return new ApplicationStateAccepted();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#decline(Application)
   */
  @Override
  public ApplicationState decline(Application application) {
    return new ApplicationStateDeclined();
  }
}
