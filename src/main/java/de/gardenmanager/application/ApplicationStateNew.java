package de.gardenmanager.application;

public final class ApplicationStateNew extends ApplicationState {

  public ApplicationStateNew() {
    super();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#process(Application) ()
   */
  @Override
  public ApplicationState process(Application application) {
    return new ApplicationStatePending();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#accept(Application) ()
   */
  @Override
  public ApplicationState accept(Application application) {
    return new ApplicationStateAccepted();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#decline(Application) ()
   */
  @Override
  public ApplicationState decline(Application application) {
    return new ApplicationStateDeclined();
  }
}
