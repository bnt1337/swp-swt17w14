package de.gardenmanager.application;

public final class ApplicationStateDeclined extends ApplicationState {

  public ApplicationStateDeclined() {
    super();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#process(Application)
   */
  @Override
  public ApplicationState process(Application application) {
    throw new IllegalStateException();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#accept(Application)
   */
  @Override
  public ApplicationState accept(Application application) {
    throw new IllegalStateException();
  }

  /**
   * {@inheritDoc}
   *
   * @see ApplicationState#decline(Application)
   */
  @Override
  public ApplicationState decline(Application application) {
    throw new IllegalStateException();
  }
}
