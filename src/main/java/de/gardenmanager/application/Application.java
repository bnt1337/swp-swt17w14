package de.gardenmanager.application;

import de.gardenmanager.person.Parcel;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public final class Application {

  @Id
  @GeneratedValue
  private long id;

  //@Column(columnDefinition = "VARBINARY")
  private ApplicationState state;

  private String name;
  private String firstname;
  private String telephone;
  private String email;

  @ManyToOne
  private Parcel parcel;

  @Deprecated
  @SuppressWarnings("unused")
  private Application() {
    //Because Spring needs it :(
  }

  /**
   * Create new Application.
   *
   * @param name Last name of applicant
   * @param firstname First name of applicant
   * @param telephone Tel. number of applicant
   * @param email Email address of applicant
   */
  public Application(String name, String firstname, String telephone, String email,
      Parcel parcel) {
    this.name = name;
    this.firstname = firstname;
    this.telephone = telephone;
    this.email = email;
    this.parcel = parcel;
    state = new ApplicationStateNew();
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getTelephone() {
    return telephone;
  }

  public String getEmail() {
    return email;
  }

  public Parcel getParcel() {
    return parcel;
  }

  public ApplicationState getState() {
    return state;
  }

  public Application process() {
    state = state.process(this);
    return this;
  }

  public Application accept() {
    state = state.accept(this);
    return this;
  }

  public Application decline() {
    state = state.decline(this);
    return this;
  }
}
