package de.gardenmanager.application;

import de.gardenmanager.person.Parcel;
import org.springframework.data.repository.CrudRepository;

public interface ApplicationRepository extends CrudRepository<Application, Long> {

  /**
   * {@inheritDoc}
   */
  Application findByNameOrderByNameAsc(String name);

  /**
   * {@inheritDoc}
   */
  Application findById(long id);

  /**
   * Find all entities by their referenced parcel.
   *
   * @param parcel {@link Parcel} to search by
   * @return found {@link Application}s
   */
  Iterable<Application> findAllByParcel(Parcel parcel);
}