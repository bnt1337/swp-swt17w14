package de.gardenmanager.application;

import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.PersonManagement;
import de.gardenmanager.person.Roles;
import de.gardenmanager.validation.ApplicationCreationForm;
import java.util.Objects;
import org.salespointframework.core.Streamable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ApplicationManagement {

  private final ApplicationRepository applications;
  private final ParcelRepository parcels;
  private final MailSender mailSender;
  private final PersonManagement personManagement;

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * Initialize the ApplicationManagement with it's required AutoWired Beans.
   */
  public ApplicationManagement(ApplicationRepository applications, ParcelRepository parcels,
      MailSender mailSender, PersonManagement personManagement) {
    this.applications = applications;
    this.parcels = parcels;
    this.mailSender = mailSender;
    this.personManagement = personManagement;
  }

  /**
   * Creates a new Applicaten from the given{@link ApplicationCreationForm}.
   *
   * @param form Form
   * @return Application
   */
  public Application createApplication(ApplicationCreationForm form) {
    Parcel parcel = parcels.findByLocation(form.getParcel());
    if (parcel == null) {
      return null;
    }
    Application a = applications.save(
        new Application(form.getName(), form.getFirstname(), form.getTelephone(), form.getEmail(),
            parcel)
    );
    if (a != null) {
      try {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        personManagement.findAll()
            .filter(p -> p.getUserAccount().hasRole(Roles.ROLE_CHAIRMAN))
            .map(p -> p.getContactInfo().getEmail()).filter(Objects::nonNull)
            .forEach(mailMessage::setTo);
        //TODO: set system email
        mailMessage.setFrom("noreply@gardenmanager.example.com");
        mailMessage.setReplyTo(a.getEmail());
        //TODO: l10n
        mailMessage.setSubject("[GardenManager] New Application for Parcel " + parcel);
        mailMessage.setText(String.format(
            "New Application:\n"
                + " Applicant Name: %s, %s\n"
                + " Applicant Contact-Info: tel:%s mailto:%s\n"
                + " For Parcel: %s\n",
            a.getName(), a.getFirstname(), a.getTelephone(), a.getEmail(), a.getParcel()));

        mailSender.send(mailMessage);
      } catch (Exception e) {
        log.error("Failed to send email", e);
      }
    }
    return a;
  }

  /**
   * Deletes the application by its Id.
   *
   * @param id Application ID
   * @return Success
   */
  public boolean deleteApplication(long id) {
    try {
      applications.delete(id);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * accept an application. this automatically declines all other application for the parcel.
   */
  public void acceptApplication(Long id) {
    Application application = applications.findOne(id);
    application.accept();
    Streamable.of(applications.findAllByParcel(application.getParcel())).stream()
        .filter(a -> a.getId() != application.getId())
        .forEach(ad -> {
          ad.decline();
          applications.save(ad);
        });
    applications.save(application);
  }

  /**
   * decline an application.
   */
  public void declineApplication(Long id) {
    applications.save(applications.findOne(id).decline());
  }

  /**
   * Returns all {@link Application}s currently available in the system.
   */
  public Streamable<Application> findAll() {
    return Streamable.of(applications.findAll());
  }

  /**
   * Saves a given entity. Use the returned instance for further operations as the save operation
   * might have changed the entity instance completely.
   *
   * @return the saved entity
   */
  public Application save(Application application) {
    return applications.save(application);
  }

  /**
   * Retrieves an entity by its id.
   *
   * @param application must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  public Application fineOne(Long application) {
    return applications.findOne(application);
  }
}
