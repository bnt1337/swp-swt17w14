package de.gardenmanager.application;

import java.io.Serializable;

public abstract class ApplicationState implements Serializable {

  protected Application application;

  public ApplicationState() {
  }

  /**
   * Change Application state by marking it as processed (e.g. read, but not yet decided upon).
   *
   * @return new State
   */
  public abstract ApplicationState process(Application application);

  /**
   * Change Application state by marking it as accepted.
   *
   * @return new State
   */
  public abstract ApplicationState accept(Application application);


  /**
   * Change Application state by marking it as declined.
   *
   * @return new State
   */
  public abstract ApplicationState decline(Application application);


}
