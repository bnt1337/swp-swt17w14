/*
 * Copyright 2014.2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.gardenmanager;

import java.util.Locale;
import org.salespointframework.EnableSalespoint;
import org.salespointframework.SalespointSecurityConfiguration;
import org.salespointframework.SalespointWebConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;


@EnableSalespoint
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class GardenManager {

  public static void main(String[] args) {
    SpringApplication.run(GardenManager.class, args);
  }

  @Configuration
  static class HttpSecurityConfiguration extends SalespointSecurityConfiguration {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

      //http.csrf().disable();

      http.authorizeRequests()
          .antMatchers("/dashboard/**")
          .authenticated()
          .and()
          .formLogin()
          .loginPage("/loginpage")
          .loginProcessingUrl("/login")
          .defaultSuccessUrl("/dashboard")
          .failureUrl("/loginpage?error")
          .and()
          .logout()
          .logoutUrl("/logout")
          .logoutSuccessUrl("/");
    }
  }

  @Configuration
  public class WebConfig extends SalespointWebConfiguration {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
      registry.addViewController("/loginpage").setViewName("fe_loginpage");
      registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
      registry.addResourceHandler("/webjars/**")
          .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * Resolve Locales. Default to {@link Locale#ENGLISH}.
     *
     * @return {@link LocaleResolver} with resolved {@link Locale}
     */
    @Bean
    public LocaleResolver localeResolver() {
      SessionLocaleResolver slr = new SessionLocaleResolver();
      slr.setDefaultLocale(Locale.ENGLISH);
      return slr;
    }

    /**
     * Set URL Parameter to resolve locale by.
     *
     * @return {@link LocaleChangeInterceptor} for parameter
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
      LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
      lci.setParamName("lang");
      return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
      registry.addInterceptor(localeChangeInterceptor());

    }
  }

  //region GlobalMethodSecurityConfiguration
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
  public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {

    @Bean
    public RoleHierarchyVoter roleHierarchyVoter() {
      return new RoleHierarchyVoter(roleHierarchy());
    }

    /**
     * Set Role Hierarchy.
     *
     * @return the {@link RoleHierarchy}
     */
    @Bean
    public RoleHierarchy roleHierarchy() {
      RoleHierarchyImpl rhi = new RoleHierarchyImpl();
      rhi.setHierarchy(
          "ROLE_ADMIN > ROLE_CEO "
              + "ROLE_ADMIN > ROLE_CATERER "
              + "ROLE_CEO > ROLE_TREASURER "
              + "ROLE_CEO > ROLE_CHAIRMAN "
              + "ROLE_TREASURER > ROLE_TENANTRY "
              + "ROLE_CHAIRMAN > ROLE_TENANTRY");
      return rhi;
    }

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
      DefaultMethodSecurityExpressionHandler
          expressionHandler = new DefaultMethodSecurityExpressionHandler();
      // expressionHandler.setPermissionEvaluator(permissionEvaluator());
      expressionHandler.setRoleHierarchy(roleHierarchy());
      return expressionHandler;
    }
  }
  //endregion

}
