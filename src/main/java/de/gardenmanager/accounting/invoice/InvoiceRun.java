package de.gardenmanager.accounting.invoice;

import de.gardenmanager.util.LocalDateConverter;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class InvoiceRun {

  @Id
  @GeneratedValue
  private Long id;

  @Column(name = "date", columnDefinition = "DATE", unique = true)
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  @Convert(converter = LocalDateConverter.class)
  private LocalDate date;

  @Column(unique = true)
  private Short year;

  /**
   * Create an new Invoice Run. setting the year to <code>date</code>'s year
   *
   * @param date Invoice Run date
   */
  public InvoiceRun(LocalDate date) {
    this.date = date;
    year = (short) date.getYear();
  }

  @Deprecated
  private InvoiceRun() {
    //Bla Bla nested exception is org.hibernate.InstantiationException:
    // No default constructor for entity
  }

  public Long getId() {
    return id;
  }

  public LocalDate getDate() {
    return date;
  }

  public Short getYear() {
    return year;
  }
}
