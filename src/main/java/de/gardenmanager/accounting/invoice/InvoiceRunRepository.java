package de.gardenmanager.accounting.invoice;

import org.springframework.data.repository.CrudRepository;

public interface InvoiceRunRepository extends CrudRepository<InvoiceRun, Long> {

  /**
   * Saves a given entity. Use the returned instance for further operations as the save operation
   * might have changed the entity instance completely.
   *
   * @return the saved entity
   */
  @Override
  InvoiceRun save(InvoiceRun entity);

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  InvoiceRun findOne(Long id);

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  boolean exists(Long id);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<InvoiceRun> findAll();

  /**
   * Returns last recently InvoiceRun.
   *
   * @return newest InvoiceRun
   */
  InvoiceRun findFirstByOrderByDateAsc();

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   */
  @Override
  long count();

  /**
   * Deletes the entity with the given id.
   *
   * @param id must not be {@literal null}.
   * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
   */
  @Override
  void delete(Long id);

  /**
   * Deletes a given entity.
   *
   * @throws IllegalArgumentException in case the given entity is {@literal null}.
   */
  @Override
  void delete(InvoiceRun entity);
}
