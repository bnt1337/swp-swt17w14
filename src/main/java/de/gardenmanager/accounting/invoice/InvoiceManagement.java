package de.gardenmanager.accounting.invoice;

import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItem;
import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItemRepository;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.FixedPricingRepository;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.MeteredPricing;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.MeteredPricingRepository;
import de.gardenmanager.calendar.WorkingHours;
import de.gardenmanager.calendar.WorkingHoursRepository;
import de.gardenmanager.person.Parcel;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import org.salespointframework.core.Streamable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Component
@Service
@Transactional
public class InvoiceManagement {

  private final InvoiceRepository invoiceRepository;
  private final InvoiceItemRepository invoiceItemRepository;
  private final MeteredPricingRepository meteredPricingRepository;
  private final FixedPricingRepository fixedPricingRepository;
  private final WorkingHoursRepository workingHoursRepository;
  //private final MeteredInvoiceItemRepository meteredInvoiceItemRepository;

  /**
   * autowired ctor.
   *
   * @param invoiceRepository autowired bean
   * @param invoiceItemRepository autowired bean
   * @param meteredPricingRepository autowired bean
   * @param fixedPricingRepository autowired bean
   */
  public InvoiceManagement(InvoiceRepository invoiceRepository,
      InvoiceItemRepository invoiceItemRepository,
      MeteredPricingRepository meteredPricingRepository,
      FixedPricingRepository fixedPricingRepository,
      WorkingHoursRepository workingHoursRepository) {
    this.invoiceRepository = invoiceRepository;
    this.invoiceItemRepository = invoiceItemRepository;
    this.meteredPricingRepository = meteredPricingRepository;
    this.fixedPricingRepository = fixedPricingRepository;
    this.workingHoursRepository = workingHoursRepository;
  }

  /**
   * Create new Invoice for Parcel. Autofilling with Fixed Items
   *
   * @param parcel Parcel to create for
   * @return created Invoice
   */
  public Invoice createInvoice(Parcel parcel, InvoiceRun invoiceRun) {
    Invoice invoice = invoiceRepository.save(new Invoice(parcel, invoiceRun));
    //i.addInvoiceItem();
    Streamable.of(fixedPricingRepository.findAllByActive(true)).stream().forEach(
        pricing -> {
          try {
            invoiceRepository.save(invoice.addInvoiceItem(
                invoiceItemRepository
                    .save(new InvoiceItem(pricing.getName(), pricing.getPrice()))));
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
        });

    //TODO make this more dynamic with MeteredPricing
    MeteredPricing mp = meteredPricingRepository.findByTagAndActive("lease", true);
    MeteredPricing mpf = meteredPricingRepository.findByTagAndActive("fine", true);
    try {
      invoice.addInvoiceItem(invoiceItemRepository
          .save(
              new InvoiceItem(mp.getName(), mp.getTag(), mp.getValue(), parcel.getSquaremeters())));
      if (parcel.getCurrentOwner().getBirthday().plusYears(WorkingHours.AGE_THRESHOLD)
          .isAfter(LocalDate.now()
          )) {
        double hoursDone = Streamable.of(workingHoursRepository.findAll()).stream().filter(wh -> wh
            .getAssignment()
            .getEvent().getStart().getYear() == LocalDate.now().getYear()).mapToDouble(
            WorkingHours::getHoursDone).sum();
        if (hoursDone < WorkingHours.MIN_HOURS_PER_YEAR) {
          invoice.addInvoiceItem(invoiceItemRepository
              .save(
                  new InvoiceItem(mpf.getName(), mpf.getTag(), mpf.getValue(),
                      WorkingHours.MIN_HOURS_PER_YEAR - hoursDone)));
        }
      }
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }

    return invoiceRepository.save(invoice);
  }

  /**
   * Sign an Invoice by setting {@link Invoice#invoiceDate} to {@link LocalDateTime#now()}.
   *
   * @param invoiceId Invoice to sign
   * @throws IllegalAccessException date is already set
   */
  public void signInvoice(Long invoiceId) throws IllegalAccessException {
    invoiceRepository.findOne(invoiceId).setInvoiceDate(LocalDateTime.now());
  }

  public Streamable<Invoice> findAllByParcel(Parcel parcel) {
    return Streamable.of(invoiceRepository.findAllByParcel(parcel));
  }

  public Optional<Invoice> findByParcelAndInvoiceDateIsNull(Parcel parcel) {
    return invoiceRepository.findByParcelAndInvoiceDateIsNull(parcel);
  }

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  public Invoice findOne(Long id) {
    return invoiceRepository.findOne(id);
  }

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  public boolean exists(Long id) {
    return invoiceRepository.exists(id);
  }

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  public Streamable<Invoice> findAll() {
    return Streamable.of(invoiceRepository.findAll());
  }

  /**
   * Returns all instances of the type with the given IDs.
   */
  public Streamable<Invoice> findAll(Iterable<Long> longs) {
    return Streamable.of(invoiceRepository.findAll(longs));
  }

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   */
  public long count() {
    return invoiceRepository.count();
  }
}
