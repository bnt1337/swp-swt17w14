package de.gardenmanager.accounting.invoice.invoiceitem.pricing;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class FixedPricing {

  @Id
  @GeneratedValue
  protected Long id;

  private String name;
  private double price;
  private boolean active;

  /**
   * Initialize Fixed Pricing with name and price.
   *
   * @param name name of the {@link FixedPricing}
   * @param price price of the {@link FixedPricing}
   */
  public FixedPricing(String name, double price) {
    this.name = name;
    this.price = price;
    active = true;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private FixedPricing() {
    //Because Spring needs it :(
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
