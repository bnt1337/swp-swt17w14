package de.gardenmanager.accounting.invoice.invoiceitem;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class InvoiceItem {

  @Id
  @GeneratedValue
  protected Long id;

  protected String name;
  protected String tag;
  protected double price;
  private boolean active;
  private double units;

  /**
   * Create a new (tagged) Item for an Invoice.
   */
  public InvoiceItem(String name, String tag, double price) {
    this.name = name;
    this.tag = tag;
    this.price = price;
    active = true;
  }

  /**
   * Create a new (untagged) Item for an Invoice with {@link InvoiceItem#units} set to {@literal
   * 1.0}.
   */
  public InvoiceItem(String name, double price) {
    this(name, "", price);
    units = 1.0;
  }

  /**
   * Create a new (tagged) metered Item for an Invoice.
   *
   * @param price price per unit
   */
  public InvoiceItem(String name, String tag, double price, double units) {
    this(name, tag, price);
    this.units = units;
  }

  @Deprecated
  @SuppressWarnings("unused")
  protected InvoiceItem() {
    //Because Spring needs it :(
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price * units;
  }

  public String getTag() {
    return tag;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public double getUnitPrice() {
    return price;
  }

  public double getUnits() {
    return units;
  }
}
