package de.gardenmanager.accounting.invoice.invoiceitem.pricing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MeteredPricing {

  @Id
  @GeneratedValue
  @SuppressWarnings("unused")
  private Long id;

  private String name;
  @Column(unique = true)
  private String tag;
  private double value;

  private boolean active;
  private boolean userProvidable;

  /**
   * Create a new Metered Pricing.
   *
   * @param name pricing name as shown on invoice.
   * @param tag internal tag
   * @param value price per unit
   * @param userProvidable should the user provide the units within his account
   */
  public MeteredPricing(String name, String tag, double value, boolean userProvidable) {
    this.name = name;
    this.tag = tag;
    this.value = value;
    this.userProvidable = userProvidable;
    active = true;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private MeteredPricing() {
    //Because Spring needs it :(
  }

  public String getName() {
    return name;
  }

  public double getValue() {
    return value;
  }

  public boolean isUserProvidable() {
    return userProvidable;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getTag() {
    return tag;
  }
}
