package de.gardenmanager.accounting.invoice.invoiceitem.pricing;

import org.salespointframework.core.DataInitializer;
import org.springframework.stereotype.Component;

/**
 * Created by bennet on 08.12.2017.
 */
@Component
class MeteredPricingDataInitializer implements DataInitializer {

  private final MeteredPricingRepository meteredPricingRepository;

  public MeteredPricingDataInitializer(
      MeteredPricingRepository meteredPricingRepository) {
    this.meteredPricingRepository = meteredPricingRepository;
  }

  /**
   * Called on application startup to trigger data initialization. Will run inside a transaction.
   */
  @Override
  public void initialize() {
    if (meteredPricingRepository.count() > 0) {
      return;
    }
    meteredPricingRepository.save(new MeteredPricing("Wasser (2017)", "water", 1.95, true));
    meteredPricingRepository.save(new MeteredPricing("Strom (2017)", "electricity", 0.2, true));
    meteredPricingRepository.save(new MeteredPricing("Pacht (2017)", "lease", 0.18, false));
    meteredPricingRepository.save(new MeteredPricing("Strafzahlung für fehlende Arbeitsstunden",
        "fine", 8.0, false));
    //    meteredPricingRepository
    //        .save(new MeteredPricing("Pacht", 0.18, false, ReflectionUtils.findMethod(
    //            Parcel.class, "getSquaremeters")));
  }
}
