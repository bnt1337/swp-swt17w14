package de.gardenmanager.accounting.invoice.invoiceitem.pricing;

import org.salespointframework.core.DataInitializer;
import org.springframework.stereotype.Component;

@Component
class FixedPricingDataInitializer implements DataInitializer {

  private final FixedPricingRepository fixedPricingRepository;

  public FixedPricingDataInitializer(
      FixedPricingRepository fixedPricingRepository) {
    this.fixedPricingRepository = fixedPricingRepository;
  }

  /**
   * Called on application startup to trigger data initialization. Will run inside a transaction.
   */
  @Override
  public void initialize() {
    if (fixedPricingRepository.count() > 0) {
      return;
    }
    fixedPricingRepository
        .save(new FixedPricing("Grundmiete für die Wasseruhr (2017)", 2.6));
    fixedPricingRepository
        .save(new FixedPricing("Grundmiete für den Stromzähler (2017)", 1.55));
    fixedPricingRepository.save(new FixedPricing("Mitgliedsbeitrag (2017)", 17.25));
    fixedPricingRepository.save(new FixedPricing("Haftpflichtbeitrag (2017)", 0.35));
    fixedPricingRepository.save(new FixedPricing("Sozialbeitrag (2017)", 0.5));
    fixedPricingRepository.save(new FixedPricing("Rechtsschutz (2017)", 0.75));
    fixedPricingRepository.save(new FixedPricing("Winterdienst (2017)", 3.0));
    fixedPricingRepository.save(new FixedPricing("Aufwandspauschale (2017)", 12.0));
    fixedPricingRepository.save(new FixedPricing("sonstige Auslagen (2017)", 1.0));
  }
}
