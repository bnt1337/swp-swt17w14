package de.gardenmanager.accounting.invoice.invoiceitem.pricing;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by bennet on 08.12.2017.
 */
@Repository
public interface MeteredPricingRepository extends CrudRepository<MeteredPricing, Long> {

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<MeteredPricing> findAll();

  /**
   * Find all pricings by their active state.
   *
   * @param b state to search for
   * @return matching entities.
   */
  Iterable<MeteredPricing> findAllByActive(boolean b);

  /**
   * Find pricing by it's unique tag.
   */
  MeteredPricing findByTagAndActiveIsTrue(String tag);

  /**
   * Find pricings by unique tag and active state.
   */
  MeteredPricing findByTagAndActive(String tag, boolean active);

  /**
   * Finds all tags.
   */
  @Query("SELECT DISTINCT m.tag FROM MeteredPricing m")
  Iterable<String> findTagDistinctByActiveIsTrue();
}
