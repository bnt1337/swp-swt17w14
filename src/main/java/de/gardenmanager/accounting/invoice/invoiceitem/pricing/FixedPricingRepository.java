package de.gardenmanager.accounting.invoice.invoiceitem.pricing;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FixedPricingRepository extends CrudRepository<FixedPricing, Long> {

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<FixedPricing> findAll();

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  FixedPricing findOne(Long id);

  /**
   * Find all pricings by their active state.
   *
   * @param b state to search for
   * @return matching entities.
   */
  Iterable<FixedPricing> findAllByActive(boolean b);
}
