package de.gardenmanager.accounting.invoice;

import de.gardenmanager.person.Parcel;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

  /**
   * Retrieves all invoices by their {@link Parcel}.
   */
  Iterable<Invoice> findAllByParcel(Parcel parcel);

  /**
   * Retrieves all unsigned invoices by their {@link Parcel}.
   */
  Optional<Invoice> findByParcelAndInvoiceDateIsNull(Parcel parcel);

  /**
   * Retrieves an entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the given id or {@literal null} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  Invoice findOne(Long id);

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}.
   * @return true if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@code id} is {@literal null}
   */
  @Override
  boolean exists(Long id);

  /**
   * Returns all instances of the type.
   *
   * @return all entities
   */
  @Override
  Iterable<Invoice> findAll();

  /**
   * Returns all instances of the type with the given IDs.
   */
  @Override
  Iterable<Invoice> findAll(Iterable<Long> longs);

  /**
   * Returns all invoices which are not signed yet.
   *
   * @return unsigned invoices
   */
  Iterable<Invoice> findAllByInvoiceDateIsNull();

  /**
   * Return all invoices by their invoice Run.
   *
   * @param invoiceRun Run to search by
   * @return Invoices
   */
  Iterable<Invoice> findAllByInvoiceRun(InvoiceRun invoiceRun);

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities
   */
  @Override
  long count();

  /**
   * Return the number of invoices for one {@link Parcel}.
   */
  long countAllByParcel(Parcel parcel);
}
