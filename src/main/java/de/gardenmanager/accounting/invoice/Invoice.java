package de.gardenmanager.accounting.invoice;

import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItem;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.util.LocalDateTimeConverter;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"parcel", "invoiceRun"})
})
public class Invoice {

  @Id
  @GeneratedValue
  private Long id;

  @OneToMany
  private List<InvoiceItem> invoiceItems;
  @ManyToOne
  @JoinColumn(name = "parcel")
  private Parcel parcel;

  @Column(columnDefinition = "TIMESTAMP")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime invoiceDate;

  @Column(columnDefinition = "TIMESTAMP")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime meterReadingDue;

  @Column(columnDefinition = "TIMESTAMP")
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime paidBy;

  @ManyToOne
  @JoinColumn(name = "invoiceRun")
  private InvoiceRun invoiceRun;

  /**
   * Create an Invoice for a Parcel.
   *
   * @param parcel reference to {@link Parcel}
   * @param invoiceRun reference to {@link InvoiceRun}
   */
  public Invoice(Parcel parcel, InvoiceRun invoiceRun) {
    this.parcel = parcel;
    this.invoiceRun = invoiceRun;
    invoiceItems = new LinkedList<>();
  }

  @Deprecated
  @SuppressWarnings("unused")
  private Invoice() {
    //Because Spring needs it :(
  }

  //region Getter and Setter
  public LocalDateTime getInvoiceDate() {
    return invoiceDate;
  }

  /**
   * Set invoiceDate if it is unset. If already set the method locks it self
   *
   * @param invoiceDate set the final invoiceDate
   * @throws IllegalAccessException invoiceDate is already set
   */
  public Invoice setInvoiceDate(@NotNull LocalDateTime invoiceDate) throws IllegalAccessException {
    if (this.invoiceDate != null) {
      throw new IllegalAccessException("can not modify an already set invoiceDate");
    }
    this.invoiceDate = invoiceDate;
    return this;
  }

  public LocalDateTime getMeterReadingDue() {
    return meterReadingDue;
  }

  public Invoice setMeterReadingDue(LocalDateTime meterReadingDue) {
    this.meterReadingDue = meterReadingDue;
    return this;
  }

  public LocalDateTime getPaidBy() {
    return paidBy;
  }

  public Invoice setPaidBy(LocalDateTime paidBy) {
    this.paidBy = paidBy;
    return this;
  }

  public List<InvoiceItem> getInvoiceItems() {
    return invoiceItems;
  }

  public Parcel getParcel() {
    return parcel;
  }

  public Long getId() {
    return id;
  }

  public InvoiceRun getInvoiceRun() {
    return invoiceRun;
  }
  //endregion

  /**
   * add an {@link InvoiceItem} to the {@link Invoice}, if no {@link Invoice#invoiceDate} is set.
   * This methods locks it self after an invoiceDate is set to prevent abuse.
   *
   * @param invoiceItem Item to add
   * @throws IllegalAccessException invoiceDate is already set
   */
  public Invoice addInvoiceItem(InvoiceItem invoiceItem) throws IllegalAccessException {
    if (invoiceDate != null) {
      throw new IllegalAccessException("can not add InvoiceItem, after a invoiceDate is set");
    }

    invoiceItems.add(invoiceItem);
    return this;
  }

  /**
   * Get the summed amount invoiced.
   */
  public double getAmount() {
    return invoiceItems.stream().mapToDouble(InvoiceItem::getPrice).sum();
  }
}
