package de.gardenmanager.accounting;

import de.gardenmanager.accounting.invoice.Invoice;
import de.gardenmanager.accounting.invoice.InvoiceManagement;
import de.gardenmanager.accounting.invoice.InvoiceRepository;
import de.gardenmanager.accounting.invoice.InvoiceRun;
import de.gardenmanager.accounting.invoice.InvoiceRunRepository;
import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItem;
import de.gardenmanager.accounting.invoice.invoiceitem.InvoiceItemRepository;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.MeteredPricing;
import de.gardenmanager.accounting.invoice.invoiceitem.pricing.MeteredPricingRepository;
import de.gardenmanager.person.Parcel;
import de.gardenmanager.person.ParcelRepository;
import de.gardenmanager.person.ParcelStateLeased;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.salespointframework.core.Streamable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Component
@Service
@Transactional
public class AccountingSystem {

  public static final int METER_READING_MIN_UNITS = 100;
  public static final int METER_READING_DUE_DAYS = 14;

  private final ParcelRepository parcelRepository;
  private final InvoiceManagement invoiceManagement;
  private final InvoiceRepository invoiceRepository;
  private final InvoiceItemRepository invoiceItemRepository;
  private final MeteredPricingRepository meteredPricingRepository;
  private final InvoiceRunRepository invoiceRunRepository;

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private final MailSender mailSender;

  //  @Autowired
  //  private MessageSource messageSource;

  /**
   * Initialize the AccountingSystem with AutoWired Beans.
   */
  public AccountingSystem(ParcelRepository parcelRepository,
      InvoiceManagement invoiceManagement,
      InvoiceRepository invoiceRepository,
      InvoiceItemRepository invoiceItemRepository,
      MeteredPricingRepository meteredPricingRepository,
      InvoiceRunRepository invoiceRunRepository,
      MailSender mailSender) {
    this.parcelRepository = parcelRepository;
    this.invoiceManagement = invoiceManagement;
    this.invoiceRepository = invoiceRepository;
    this.invoiceItemRepository = invoiceItemRepository;
    this.meteredPricingRepository = meteredPricingRepository;
    this.invoiceRunRepository = invoiceRunRepository;
    this.mailSender = mailSender;
  }

  /**
   * Create new Invoices to all Leased Parcels. this automatically send email notifications to all
   * users.
   */
  public void createInvoices() {
    log.debug("Creating new InvoiceRun with " + LocalDate.now());
    InvoiceRun ir = invoiceRunRepository.save(new InvoiceRun(LocalDate.now()));

    //All Parcels
    Streamable.of(parcelRepository.findAll()).stream()
        //Filter Leased
        .filter(p -> p.getState() instanceof ParcelStateLeased)
        //Create Invoice with Parcel and InvoiceRun
        .forEach(p -> invoiceManagement.createInvoice(p, ir));

    //All Invoices
    Streamable.of(invoiceRepository.findAll()).stream()
        //Filter Unsigned
        .filter(i -> i.getInvoiceDate() == null && i.getMeterReadingDue() == null)
        //Set meter-reading due and send Mail
        .forEach(i -> {
          invoiceRepository
              .save(i.setMeterReadingDue(LocalDateTime.now().plusDays(METER_READING_DUE_DAYS)));

          Parcel parcel = i.getParcel();
          //If use has an email...
          if (parcel.getCurrentOwner().getContactInfo().getEmail() != null && !parcel
              .getCurrentOwner().getContactInfo().getEmail().isEmpty()) {
            try {
              SimpleMailMessage mailMessage = new SimpleMailMessage();
              mailMessage.setTo(parcel.getCurrentOwner().getContactInfo().getEmail());
              //TODO: configuratable mail from
              mailMessage.setFrom("noreply@gardenmanager.example.com");
              //TODO: l10n
              mailMessage.setSubject("New Invoice Available. Add Meter readings now!");
              mailMessage.setText(String.format(
                  "Dear %s %s,\n"
                      + "\n"
                      + "their is an new Invoice available.\n"
                      + "Please add your meter reading until %s.\n"
                      + "\n"
                      + "Regards\n"
                      + " the Java GardenManager",
                  parcel.getCurrentOwner().getFirstname(), parcel.getCurrentOwner().getLastname(),
                  i.getMeterReadingDue()));

              mailSender.send(mailMessage);
            } catch (Exception e) {
              log.error("Failed to send email", e);
            }
          }
        });
  }

  /**
   * add a meter reading.
   *
   * @throws IllegalAccessException it is past {@link Invoice#meterReadingDue}.
   */
  public void addMeterReading(MeteredPricing meteredPricing, Long invoiceId, double reading)
      throws IllegalAccessException {
    Invoice i = invoiceRepository.findOne(invoiceId);

    if (LocalDateTime.now().isAfter(i.getMeterReadingDue())) {
      throw new IllegalAccessException("It is to late to add meter readings");
    }

    invoiceRepository.save(i.addInvoiceItem(invoiceItemRepository.save(
        new InvoiceItem(meteredPricing.getName(), meteredPricing.getTag(),
            meteredPricing.getValue(), reading))));
  }

  /**
   * estimates missing meter reading by previous invoices average.
   */
  private void estimateMeterReading(Long invoiceId) {
    meteredPricingRepository.findAllByActive(true).forEach(mp -> {
      Invoice i = invoiceRepository.findOne(invoiceId);
      try {
        if (i.getInvoiceItems().stream()
            .noneMatch(
                invoiceItem -> invoiceItem.getName().equals(mp.getName()))) { //estimate unset

          invoiceRepository.save(i.addInvoiceItem(
              invoiceItemRepository.save(new InvoiceItem(
                  mp.getName(),
                  mp.getTag(),
                  mp.getValue(),
                  Math.max(METER_READING_MIN_UNITS,
                      Streamable.of(invoiceRepository.findAllByParcel(i.getParcel())).stream()
                          .filter(fi -> !fi.getId().equals(i.getId())) //exclude current invoice
                          .mapToDouble(
                              invoice -> invoice.getInvoiceItems().stream()
                                  .filter(invoiceItem -> invoiceItem.getTag().equals(mp.getTag()))
                                  .findAny()
                                  .orElse(new InvoiceItem("",0)).getUnits()
                          ).average().orElse(0.0))
              ))
          ));
        }
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    });
  }

  /**
   * Sign an Invoice by setting {@link Invoice#invoiceDate} to {@link LocalDateTime#now()}.
   *
   * @param invoiceId Invoice to sign
   * @throws IllegalAccessException date is already set
   */
  public void signInvoice(Long invoiceId) throws IllegalAccessException {
    Invoice i = invoiceRepository.findOne(invoiceId);
    estimateMeterReading(i.getId());
    invoiceRepository.save(i.setInvoiceDate(LocalDateTime.now()));
  }

  /**
   * mark an Invoice paid.
   */
  public void markPaid(Long invoiceId) {
    invoiceRepository.save(invoiceRepository.findOne(invoiceId).setPaidBy(LocalDateTime.now()));
  }
}
