package de.gardenmanager.roombooking;

import java.io.Serializable;
import javax.persistence.Embeddable;
import org.salespointframework.core.SalespointIdentifier;

/**
 * Created by bennet on 14.11.17.
 */
@Embeddable
public final class ReservationIdentifier extends SalespointIdentifier implements Serializable {

  private static final long serialVersionUID = 201711142029L;

  /**
   * Creates a new unique identifier for {@link Reservation}s.
   */
  ReservationIdentifier() {
    super();
  }

  /**
   * Only needed for property editor, shouldn't be used otherwise.
   *
   * @param reservationIdentifier The string representation of the identifier.
   */
  ReservationIdentifier(String reservationIdentifier) {
    super(reservationIdentifier);
  }
}
