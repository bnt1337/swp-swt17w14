package de.gardenmanager.roombooking;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by bennet on 14.11.17.
 */
public class ReservationCreationForm {
  @NotEmpty(message = "{ReservationCreationForm.description.NotEmpty}") //
  public String description;

  @NotEmpty(message = "{ReservationCreationForm.owner.NotEmpty}") //
  public String owner;

  @NotEmpty(message = "{ReservationCreationForm.start.NotEmpty}")
  public String start;

  @NotEmpty(message = "{ReservationCreationForm.end.NotEmpty}")
  public String end;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public String getStart() {
    return start;
  }

  public void setStart(String start) {
    this.start = start;
  }

  public String getEnd() {
    return end;
  }

  public void setEnd(String end) {
    this.end = end;
  }
}