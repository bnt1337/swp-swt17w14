package de.gardenmanager.roombooking;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bennet on 14.11.17.
 */
@Service
@Transactional
public class RoomBookingsystem {
  private final ReservationRepository reservationRepository;

  public RoomBookingsystem(ReservationRepository reservationRepository) {
    this.reservationRepository = reservationRepository;
  }

  public List<Reservation> getAll(){
    List<Reservation> list = new LinkedList<>();
    reservationRepository.findAll().forEach(list::add);
    return list;
  }

  public Reservation getReservation(ReservationIdentifier reservationIdentifier){
    return reservationRepository.findOne(reservationIdentifier);
  }

  public boolean createReservation(LocalDateTime start, LocalDateTime end, String description, String owener){
    if(reservationRepository.getCollusions(start, end).size() > 0) {
      return false;
    }

    reservationRepository.save(new Reservation(start, end, owener, description));
    return true;
  }

  public void deleteReservation(ReservationIdentifier reservationIdentifier){
    reservationRepository.delete(reservationIdentifier);
  }
}
