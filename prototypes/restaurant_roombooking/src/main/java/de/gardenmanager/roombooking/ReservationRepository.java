package de.gardenmanager.roombooking;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by bennet on 14.11.17.
 */
@Repository
public interface ReservationRepository extends CrudRepository<Reservation, ReservationIdentifier> {

  @Query("select id from Reservation r where (r.start <= :startdate and r.end >= :startdate) or (r.start <= :enddate and r.end >= :enddate) or (r.start >= :startdate and r.end <= :enddate)")
  List<Reservation> getCollusions(@Param("startdate") LocalDateTime start,
      @Param("enddate") LocalDateTime end);
}
