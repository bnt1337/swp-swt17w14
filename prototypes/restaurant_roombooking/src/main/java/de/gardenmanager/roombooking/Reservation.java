package de.gardenmanager.roombooking;

import de.gardenmanager.LocalDateTimeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.salespointframework.core.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by bennet on 14.11.17.
 */

@Entity
public class Reservation extends AbstractEntity<ReservationIdentifier> implements Comparable<Reservation>,
    Serializable {

  @EmbeddedId
  @AttributeOverride(name = "id", column = @Column(name = "id"))
  private ReservationIdentifier id = new ReservationIdentifier();

  @Column(columnDefinition = "TIMESTAMP")
  @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime start;

  @Column(columnDefinition = "TIMESTAMP")
  @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
  @Convert(converter = LocalDateTimeConverter.class)
  private LocalDateTime end;

  private String owner;
  private String desciption;

  private static final long serialVersionUID = 201711152149L;

  private Reservation() {}

  public Reservation(LocalDateTime start, LocalDateTime end, String owner,
      String desciption) {
    this.start = start;
    this.end = end;
    this.owner = owner;
    this.desciption = desciption;
  }

  public ReservationIdentifier getId() {
    return id;
  }

  public LocalDateTime getStart() {
    return start;
  }

  public LocalDateTime getEnd() {
    return end;
  }

  public String getOwner() {
    return owner;
  }

  public String getDesciption() {
    return desciption;
  }

  @Override
  public int compareTo(Reservation other) {
    return this.start.compareTo(other.start);
  }
}
