package de.gardenmanager.restaurant;

import org.salespointframework.catalog.Catalog;
import org.salespointframework.catalog.ProductIdentifier;

public interface CurrentMealCatalog extends Catalog<Meal> {

  Iterable<Meal> findAll();
}
