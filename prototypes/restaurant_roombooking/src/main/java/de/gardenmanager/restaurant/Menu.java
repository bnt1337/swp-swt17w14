package de.gardenmanager.restaurant;

import static org.salespointframework.core.Currencies.EURO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.javamoney.moneta.Money;
import org.salespointframework.catalog.Product;

@Entity
public class Menu extends Product {

  @Id
  private static final long serialVersionUID = 201711121347L;

  @ManyToOne
  private Meal starter1;
  @ManyToOne
  private Meal starter2;
  @ManyToOne
  private Meal starter3;
  @ManyToOne
  private Meal starter4;
  @ManyToOne
  private Meal maincourse1;
  @ManyToOne
  private Meal maincourse2;
  @ManyToOne
  private Meal maincourse3;
  @ManyToOne
  private Meal maincourse4;
  @ManyToOne
  private Meal dessert1;
  @ManyToOne
  private Meal dessert2;
  @ManyToOne
  private Meal dessert3;
  @ManyToOne
  private Meal dessert4;

  @Deprecated
  protected Menu(){}

  public Menu(String name, Meal starter1, Meal starter2, Meal starter3, Meal starter4,
      Meal maincourse1, Meal maincourse2, Meal maincourse3, Meal maincourse4,
      Meal dessert1, Meal dessert2, Meal dessert3, Meal dessert4) {
    super(name, Money.zero(EURO));
    this.starter1 = starter1;
    this.starter2 = starter2;
    this.starter3 = starter3;
    this.starter4 = starter4;
    this.maincourse1 = maincourse1;
    this.maincourse2 = maincourse2;
    this.maincourse3 = maincourse3;
    this.maincourse4 = maincourse4;
    this.dessert1 = dessert1;
    this.dessert2 = dessert2;
    this.dessert3 = dessert3;
    this.dessert4 = dessert4;
  }

  public String getName() {
    return super.getName();
  }

  public Meal getStarter1() {
    return starter1;
  }

  public Meal getStarter2() {
    return starter2;
  }

  public Meal getStarter3() {
    return starter3;
  }

  public Meal getStarter4() {
    return starter4;
  }

  public Meal getMaincourse1() {
    return maincourse1;
  }

  public Meal getMaincourse2() {
    return maincourse2;
  }

  public Meal getMaincourse3() {
    return maincourse3;
  }

  public Meal getMaincourse4() {
    return maincourse4;
  }

  public Meal getDessert1() {
    return dessert1;
  }

  public Meal getDessert2() {
    return dessert2;
  }

  public Meal getDessert3() {
    return dessert3;
  }

  public Meal getDessert4() {
    return dessert4;
  }

}
