package de.gardenmanager.restaurant;

import org.salespointframework.useraccount.UserAccount;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by bennet on 10.11.17.
 */
public interface CatererRepository extends CrudRepository<UserAccount, Long> {}
