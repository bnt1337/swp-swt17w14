package de.gardenmanager.restaurant;

import javax.persistence.Entity;
import org.javamoney.moneta.Money;
import org.salespointframework.catalog.Product;

@Entity
public class Meal extends Product {

  public enum MealType{
    STARTER, DESSERT, MAINCOURSE
  }

  private static final long serialVersionUID = 201711121347L;
  private String allergenes;
  private MealType mealType;

  @Deprecated
  public Meal(){}

  public Meal(String name, String allergenes, Money price, MealType mealType) {
    super(name, price);
    this.allergenes = allergenes;
    this.mealType = mealType;
  }

  public String getAllergenes(){
    return allergenes;
  }

  public MealType getMealType() {
    return mealType;
  }

  public void setMealType(MealType mealType) {
    this.mealType = mealType;
  }

}