package de.gardenmanager.restaurant;

import org.salespointframework.catalog.Catalog;
import org.salespointframework.catalog.ProductIdentifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public interface MenuCatalog extends Catalog<Menu> {
  static final Sort DEFAULT_SORT = new Sort(Direction.DESC, "productIdentifier");

  Iterable<Meal> findById(ProductIdentifier id, Sort sort);
  Iterable<Menu> findAll();

  default Iterable<Meal> findById(ProductIdentifier id) {
    return findById(id, DEFAULT_SORT);
  }

}
