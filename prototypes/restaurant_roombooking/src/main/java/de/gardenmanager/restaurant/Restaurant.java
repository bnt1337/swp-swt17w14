package de.gardenmanager.restaurant;

import de.gardenmanager.restaurant.Meal.MealType;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;

/**
 * business-logic for the Restaurant-Controller
 */
@Component
public class Restaurant {

  private final MenuCatalog temporary_menu;
  private final MealCatalog mealCatalog;

  /**
   * constructor of the class
   *
   * @param menu reference to the menu
   * @param mealCatalog the meal-catalog where the meals are stored
   */
  public Restaurant(MenuCatalog menu, MealCatalog mealCatalog) {
    this.temporary_menu = menu;
    this.mealCatalog = mealCatalog;
  }

  /**
   * creates an new meal with the given parameters
   *
   * @param name the name of the meal
   * @param allergens allergens in the meal
   * @param price price of the meal
   * @param type type of the meal as string
   * @return the new meal
   */
  public Meal create_meal(String name, String allergens, Money price, String type) {

    if (type.equals("Hauptgericht")) {
      Meal meal = new Meal(name, allergens, price, MealType.MAINCOURSE);
      mealCatalog.save(meal);
      return meal;
    } else if (type.equals("Dessert")) {
      Meal meal = new Meal(name, allergens, price, MealType.DESSERT);
      mealCatalog.save(meal);
      return meal;
    } else {  // type is Starter
      Meal meal = new Meal(name, allergens, price, MealType.STARTER);
      mealCatalog.save(meal);
      return meal;
    }
  }

  /**
   * deletes the meal given by the name of the meal
   *
   * @param name the meal of the meal which should be removed
   */
  public void delete_meal(String name) {
    mealCatalog.delete(mealCatalog.findByNameOrderByNameAsc(name));

    return;
  }

  /**
   * returns all meals with all their properties
   *
   * @return the meals with all their properties
   */
  public Iterable<Meal> get_meals() {
    return this.mealCatalog.findAll();
  }

  /**
   * returns all meals with the STARTER property
   *
   * @return all starters
   */
  public Iterable<Meal> get_Starters() {return this.mealCatalog.findByMealType(MealType.STARTER);}

  /**
   * returns all meals with the MAINCOURSE property
   *
   * @return all main courses
   */
  public Iterable<Meal> get_MainCourse() {return this.mealCatalog.findByMealType(MealType.MAINCOURSE);}

  /**
   * returns all meals with the DESSERT property
   *
   * @return all desserts
   */
  public Iterable<Meal> get_Desserts() {return this.mealCatalog.findByMealType(MealType.DESSERT);}

  /**
   * stores the temporary menu in the menu-list and resets the temporary menu
   */
  public void create_menu() {
  }

  /**
   * deletes the menu given by name from the temporary menu
   * @param name the name of the menu to get deleted
   */
  public void delete_menu(String name) {
    this.temporary_menu.delete(this.temporary_menu.findByName(name));
  }

  /**
   * returns the content of the temporary menu
   * @return returns all actual meals from the temporary menu
   */
  public Iterable<Menu> get_menu() {
    return this.temporary_menu.findAll();
  }

  /**
   * adds an meal to the temporary menu
   *
   * @param name name of the new menu
   * @param starter1 the first starter
   * @param starter2 the second starter
   * @param starter3 the third starter
   * @param starter4 the fourth starter
   * @param maincourse1 the first maincourse
   * @param maincourse2 the second maincourse
   * @param maincourse3 the third maincourse
   * @param maincourse4 the fourth maincourse
   * @param dessert1 the first dessert
   * @param dessert2 the second dessert
   * @param dessert3 the third dessert
   * @param dessert4 the fourth dessert
   */
  public void add_meal_to_menu(String name, String starter1, String starter2, String starter3, String starter4,
      String maincourse1, String maincourse2, String maincourse3, String maincourse4, String dessert1,
      String dessert2, String dessert3, String dessert4) {
    Menu menu = new Menu(name,
        this.mealCatalog.findByNameOrderByNameAsc(starter1).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(starter2).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(starter3).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(starter4).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(maincourse1).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(maincourse2).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(maincourse3).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(maincourse4).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(dessert1).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(dessert2).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(dessert3).iterator().next(),
        this.mealCatalog.findByNameOrderByNameAsc(dessert4).iterator().next());

    this.temporary_menu.save(menu);
  }

  /**
   * removes a meal from the temporary menu
   *
   * @param meal the meal to remove
   */
  public void remove_meal_from_menu(Meal meal) {
  }

  /**
   * removes all meals from the temporary menu
   */
  public void remove_all_from_menu() {
    this.temporary_menu.deleteAll();
  }
}
