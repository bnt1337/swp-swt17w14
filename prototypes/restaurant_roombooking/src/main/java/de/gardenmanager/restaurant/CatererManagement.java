package de.gardenmanager.restaurant;

/**
 * Created by bennet on 10.11.17.
 */

import de.gardenmanager.validation.CatererRegistrationForm;
import org.salespointframework.core.Streamable;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Class wrapping the UserAccount of Resturant Employee
 */
@Service
@Transactional
public class CatererManagement {
  private final CatererRepository caterers;
  private final UserAccountManager userAccounts;

  /**
   * @param caterers must not be {@literal null}.
   * @param userAccounts must not be {@literal null}.
   */
  CatererManagement(CatererRepository caterers, UserAccountManager userAccounts) {

    Assert.notNull(caterers, "CatererRepository must not be null!");
    Assert.notNull(userAccounts, "UserAccountManager must not be null!");

    this.caterers = caterers;
    this.userAccounts = userAccounts;
  }

  /**
   * Creates a new {@link UserAccount} using the information given in the {@link CatererRegistrationForm}.
   *
   * @param name must not be {@literal null}.
   * @param password must not be {@Literal null}.
   * @return
   */
  public UserAccount createCustomer(String name, String password) {

    Assert.notNull(name, "Name must not be null!");
    Assert.notNull(password, "Password must not be null!");

    UserAccount userAccount = userAccounts.create(name, password, Role.of("ROLE_CATERER"));

    return caterers.save(userAccount);
  }

  /**
   * Returns all {@link UserAccount}s currently available in the system.
   *
   * @return
   */
  public Streamable<UserAccount> findAll() {
    return Streamable.of(caterers.findAll());
  }
}
