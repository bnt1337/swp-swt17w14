package de.gardenmanager.restaurant;

import de.gardenmanager.restaurant.Meal.MealType;
import org.salespointframework.catalog.Catalog;

public interface MealCatalog extends Catalog<Meal> {

  Iterable<Meal> findByMealType(MealType type);
  Iterable<Meal> findAll();
  Iterable<Meal> findByNameOrderByNameAsc(String name);
}
