package de.gardenmanager.controller;

import de.gardenmanager.roombooking.Reservation;
import de.gardenmanager.roombooking.ReservationCreationForm;
import de.gardenmanager.roombooking.RoomBookingsystem;
import java.time.LocalDateTime;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by bennet on 14.11.17.
 */
@Controller
public class RoombookingController {
  private final RoomBookingsystem roomBookingsystem;

  public RoombookingController(RoomBookingsystem roomBookingsystem) {
    this.roomBookingsystem = roomBookingsystem;
  }

  @GetMapping("/booking/add")
  public String createGet(Model model){
    model.addAttribute("reservationCreationForm", new ReservationCreationForm());
    return "booking";
  }

  @RequestMapping("/booking/add")
  public String createPost(@ModelAttribute @Valid
      ReservationCreationForm reservationCreationForm, BindingResult result){
    if(result.hasErrors()) {
      return "booking";
    }

    if(roomBookingsystem.createReservation(
        LocalDateTime.parse(reservationCreationForm.start),
        LocalDateTime.parse(reservationCreationForm.end),
        reservationCreationForm.description,
        reservationCreationForm.owner)) {
      return "redirect:/booking";
    } else {
      result.reject("start", "{ReservationCreationForm.start.reservationcollusion}");
      result.reject("end", "{ReservationCreationForm.end.reservationcollusion}");
      return "booking";
    }
  }

  @GetMapping("/booking")
  public String list(Model model){
    model.addAttribute("bookings", roomBookingsystem.getAll());
    return "dashboardbooking";
  }

  @GetMapping("/booking/{reservation}/delete")
  public String delete(@PathVariable("reservation")Reservation reservation){
    roomBookingsystem.deleteReservation(reservation.getId());
    return "redirect:/booking";
  }
}
