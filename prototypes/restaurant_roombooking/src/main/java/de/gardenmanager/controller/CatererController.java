package de.gardenmanager.controller;

import de.gardenmanager.restaurant.CatererManagement;
import de.gardenmanager.validation.CatererRegistrationForm;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by bennet on 10.11.17.
 */

/**
 * Controller between {@link CatererManagement} and UI
 */
@Controller
public class CatererController {

  private final CatererManagement catererManagement;

  CatererController(CatererManagement catererManagement) {
    Assert.notNull(catererManagement, "CatererManagement must not be null!");
    this.catererManagement = catererManagement;
  }

  @RequestMapping("/caterer/register")
  public String createCatererPost(@ModelAttribute("catererRegistrationForm") @Valid CatererRegistrationForm catererRegistrationForm, BindingResult result){
    if (result.hasErrors()) {
      return "catererregistration";
    }

    this.catererManagement.createCustomer(catererRegistrationForm.getName(), catererRegistrationForm.getPassword());

    return "redirect:/dashboardhome";
  }

  @RequestMapping("/caterer/add")
  public String createCatererGet(Model model) {
    model.addAttribute("catererRegistrationForm", new CatererRegistrationForm());
    return "catererregistration";
  }

  @GetMapping("/??????")
  String listCaterers(Model model) {

    model.addAttribute("customerList", catererManagement.findAll());

    return "customers";
  }
}
