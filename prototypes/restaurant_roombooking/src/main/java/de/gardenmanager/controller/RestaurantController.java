package de.gardenmanager.controller;

import static org.salespointframework.core.Currencies.EURO;

import de.gardenmanager.validation.MealForm;
import de.gardenmanager.validation.MenuForm;
import javax.validation.Valid;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Controller;
import de.gardenmanager.restaurant.Restaurant;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * controller for the Restaurant-Controller
 */
@Controller
public class RestaurantController {

  private final Restaurant restaurant;

  /**
   * constructor
   *
   * @param restaurant restaurant für the business-logic
   */
  RestaurantController(Restaurant restaurant) {
    this.restaurant = restaurant;
  }

  /**
   * Mapping for /meal (show all meals)
   *
   * @param model the model
   */
  @RequestMapping("/meal")
  public String meal(Model model) {
    model.addAttribute("mealList", restaurant.get_meals());
    return "dashboardmeal";
  }

  /**
   * Mapping for /menu (show all menus)
   *
   * @param model the model
   */
  @RequestMapping("/menu")
  public String menu(Model model) {
    model.addAttribute("menuList", restaurant.get_menu());
    return "dashboardmenu";
  }

  /**
   * Mapping for /home (homepage of the backend)
   */
  @RequestMapping("/home")
  public String home() {
    return "/dashboardhome";
  }

  /**
   * Mapping for /booking (reservation of the room)
   */
  @RequestMapping("/booking")
  public String booking() {
    return "dashboardbooking";
  }

  /**
   * Mapping for /meal/add (GUI for adding a new menu)
   *
   * @param model the model
   */
  @RequestMapping("/meal/add")
  public String addMeal(Model model) {
    model.addAttribute("mealForm", new MealForm());
    return "dashboardaddmeal";
  }

  /**
   * Mapping for /meal/add/new
   *
   * @param mealForm the mealForm with the required data
   * @param result the result
   */
  @RequestMapping("/meal/add/new")
  public String addNewMeal(@ModelAttribute("mealForm") @Valid MealForm mealForm,
      BindingResult result) {
    if (result.hasErrors()) {
      return "dashboardaddmeal";
    }

    double amount = mealForm.getPrize().doubleValue();
    if (Integer.numberOfTrailingZeros(((int) amount) % 10) > 0 && (amount - (int) amount) < 0.01) {
      amount -= 0.01;
    }

    if(amount < 0) {
      amount = 0.0;
    }
    
    restaurant.create_meal(mealForm.getName(), mealForm.getAllergen(),
        Money.of(amount, EURO), mealForm.getMealType());

    return "redirect:/meal";
  }

  /**
   * Mapping for /meal/remove/{name} (remove an existing meal)
   *
   * @param name the name of the meal which we would remove
   */
  @RequestMapping("/meal/remove/{name}")
  public String removeMeal(@PathVariable(value = "name") String name) {
    restaurant.delete_meal(name);
    return "redirect:/meal";
  }

  /**
   * Mapping for /menu/add (add an new (temporary) menu)
   *
   * @param model the model
   */
  @RequestMapping("/menu/add")
  public String addMenu(Model model) {
    model.addAttribute("starters", restaurant.get_Starters());
    model.addAttribute("maincourses", restaurant.get_MainCourse());
    model.addAttribute("desserts", restaurant.get_Desserts());
    model.addAttribute("menuForm", new MenuForm());
    return "dashboardaddmenu";
  }

  /**
   * Mapping for /menu/add/new (add meal to temporary menu)
   *
   * @paaram menuForm the menuForm object for data exchange
   * @param result the result
   */
  @RequestMapping(value="/menu/add/new", method = RequestMethod.POST)
  public String addNewMenu(@Valid MenuForm menuForm, BindingResult result) {
    if(result.hasErrors()) {
      return "dashboardaddmenu";
    }
    restaurant.add_meal_to_menu(menuForm.getName(),
        menuForm.getStarter1(), menuForm.getStarter2(), menuForm.getStarter3(), menuForm.getStarter4(),
        menuForm.getMainCourse1(), menuForm.getMainCourse2(), menuForm.getMainCourse3(), menuForm.getMainCourse4(),
        menuForm.getDessert1(), menuForm.getDessert2(), menuForm.getDessert3(), menuForm.getDessert4());
    return "redirect:/menu";
  }

  /**
   * Mapping for /menu/remove (remove meal from temporary menu)
   *
   * @param name the name of the menu to store
   */
  @RequestMapping("/menu/remove/{name}")
  public String removeMenu(@PathVariable(value = "name") String name) {
    restaurant.delete_menu(name);
    return "redirect:/menu";
  }

  /**
   * Mapping for /menu/create (write meals from temporary menu card to menu card)
   */
  @RequestMapping("/menu/create")
  public String createMenu() {
    restaurant.create_menu();
    return "redirect:/menu";
  }
}