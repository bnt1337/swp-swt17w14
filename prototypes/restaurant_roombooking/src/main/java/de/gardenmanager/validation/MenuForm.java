package de.gardenmanager.validation;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import de.gardenmanager.restaurant.Meal;

/**
 * form to access menu data
 */
public class MenuForm {

  @NotEmpty(message = "{MenuForm.name.NotEmpty}")
  private String name;

  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String starter1;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String starter2;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String starter3;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String starter4;

  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String mainCourse1;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String mainCourse2;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String mainCourse3;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String mainCourse4;

  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String dessert1;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String dessert2;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String dessert3;
  @NotEmpty(message = "bitte Feld ausfüllen!")
  private String dessert4;

  public String getName() {
    return name;
  }
  public String getStarter1() {
    return this.starter1;
  }
  public String getStarter2() {
    return this.starter2;
  }
  public String getStarter3() {
    return this.starter3;
  }
  public String getStarter4() {
    return this.starter4;
  }
  public String getMainCourse1() {
    return this.mainCourse1;
  }
  public String getMainCourse2() {
    return this.mainCourse2;
  }
  public String getMainCourse3() {
    return this.mainCourse3;
  }
  public String getMainCourse4() {
    return this.mainCourse4;
  }
  public String getDessert1() {
    return this.dessert1;
  }
  public String getDessert2() {
    return this.dessert2;
  }
  public String getDessert3() {
    return this.dessert3;
  }
  public String getDessert4() {
    return this.dessert4;
  }

  public void setName(String name) {
    this.name = name;
  }
  public void setStarter1(String meal) {
    this.starter1 = meal;
  }
  public void setStarter2(String meal) {
    this.starter2 = meal;
  }
  public void setStarter3(String meal) {
    this.starter3 = meal;
  }
  public void setStarter4(String meal) {
    this.starter4 = meal;
  }
  public void setMainCourse1(String meal) {
    this.mainCourse1 = meal;
  }
  public void setMainCourse2(String meal) {
    this.mainCourse2 = meal;
  }
  public void setMainCourse3(String meal) {
    this.mainCourse3 = meal;
  }
  public void setMainCourse4(String meal) {
    this.mainCourse4 = meal;
  }
  public void setDessert1(String meal) {
    this.dessert1 = meal;
  }
  public void setDessert2(String meal) {
    this.dessert2 = meal;
  }
  public void setDessert3(String meal) {
    this.dessert3 = meal;
  }
  public void setDessert4(String meal) {
    this.dessert4 = meal;
  }
}
