package de.gardenmanager.validation;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class MealForm {

  @NotEmpty(message = "{MealForm.name.NotEmpty}")
  private String name;

  @NotEmpty(message = "{MealForm.allergen.NotEmpty}")
  private String allergen;

  @NotNull(message = "{MealForm.prize.NotNull}")
  private Number prize;

  @NotEmpty(message = "{MealForm.Type.NotEmpty}")
  private String mealType;

  public String getMealType() {
    return mealType;
  }

  public void setMealType(String mealType) {
    this.mealType = mealType;
  }

  public Number getPrize() {
    return prize;
  }

  public void setPrize(Number prize) {
    this.prize = prize;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAllergen() {
    return allergen;
  }

  public void setAllergen(String allergen) {
    this.allergen = allergen;
  }

}
