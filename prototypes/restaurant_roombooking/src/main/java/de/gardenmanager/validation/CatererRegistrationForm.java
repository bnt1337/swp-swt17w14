package de.gardenmanager.validation;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by bennet on 11.11.17.
 */

public class CatererRegistrationForm {

  @NotEmpty(message = "{CatererRegistrationForm.name.NotEmpty}")
  public String name;

  @NotEmpty(message = "{CatererRegistrationForm.password.NotEmpty}")
  public String password;

  public String getName(){ return name;}

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password){
    this.password = password;
  }
}
