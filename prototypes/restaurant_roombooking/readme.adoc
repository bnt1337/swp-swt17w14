= Kickstart

The kickstart module is a template project to bootstrap the Java project in the Software Engineering lab.
The project is supposed to be copied into the group's repository to get it started easily.
It contains the following features:

* a skeleton Java 8 web application based on Spring Boot and Salespoint framework (see `garden-manager/src/main/java`)
* a placeholder Asciidoc file in `garden-manager/src/main/asciidoc` for documentation

== How to run the application?

* In the IDE: find `Application.java`, richt-click project, select "Run As > Java Application"
* From the command line: run `./gradlew runBoot`

== How to package the application?

* Run `./gradlew clean`. The packaged application (a JAR in `build/`) can be run with `java -jar $jarName`.

= Setup Code Style in IDEA
Settings > Editor > Code Style > (Gear) > Import Scheme > Intellij IDEA Code Style XML > (select the `intellij-java-google-style.xml`)
